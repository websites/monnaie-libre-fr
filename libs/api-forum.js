import config from 'static/settings/globals.json'

const forumUrl = config.forum_url

/**
 * Fetch forum categories.
 *
 * @return {Array} An array of categories
 */
export async function fetchCategories() {
  // Caches to avoid fetching each time the dropdown is opened
  if (sessionStorage.getItem('categories_forum')) {
    return JSON.parse(sessionStorage.getItem('categories_forum'))
  } else {
    const categories = await fetch(`${forumUrl}/categories.json`)
      .then((response) => response.json())
      .then((data) => data.category_list.categories)

    sessionStorage.setItem('categories_forum', JSON.stringify(categories))
    return categories
  }
}

/**
 * Fetch calendar page to get all events. Filter events already done.
 *
 * @return {Array} An array of events
 */
export async function fetchNextEvents(params) {
  // Caches to avoid fetching each time it's mounted
  if (sessionStorage.getItem('events_forum')) {
    return JSON.parse(sessionStorage.getItem('events_forum'))
  } else {
    const events = await fetch(
      `${forumUrl}/discourse-post-event/events.json?include_details=true${params}`
    )
      .then((response) => response.json())
      .then((data) =>
        data.events.filter((event) => Date.parse(event.starts_at) > Date.now())
      )
    // console.log(events)
    sessionStorage.setItem('events_forum', JSON.stringify(events))
    return events
  }
}
