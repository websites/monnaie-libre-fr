export default async () => {
  const { $content } = require('@nuxt/content')

  const routes = []

  const posts = await $content({ deep: true })
    .where({ dir: { $nin: ['/ui', '/ressources'] } })
    .only('path')
    .fetch()

  for (const post of posts) {
    routes.push(post.path)
  }

  return routes
}
