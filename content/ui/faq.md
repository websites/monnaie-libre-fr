---
title: Foire aux questions
---
## Les groupes locaux, pour poser vos questions de vive voix

N’hésitez pas à vous rapprocher de votre groupe local pour discuter monnaie libre avec des gens qui utilisent déjà la Ğ1 :

**[Voir la carte des groupes locaux](https://carte.monnaie-libre.fr?members=false)**

## Un forum de discussion pour poser vos questions en ligne

Pour davantage d’explications, il existe une plateforme de discussion à l’intérieur de laquelle vous pouvez poser vos questions :

**[Visiter le forum de discussion](https://forum.monnaie-libre.fr/)**

Attention : si la plupart des membres du forum vous répondront de façon cordiale, les membres les plus actifs sont parfois aussi lassés de répondre souvent aux mêmes questions. Pensez donc à utiliser [la fonction de recherche](https://forum.monnaie-libre.fr/search), car il y a des chances que votre question ait déjà été posée et traitée.