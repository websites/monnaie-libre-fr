---
title: Introduction page d'accueil
description: Texte d'introduction en page d'accueil sous la section de recherche
---
<img src="/uploads/logo-monnaie-libre.svg" align ="right" style="width:300px;height:300px;margin:30px" >

## Avons-nous besoin d'une monnaie ?

Un outil de mesure ; une unité de compte. Elle mesure les échanges et les stocks.
Un outil pour les échanges ; une valeur d'échange. Elle rend possible tous les échanges, de natures différentes, de biens et services différents, à des moments différents.
Un outil pour construire l'avenir ; une valeur de réserve. Elle permet de prévoir des situations à venir, de rassembler des moyens pour agir.

Une monnaie a - *ou peut avoir* - d'autres fonctions, mais ce sont les 3 principaux usages partagés par tout le monde.

### Peut-on s'en passer ?

On peut toujours trouver des situations et des contextes où l'on peut mesurer, échanger, épargner, sans recours à la monnaie.

Mais la monnaie est le meilleur moyen que nous ayons pour faire tout cela dès qu'il y a ... asymétrie. Or dans les échanges, les asymétries sont ce qu'il y a de plus courant. **Gérer les asymétries à grande échelle** est donc le rôle principal d'une monnaie.

Il est très pertinent de se poser la question : d'où vient la monnaie ? Comment est-elle créée ? Par qui ?

## Pourquoi une autre monnaie ? Quel est le problème ?

Tout peut être une monnaie, c'est l'usage qui fait d'un objet une monnaie. Les céréales (dont le blé), le sel (d'où vient le mot salaire), les pièces d'or, ont longtemps servi de monnaie. Aujourd'hui les monnaies sont fiduciaires, elles reposent sur la confiance.

Aujourd'hui pour simplifier, 90% de la monnaie est créée par les banques privées, lorsqu'un crédit est accordé. C'est le fait de contracter une dette auprès d'une banque qui crée la monnaie. Aussi l'appelle-t-on communément ... **monnaie-dette**. C'est une monnaie scripturale, une simple ligne d'écriture.

Notre euro est une monnaie-dette, et il n'est pas le nôtre, il appartient aux banques. On ne fait que le louer, et il nous coûte - très - cher. **En utilisant la monnaie-dette vous faites confiance ... aux banquiers.**

En souscrivant un crédit, on s'engage à créer une valeur qui n'existe pas encore (production d'un bien ou d'un service, travail salarié, ...). Or ce sont les banques qui accordent ce crédit, ce sont donc elles qui décident ce qui pourra être produit ou pas, **ce qui a de la valeur et ce qui n'en a pas**. Selon des critères qui lui appartiennent. Nous n'avons pas vraiment - du tout - droit au chapitre sur cette question.

En souscrivant un crédit de 10.000, nous devons rembourser les 10.000 (qui seront détruits comme ils ont été créés), mais également lesdits "intérêts". Or les intérêts eux, ne sont pas créés par la souscription du crédit. Nous devons collectivement - en permanence - rembourser plus de monnaie qu'il n'en a été créée, en **rendre plus qu'il n'en existe**. 

On peut appeler cela une "chaise musicale", ou une "machine à faillite". Libre à chacun(e) de ne pas y voir de problème.

Seule alternative possible : quel que soit le lieu, et quelle que soit l'époque, chaque individu crée la même proportion relative de monnaie, durant toute sa vie.

## Une monnaie relative

Ce n'est pas la quantité de monnaie que l'on possède qui compte, mais bien la part relative que représente cette quantité, par rapport à la masse monétaire globale existante et par rapport au nombre de personnes qui la "partagent".\
En monnaie libre, on ne compte plus en quantitatif mais en relatif. On utilise une **unité relative de monnaie : le <lexique>DU</lexique> quotidien**.