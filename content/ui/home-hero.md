---
title: Héro page d'accueil
---
<section class="xl:mr-20">
  <h1 class="mb-4 text-3xl md:text-4xl xl:text-5xl bg-clip-text text-transparent bg-gradient-to-r from-purple-800 to-blue-600 font-extrabold leading-tight slide-in-bottom-h1">
  La monnaie libre
  </h1>

  <p class="leading-normal text-xl text-gray-700 dark:text-gray-300 md:text-2xl xl:text-3xl mb-12 font-semibold slide-in-bottom-subtitle">
  Une autre création monétaire...<br />à expérimenter !
  </p>

  <p class="leading-normal text-base text-gray-700 dark:text-gray-300 xl:text-xl mb-4 slide-in-bottom-subtitle">
  La monnaie libre rend possible un modèle économique plus juste et durable, modèle à concevoir et mettre en oeuvre.<br /><br />La monnaie libre ne privilégie personne, ni géographiquement, ni d'une génération à une autre : chaque personne crée sa part égale relative de monnaie, chaque jour.
  </p>

  <p class="leading-normal text-base text-gray-700 dark:text-gray-300 xl:text-xl mb-4 slide-in-bottom-subtitle">La June est une expérience en cours, accessible à tout le monde, sans autre condition que de la choisir. 
  </p>
<p>
<a href="maj-v2/" style ="font-size:1.8em; font-weight: bold; color:#34A1FF">
 <div style ="border: solid #34A1FF; border-radius: 25px;  background: gold; width:25em; padding:0.5em;">
 <img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width=90 align=left>
  Détails sur la Mise </br> à Jour Duniter V2
 </div>
</a>
</p>
</section>
