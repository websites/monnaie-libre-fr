---
title: Footer
---
La monnaie libre est portée par un collectif d’êtres humains, elle ne dépend d’aucune organisation "officielle".     
De nombreux [collectifs](/ressources?filters=Asso&filters=Groupe%20local) et [autres sites](/ressources?filters=Blog) sur la monnaie libre existent pour en parler…     
Bien que francophone, ce site se veut compréhensible et utile pour le plus grand nombre.     
Il est possible de rencontrer les contributeurs de ce site lors des [événements](/#agenda) monnaie libre et discuter avec eux sur les 2 forums historiques afin de [contribuer](/contribuer) vous aussi.
