---
title: Sobriété numérique et démocratie
dev-notes: It's look ugly for now, but it will be better and easier to build
  block in next version with vuejs component in markdown...
---
<nuxt-link to="/la-monnaie-libre-et-lenvironnement" class="group block mt-6 lg:mt-12 p-8 hover:shadow-xl rounded-lg transition transform hover:-translate-y-1">
  <section class="flex flex-col md:flex-row items-center gap-8">
      <img src="/img/healthy-eating.svg" class="w-4/5 md:w-2/5" />
      <div>
        <h2 class="text-purple-800 dark:text-purple-600 text-2xl lg:text-3xl mb-4">L'environnement</h2>
        <p class="text-base lg:text-lg">Il serait abusif de qualifier la Ğ1 de monnaie écologique, car il est difficile de considérer un système numérique comme tel. Mais la Ğ1 est concue pour sa sobriété dans la très longue durée.
Elle n'utilise pas une puissance de calcul pour sécuriser la blockchain.

Elle a de plus un impact très concret sur notre économie. La monnaie-dette oblige chacun(e) et toute la société à produire toujours davantage. Dans ce référentiel de création monétaire, ladite croissance n'est pas une option. Lorsqu'on rembourse le capital d'une dette, la monnaie disparait (le capital est détruit, supprimé). Donc plus de nouvelle dette, plus de monnaie. Pas de monnaie, pas d'économie. Elle s'effondre, et nous avec.

Une seule solution possible, changer de référentiel de création monétaire.

La création monétaire par DU permet de ne plus passer par cet dette perpétuelle. Nous ne sommes plus obligés de produire toujours plus comme une fin en soi, quoi qu'il en coûte. Il devient alors possible de concevoir et mettre en oeuvre une économie qui cherche simplement à couvrir nos besoins et *vivre heureux*, en hamonie avec les ressources disponibles.</p>
        <div class="opacity-100 md:opacity-0 transition group-hover:opacity-100 underline pt-4 group-hover:text-hover">En savoir plus...</div>
      </div>

  </section>
</nuxt-link>

<nuxt-link to="/la-monnaie-libre-et-la-democratie" class="group block mt-6 lg:mt-12 p-8 hover:shadow-xl rounded-lg transition transform hover:-translate-y-1">
  <section class="flex flex-col md:flex-row items-center gap-8 flex-col-reverse">
    <div>
      <h2 class="text-purple-800 dark:text-purple-600 text-2xl lg:text-3xl mb-4">La démocratie</h2>
      <p class="text-base lg:text-lg">"Laissez-moi créer la monnaie et contrôler la circulation des crédits, et je me fiche de qui écrit les lois - let me issue the money and control credits, then I care not who writes the law". Cette phrase a réellement été dite un jour par un homme, et "on" lui a accordé ce petit privilège. La création monétaire est un pouvoir, la monnaie libre le rend à chacun(e). On peut y voir un début de démocratie, au-delà de tout discours.</p>
      <div class="opacity-100 md:opacity-0 transition group-hover:opacity-100 underline pt-4 group-hover:text-hover">En savoir plus...</div>
    </div>
    <img src="/img/work-life-balance.svg" class="w-3/5 md:w-1/4" />
  </section>
</nuxt-link>

<nuxt-link to="/la-monnaie-libre-et-les-crypto-monnaies" class="group block mt-6 lg:mt-12 p-8 hover:shadow-xl rounded-lg transition transform hover:-translate-y-1">
  <section class="flex flex-col md:flex-row items-center gap-8">
    <img src="/img/High-quality-products.svg" class="w-3/5 md:w-1/3" />
    <div>
      <h2 class="text-purple-800 dark:text-purple-600 text-2xl lg:text-3xl mb-4">Les crypto-monnaies</h2>
      <p class="text-base lg:text-lg">La Ğ1 repose sur une "chaine de blocs chiffrée - blockchain", deux en fait, comme les maintenant fameuses crypto-monnaies. 
Mais son modèle spécifique de création monétaire en fait une monnaie bien plus équitable que toute autre "crypto". Tout le monde a le même accès à la création monétaire, en tout lieu et en tout temps, le flux monétaire est directement corrélé au flux de la vie, de chaque vie humaine.</p>
      <div class="opacity-100 md:opacity-0 transition group-hover:opacity-100 underline pt-4 group-hover:text-hover">En savoir plus...</div>
    </div>
  </section>
</nuxt-link>

<nuxt-link to="/la-monnaie-libre-et-les-monnaies-locales" class="group block mt-6 lg:mt-12 p-8 hover:shadow-xl rounded-lg transition transform hover:-translate-y-1">
  <section class="flex flex-col md:flex-row items-center gap-8 flex-col-reverse">
    <div>
      <h2 class="text-purple-800 dark:text-purple-600 text-2xl lg:text-3xl mb-4">Les monnaies locales</h2>
      <p class="text-base lg:text-lg">Sauf exceptions, les monnaies locales sont indexées à l'Euro. Ce n'est pas un changement de référentiel, juste un "changement de couleur" du billet. Dans son bassin de vie il est possible d'utiliser la monnaie libre comme une monnaie locale, ou un SEL - système d'échanges local, mais elle est fondamentalement autre chose ; la monnaie libre va bien au-delà.</p>
      <div class="opacity-100 md:opacity-0 transition group-hover:opacity-100 underline pt-4 group-hover:text-hover">En savoir plus...</div>
    </div>
    <img src="/img/MLC.svg" class="w-3/5 md:w-1/3" />
  </section>
</nuxt-link>