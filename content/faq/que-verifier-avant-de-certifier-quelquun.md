---
title: Que vérifier avant de certifier quelqu'un ?
description: La licence doit être respectée
---
## Le respect de la licence.   
Il est important que la [licence](https://duniter.fr/wiki/g1/licence-txt/) soit respectée pour que la monnaie reste digne de confiance. Sans confiance, la monnaie perd toute sa "valeur".  
Donc il faut s'assurer que la personne a bien lue et compris la [licence](/licence-g1).  

## Connaître la personne. 
À chacun de voir ce qu'il entends par "connaître" une personne, de nombreux indices sont dans la licence.   
Il s'agit surtout de s'assurer qu'on pourra, en cas de doute, vérifier que la personne ne possède pas deux comptes cocréateurs.

## S'assurer qu'elle maitrise son compte.
Il faut être sûr que la personne sache se connecter à son compte. Le plus simple est de lui envoyer quelques Ğ1, et lui demander de vous les renvoyer ou le transférer sur le compte de son choix.  
Si la personne n'y arrive pas, ne la certifiez pas, voyez avec elle là où elle fait une erreur. 

## Le document de révocation.
Actuellement, un bug empêche le téléchargement de document de révocation à partir d'un téléphone. 
Si la personne ne sait pas où se trouve son document de révocation, il ne faut pas la certifier tout de suite, mais l'aider à télécharger et sauvegarder ce document. 

## Vérifiez votre disponibilité
Vos certifications émises ne sont validées qu'au rythme d'une tous les 5 jours. Si vous émettez une certification avant ce délai, elle restera en attente en piscine.   
** Évitez d'encombrer les piscines** cela ralenti le processus d'entrée des nouveaux.  
Vous pouvez vérifier la date de dernière validation d'une certification en regardant vos notifications dans césium+, vous verrez "votre certification à untel a été effectuée" suivi d'une date et heure. Vous êtes indisponibles pendant 5 jours à partir de cette heure.   
Cela ne sert à rien d'émettre une certification en avance, la fin de validité (2 ans) est comptée depuis la date d'émission et non de validation.  

## Si la personne est déjà membre.
**Il faut quand même faire ces vérifications**, car trop nombreux sont ceux qui font confiance sans vérifier par eux mêmes.  
On peut s’apercevoir que la personne ne se souvient plus d'où elle a sauvegardé son document de révocation. Ou qu'elle a encore des lacunes dans la compréhension de la licence. 

## Si la personne n'est pas encore membre
**Éviter d'encombrer les piscines** Inutile d'émettre une certification qui ne passera pas, cela encombre les piscines et ralenti le traitement des certifications validables. 
- Vérifier que la personne à bien au moins 4 autres certificateurs
- Vérifier que tous ces certificateurs sont disponibles avec [Wotwizard-UI](https://wotwizard.axiom-team.fr/fr/membres)
- Vérifier que ces certificateurs suffisent pour respecter la règle de distance avec le [calculateur](https://wot-wizard.duniter.org/23calculator)   

Si les conditions ne sont pas remplies, invitez la personne à trouver d'autres certificateurs (de préférence en dehors du groupe local)


## La qualité avant la quantité. 
Il est préférable d'avoir une petite toile de grande confiance qu'une grande toile de petite confiance.    
Il n'y a pas d'urgence à devenir cocréateur, le plus important est de comprendre comment fonctionne la monnaie libre. 
Si la personne participe régulièrement aux rencontres, elle se fera bien connaître et obtiendra ses certifications sans mal. 
