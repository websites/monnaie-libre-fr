---
title: La base théorique de la Ğ1 est-elle démontrée ?
description: "La Ğ1 est une expérimentation. "
---
La Ğ1 est basée sur la Théorie Relative de la Monnaie (<lexique>TRM</lexique>), démonstration de la possibilité d'existence des monnaies libres telles qu'elle les définit.

Pour vérifier la validité de la démonstration et des outils mathématiques qu'elle fournit, on a réalisé des expériences :

* Le Ğeconomicus est un jeu de simulation d'économie. Ses résultats montrent une efficacité de la monnaie libre supérieure à celle de la monnaie dette (€) pour résoudre les inégalités, favoriser la production de valeurs et les échanges.
* La Ğ1 est la première application de la monnaie libre à moyenne échelle. Même si l'échantillon est encore trop faible pour tirer des conclusions quant aux inégalités, on constate qu'elle fonctionne.

