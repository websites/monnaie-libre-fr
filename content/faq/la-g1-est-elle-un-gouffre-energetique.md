---
title: La Ğ1 est-elle un gouffre énergétique ?
description: "La monnaie Ğ1 est sobre en énergie. "
---
Les serveurs qui gèrent la monnaie tournent très bien sur des ordinateurs de taille très modeste du type RaspberryPi, un nano-ordinateur qui consomme (seulement) 5W d’électricité.

Cela fait de la Ğ1 une monnaie low-tech, très sobre énergétiquement, et dont l’emprunte écologique est très faible (bien plus faible que celle de l’euro, et infiniment inférieure à celle du Bitcoin, où des fermes de calcul gigantesques sont en compétition les unes contre les autres et se disputent la création monétaire).

Plus d'explication sur le site de duniter [est il energivore](https://duniter.fr/faq/duniter/duniter-est-il-energivore/)

43 minutes de vidéo explicative sur la blockchain duniter
<!--iframe width="100%" height="405" sandbox="allow-same-origin allow-scripts allow-popups" src="https://peertube.normandie-libre.fr/videos/embed/49368972-ad4a-46b3-825b-4670dd812f41" frameborder="0" allowfullscreen></iframe-->
