---
title: Comment faire sans smartphone ?
description: De chez soi et à l'extérieur, recevoir et acheter
---

## Si vous avez un ordinateur

Depuis chez vous, gérez votre compte avec votre ordinateur en utilisant une [application dédiée](/ressources?filters=App).

À l'extérieur, pour recevoir des paiements comme vendre des choses sur un ğmarché, il suffit d'imprimer votre QR-code.
Dans césium cliquez sur le QR-code pour l'agrandir, faites une copie d'écran et imprimer le. Découper l'image avant de l'imprimer.
Si vous n'avez pas d'imprimante vous pouvez demander à quelqu'un de le faire pour vous. En plusieurs exemplaires tant qu'à faire.

Pour faire des achats deux possibilités :

1. Montrez au vendeur qu'il peut vous faire confiance, donnez vos coordonnées mail et téléphone, ou demandez-lui qu'il vous identifie sur Césium. Il peut alors vous envoyer sa clé publique.
   Ne copiez jamais manuellement une clé, il y trop de risque d'erreur.
   **Vous paierez une fois rentré chez vous.**

2. Imprimez des billets.
   Cela consiste à créer des portefeuilles papier que vous créditez de quelques DUğ1. <https://forum.monnaie-libre.fr/t/nouveau-g1-billets/14529> ou <https://forum.monnaie-libre.fr/t/le-g1billet-nantais/14977>
   C'est un peu technique, faites vous aidez par un informaticien local.
   Vous donnez ces portefeuilles au vendeur qui peut les reverser sur le compte de son choix.
   Attention n'imprimer jamais deux fois le même billet.

## Si vous n'avez pas d'ordinateur.

Un ordinateur est indispensable pour créer et gérer vos comptes Ğ1.
**Voyez auprès des associations locales, ou de vos proches.**

<alert type="danger"> Saisir ses identifiants et mot de passe sur l'ordinateur d'un tiers, c'est faire confiance à ce tiers et au fait qu'il soit bien protéger du piratage.
Ne saisissez jamais vos mots de passe si vous avez un doute. </alert>
