---
title: Ne vaudrait-il pas mieux abolir la monnaie ?
description: La monnaie a existé bien avant les banques
---
Vous êtes libre de l’abolir pour vous-même si vous le souhaitez, mais il y aura probablement peu de gens prêts à vous suivre.

## En petite communauté

Même quand on n’utilise pas de monnaie, il est rare qu’on ne compte pas.

Certaines communautés prétendent vivre “sans monnaie”, mais il y a toujours un livre de compte quelque part, au moins dans la tête de chaque membre de la communauté.

Ce genre de communauté “sans monnaie” existe dans des conditions très particulières : elles sont généralement très petites et isolées du reste du monde, par exemple par la géographie .

## Entre amis

S’il arrive qu’on se rende des services entre “amis”, c’est uniquement parce qu’on a rarement plus de 150 “amis”.

Même entre voisins, la réciprocité peut prédominer.

De la même façon, à la campagne, on se rend des services entre voisins, mais si vous osez dire merci, on vous dira probablement que :

> Il n’y a pas de merci.

Car dire “merci” est une façon d’éteindre une dette. Mais à la campagne, c’est :

> Un donné pour un rendu.

## La monnaie : source de tous les mots ?

*Idéalement, la monnaie ne devrait être qu’un moyen de mesurer les échanges, de façon totalement neutre.*
*Et c’est ce que fait la monnaie libre !*

Ce n’est certainement pas l’expérience que vous avez eue jusqu’à présent, mais si vous dites à un <lexique>juniste</lexique> que

> l’argent pourrit tout

il vous répondra probablement que :

> ça dépend du système monétaire

ou que :

> l’argent est un métal, qui ne sert plus de monnaie depuis bien longtemps

ou encore que :

> ce n’est pas surprenant d’être prêt à mettre sa conscience de côté pour capter de la monnaie, quand on vit dans un système monnaie-dette où le manque de monnaie stresse tout le monde