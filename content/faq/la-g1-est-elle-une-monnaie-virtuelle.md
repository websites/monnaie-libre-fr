---
title: "La Ğ1 est-elle une « monnaie virtuelle » ?"
description: "Est “virtuel” uniquement ce qui n’existe pas."
---
La Ğ1 existe bien, tout comme les milliers de transactions effectuées par ses utilisateurs depuis sa création, le 8 mars 2017.

Le fait que les Ğ1 utilisées soient stockées sur des ordinateurs ne la rendent pas virtuelles.

95% des euros en circulations n’existent que sous forme numérique. Pour vous convaincre de ce chiffre, comptez les euros présents dans votre portefeuille et rapportez-les au total des euros que vous possédez : vos pièces et vos billets représentent-ils ne serait-ce que 5% du total des euros que vous possédez ?

Bien-sûr, en aucun cas tous les euros affichés sur les soldes de vos comptes courants et épargne ont leur pendant sous forme de billets ou de pièces, et encore moins de lingot d’or. Sont-ils pour autant “virtuels” ?

Comprenez bien que le solde de votre compte en banque est la quantité de monnaie que vous doit la banque. Cela ne veut pas dire qu'elle possède cette monnaie. En y regardant de plus près la monnaie virtuelle, c'est l'Euro !