---
title: " Différences entre la Ğ1 et les monnaies locales complémentaires (MLC)"
description: Les MLC sont des euros déguisés
---
Vous pouvez consulter la page du site sur les [monnaies locales](https://monnaie-libre.fr/la-monnaie-libre-et-les-monnaies-locales).

Les monnaies locales complémentaires, telles que celles autorisées par [la loi du 31 juillet 2014](https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000029313554), sont adossées à l'Euro.

À chaque unité monétaire d'une MLC est associé un Euro sur un compte en banque. Pour acquérir des unités de monnaies locales, il faut donc le plus souvent les acheter en payant en Euros.   

Pour acquérir des Ğ1, il faut vendre des biens ou services (ou des euros si vous avez envie), mais chacun peut devenir cocréateur de monnaie. 

Alors que 1 Sol-Violette = 1 Euro, le cours Ğ1/Euro n’est pas défini.
Si des utilisateurs veulent échanger des Euros contre des Ğ1, ils conviennent librement du cours qu'ils veulent à l'instant de l'échange.