---
title: " Pourquoi ça s’appelle la monnaie “libre” ?"
description: La monnaie qui rend libre
---
Dans la Théorie Relative de la Monnaie, Stéphane Laborde définit la monnaie libre comme respectant les quatre libertés économiques suivantes :

0. La liberté de choix de son système monétaire
1. La liberté d’utiliser les ressources
2. La liberté d’estimation et de production de toute valeur économique
3. La liberté d’échanger, comptabiliser et afficher ses prix “dans la monnaie”

Pour plus de détails, visitez le [site de la TRM appendice 1]( https://trm.creationmonetaire.info/appendice-1.html)

**La liberté s’entend toujours aux sens :**

* non-nuisance d’autrui vis-à-vis de soi-même
* non-nuisance de soi-même vis-à-vis d’autrui.

**Dans un système monétaire libre :**

* la symétrie spatiale permet à un individu d’éviter de nuire à ses contemporains,
* la symétrie temporelle permet à une génération d’éviter de nuire aux suivantes.

Les 4 libertés fondamentales de la monnaie libre sont directement inspirées de celles du [logiciel libre](https://vive-gnulinux.fr.cr/logiciel-libre/4-libertes/), telles que les a définies Richard Stallman.   
![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Galuel_RMS_-_free_as_free_speech%2C_not_as_free_beer.png/800px-Galuel_RMS_-_free_as_free_speech%2C_not_as_free_beer.png "Stéphane Laborde en compagnie de Richard Stallman pour illustrer la citation \"free as in free speech, not as in free beer\"")
Stéphane Laborde en compagnie de Richard Stallman pour illustrer la citation :  
“Free as in free speech, not as in free beer”  
(source : [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Galuel_RMS_-_free_as_free_speech,_not_as_free_beer.png?uselang=fr), licence Creative Commons BY SA)
Navigation
