---
title: Comment faire quand on est seul
description: "il faut créer une communauté. "
---
## Créez un groupe local. 
La monnaie est avant tout un intermédiaire d'échange.  
Donc si vous êtes seul cette monnaie ne vous servira à rien. 
Il faut que vous soyez un groupe de personnes pour pouvoir faire des échanges. 

## Élargissez vos connaissances. 
Sans sortir de chez vous, vous pouvez participer à des visioconférences. Trouvez [les visios sur le forum](https://forum.monnaie-libre.fr/t/organisation-dune-chaine-visios/13044)  
Cela pour en apprendre plus sur la monnaie et aussi faire connaissance avec d'autres junistes. 
Vous n'obtiendrez pas de certifications par ce moyen, mais lors d'une rencontre, vous reconnaîtrez des personnes connues. Cela facilitera votre certification, surtout si vous avez aussi échangé par téléphone et par courrier. 

Le fait d'être connu par des personnes loin de chez vous vous permettra d'augmenter votre indice de proximité, ou [qualité de dossier](/faq/quest-ce-que-la-qualite-de-membre-et-de-dossier). 

## Attirez du monde. 
Vous pouvez proposer des biens ou des services sur [ğchange](https://www.gchange.fr/). Ce qui vous permettra de commencer à faire des échanges.    
Si vous avez de l'espace disponible vous pouvez aussi proposer un hébergement sur [airbnjune](https://airbnjune.org/). C'est un très bon moyen de faire venir des junistes chez vous. 

## Parlez en autour de vous. 

Si vous n'avez pas de connaissance dans la monnaie libre, il faut  soit faire connaissance avec des personnes qui l'utilisent, soit amener vos connaissances à utiliser cette monnaie.  
Vous pouvez en parler à vos amis, aux associations autour de vous, AMAP et Système d'Échange Local, vous y trouverez des gens déjà sensibilisés aux problèmes du système monétaire actuel. 

Vous pouvez proposer des rencontres près de chez vous, et indiquer ces rencontres sur [le forum](https://forum.monnaie-libre.fr/).  
Commencez par des rencontres dans des lieux publics : bar parc ou autres, chez vous ou chez un ami. 

Vous n'aurez pas toujours les réponses à toutes les questions, mais vous ferez connaissance (cela sera utile pour les certifications futures).  
Vous pouvez parler des visios et des vidéos qui pourront apporter des réponses à vos questions.  
Et avec ce petit groupe, vous pourrez covoiturer pour aller aux rencontres un peu plus loin. Et enfin rencontrer les personnes avec qui vous avez parlé en visio. 

## Comment ne pas rater une rencontre 

Voir le petit tuto fait sur le forum par un Breton : [
Comment ne pas rater une rencontre ?](https://forum.monnaie-libre.fr/t/comment-ne-pas-rater-une-rencontre/7408). Adaptez-le pour votre bassin de vie.   
Voir aussi le sujet épinglé en haut de la catégorie de votre région, il y a souvent des liens ou des listes de diffusion où vous pouvez vous inscrire. 

## Ce sera long
Ne vous découragez pas, vous ne serez peut-être que 2 à votre première rencontre. Mais cela permettra de faire savoir que les choses bougent dans votre coin. 
Il faudra du temps avant que chaque rencontre amène une dizaine de personnes. 
Vous serez des pionniers. Courage. 

*Ou alors vous attendez tranquillement que d'autres fassent tout ce travail pour vous. **(Ce sera encore plus long)** * 