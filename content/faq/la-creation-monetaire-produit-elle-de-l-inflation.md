---
title: La création monétaire produit-elle de l'inflation ?
description: Est-ce que le pouvoir d'achat diminue avec la création monétaire ?
---
## La création monétaire me fait-elle perdre du pouvoir d'achat ?

Dans la tête de la plupart des gens se trouve cette idée :
>
> création monétaire = planche à billet = inflation

Le terme "inflation" renvoie à l'augmentation générale du niveau des prix.

Dans les systèmes monétaires les plus répandus (monnaie dette), cette augmentation générale du niveau des prix est généralement attribuée à l'augmentation de la masse monétaire.

En monnaie libre, la masse monétaire augmente à un rythme prévisible : 10% par an.

Est-ce que ça veut dire que les prix de toutes les denrées augmentent de 10% chaque année ?

Oui... et non.

Pour comprendre ça, plaçons-nous dans un système que vous connaissez déjà : le système Euro.

#### L'exemple de la pizza

Imaginez qu'en 2019, vous puissiez créer 150 € chaque mois, soit environ 5 € par jour.

Appelons ces 5 € que vous créez chaque jour Dividende Universel (ou DU).

Chaque jeudi soir, vous sortez avec vos amis pour manger chacun une pizza, que vous payez 10 €

Par rapport à la monnaie que vous créez chaque jour, une pizza représente :

**10 / 5 = 2 DU**

Que se passera-t-il en 2020 ?

La masse monétaire augmentera de 10 %.

Votre pizzaïolo préféré répercutera peut-être
l'augmentation de la masse monétaire sur ses prix et, si c'est le cas,
vous paierez votre pizza 11 € au lieu de 10 €.

Oui, mais, attention :

C'est vous qui serez responsable de cette augmentation de la masse monétaire !

Concrètement, au lieu de créer 5 € chaque jour, vous créerez maintenant 5,50 €.

Par rapport à la monnaie que vous créez chaque jour, une pizza représentera donc :

**11 / 5,5 = 2 DU**

Aurez-vous perdu en pouvoir d'achat ?

#### Une astuce pour que les prix ne bougeront pas

Ne vous inquiétez pas pour ça :

Les acteurs économiques choisiront probablement
d'afficher les prix en DU et, comme vous venez de le constater avec
l'exemple de la pizza, l'augmentation de la masse monétaire ne fera pas
changer le prix en DU.

## La création monétaire fait-elle fondre mon épargne ?

Ça peut être vrai dans certains cas particuliers :

Pour ceux dont le solde est au-dessus du solde moyen par
membre, si les prix ne diminuent en DU, l'augmentation du nombre de G1
dans un DU va diminuer le nombre de DU possédés.

Pour ceux dont le solde est en-dessous du solde moyen par membre, le versement du DU va augmenter le nombre de DU possédés.

Au fil du temps, les soldes de chacun convergent vers le solde moyen.

C'est l'une des grandes propriétés de la monnaie libre : deux individus dont les soldes seraient éloignés de part et d'autres de la moyenne verraient leurs soldes converger s'ils n'avaient aucune activité économique pendant 40 ans : [![](https://monnaie-libre.fr/wp-content/uploads/2019/02/convergence-des-soldes.png)][0]

Il faut garder une chose en tête cependant : les valeurs économiques varient dans le temps.

En règle générale, la valeur des biens produits augmente avec le temps.

Un ordinateur portable de 2019 est bien supérieur à un ordinateur fixe de 2009, alors qu'il est vendu bien moins cher, même si on compte en absolu (Euros), et alors même que la masse monétaire Euro a beaucoup augmenté dans cet intervalle de 10 ans.

[0]: https://monnaie-libre.fr/wp-content/uploads/2019/02/convergence-des-soldes.png