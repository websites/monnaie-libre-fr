---
title: L'unité c'est le DUğ1 ou la June Ğ1
description: "La Ğ1 pour les comptes, le DUğ1 pour les échanges. "
---
## Le DUğ1 est l'invariant temporel.

Quand on parle d'égalité spatiotemporelle dans la monnaie libre, on compte en DUğ1. Chaque être humain va créer autant de DUğ1.   
Le DUğ1 est une petite portion de la masse monétaire globale, cette portion est réévaluée tous les six mois pour représenter toujours le même pourcentage de la masse moyenne globale.  
Quel que soit le montant du DUğ1, il représentera toujours une journée de création monétaire individuelle. 

## La Ğ1, ce sont les chiffres dans la base de données.

Chaque DUğ1 est un nombre de Ğ1, c'est ce nombre de Ğ1 qui est inscrit dans la base de données.  
Voyez cela comme votre date de naissance inscrite dans une base de données pour afficher votre âge. Ce n'est pas vraiment votre date de naissance qui intéresse la banque ou l'administration, mais votre âge. La date de naissance stockée ne sert qu'à calculer votre âge au moment de l'affichage.  
Eh bien, c'est pareil pour le nombre de Ğ1 stocké dans la blockchain, il est utile pour calculer votre "fortune" affichée en DUğ1.

## Le DUğ1 comme unité d'échange.

En affichant vos prix en DUğ1 vous n'avez plus besoin de scruter l'augmentation de la masse monétaire, avec la réévaluation semestrielle vos prix suivent l'augmentation de la masse monétaire "naturellement". Un peu comme si vous affichiez vos prix en Smic horaire au lieu d'unités monétaire (sauf que le Smic n'est pas vraiment un invariant)  
De plus cela permettra à nos descendants de comparé les prix dans le temps. Il est difficile de comparer les prix d'aujourd'hui avec ceux du passé, la masse monétaire était différente, les salaires aussi, il nous manque une unité de valeur commune.  

*Le DUğ1 est notre unité de valeur commune à travers l'espace et le temps.* 

## Paramétrez Cesium. 

Dans les paramètres de césium, vous pouvez choisir l'affichage en DUğ1.  
Faites-le, vous constaterez que vos échanges du passé ont de moins en moins de valeur, en DUğ1 actualisé. Il manque un affichage en DUğ1 d'époque à côté du DUğ1 actualisé (peut-être un jour). 
 