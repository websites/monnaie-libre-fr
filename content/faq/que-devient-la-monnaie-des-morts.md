---
title: "Que devient la monnaie des morts ? "
description: "Elle devient insignifiante avec le temps. "
---
Si la personne disparue n'a pas pris soin de laisser ses codes d'accès à ses héritiers, la monnaie reste sur le compte. L'adhésion ne pouvant être renouvelée, à l'expiration de celle-ci (moins de 1 an) le compte ne créera plus de monnaie.\
Si les héritiers ont accès au compte, ils peuvent récupérer la monnaie par transfert, il faut alors révoquer le compte pour respecter la licence.    

Grâce au dividende universel, la quantité de monnaie continue de croître de manière exponentielle quantitativement.
La monnaie créée par les personnes décédées représente une part de plus en faible de la monnaie existante, jusqu'à devenir insignifiante au bout d'environ 40 ans (en <lexique>monnaie pleine</lexique>)

![La courbe monte de plus en plus lentement, et baisse dès qu'il n'y a plus de création de monnaie](https://duniter.github.io/prez_rmll2017/images/evolution_compte.png "Évolution d'un compte membre en DU annuel")
L'image représente la part de monnaie crée en nombre de DU annuels cumulés (pour un DU de 10% par an) pour une personne décédée à 80 ans.  
Le nombre de DU augmente rapidement et se stabilise vers 40 ans, car la création compense la réévaluation du DU. À partir du décès, cette réévaluation n'est plus compensée par la création.