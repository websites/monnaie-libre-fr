---
title: DUNITER V2 em português
description: Versão 2 para o software Ğ1
---
<div style = "display:inline-block;float:left;">
<img src="/uploads/demenagement-v1-v2.jpg" width=400px align="center" style="margin:auto;"/>
</div>
<div style = "display:inline-block;float:left; padding-left : 20px;">
<p style="color:#34A1FF;" id="langues">Outras línguas &nbsp; &nbsp;&nbsp;</p>

[En français](/maj-v2/)

[En español](/duniter-v2-en-espanol/)

[In-english](/duniter-v2-in-english/)

[In-italiano](/duniter-v2-in-italiano/)

[Auf-deutsch](/duniter-v2-auf-deutsch/)

</div>

<p style="float:left;"><strong> Tradução automática com \[deepl](https://www.deepl.com/fr/translator)</strong> </p>

<div style = "display :inline-block; margin:5px auto 5px; text-align:center; width:400px; border:solid #3FB2FF 1px; background-color:#3FB2FF;border-radius: 10px; padding: 1em;">
<a style="font-size:1.5em; font-weight: bold; color: white;" href="https://www.helloasso.com/associations/axiom-team/collectes/finalisation-de-cesium-v2-et-duniter-v2">financiamento coletivo<img src="/uploads/crowfunding100.png" style="padding:5px;margin:auto;"/></a>
</div>

<h2 style="color:#34A1FF;" id="plan">Plano de Comunicação</h2>

Quando será informado sobre o progresso do projeto?

1. **<p style="font-size:1.1em;"><strong style="color:#F7A212;">Outubro:**</strong> [porquê mudar?](#why)</ p >
2. **<p style="font-size:1.1em;"><strong style="color:#F7A212;">Novembro – Dezembro:** </strong> [o que vai mudar](#evolutions) < /p>
3. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Janeiro:**</strong> [que não irá alterar](#identique) <br/> [e  **como este vai acontecer**](#bascule) </p>
4. **<p style="font-size:1.1em;"><strong style="color:#F7A212;">Fevereiro:**</strong> software de satélite: **descobrir, testar e utilizar* *</p>

</div>
<hr style="border: 2px solid #34A1FF;margin:50px automático;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="300" align ="center" >

<h2 style="color:#34A1FF;" id="why">Porquê mudar?</h2>

<h3 style ="color: #F7A212" id="inconvenients">1/ Desvantagens da V1</h3>

#### As primeiras desvantagens são visíveis principalmente para os programadores:

* A principal razão desta migração: **Dificuldades em manter o <lexicon>Duniter</lexicon> V1 (corrigir bugs e fazer melhorias).**
* Questões de segurança,

  * O Duniter pode ser facilmente bloqueado (ataque de saturação da rede).
  * Todas as contas de membros são potencialmente ferreiras[^1], mas algumas são facilmente hackeáveis, o que permitiria a um atacante comprometer potencialmente toda a blockchain
* Obrigação dos <lexique title="blacksmith">ferreiros</lexique> de fazerem ressincronizações manualmente para que continue a funcionar.
* Risco de fork* se os <lexicon title="blacksmith">smiths</lexicon> não actualizarem o <lexicon>Duniter</lexicon> rapidamente no caso de uma correcção de um bug.

[^1]: um ferreiro é um membro do TDC (Web of Trust) que descarregou o software Duniter para o seu dispositivo (computador ou outro), que se torna um servidor (ou nó) para operar o June.

<alert type="info">

<details> 
  <summary> *Clique aqui para saber: O que é um fork</summary>

Um fork é um fork quando dois nós calculam o mesmo bloco ao mesmo tempo, resultando em 2 versões do mesmo bloco, portanto, 2 versões da blockchain a seguir. \
Uma bifurcação pode resolver-se sozinha, sem perda de dados (esta é uma operação normal quando os nós estão atualizados),
Pode ser resolvido com perda de dados, caso que vemos com bastante frequência em V1 (Quando há problemas de sincronização ou de actualização de nós).\
Pode não ser resolvido automaticamente, o que leva a uma divisão da blockchain (alguns utilizadores não veem a mesma coisa que outros).\
Se o fork não for resolvido (automática ou manualmente) poderá resultar na divisão da comunidade\
**Mais detalhes seguindo estes links:**\
https://journalducoin.com/lexique/fork/\
https://www.coinbase.com/fr/learn/crypto-basics/what-is-a-fork\
https://fr.cryptonews.com/exclusives/c-est-quoi-un-fork-5803.htm

</details>
</alert>

#### Todos os utilizadores já notaram outras desvantagens:

<img style="float:right; width:15%;" src="../uploads/escargot-inconvenients.png"/>

* Execução lenta.

  * Muito tempo para exibir operações
  * Entre 5 e 10 minutos para processar uma transação, 30 minutos para a validar
* Problemas de sincronização de pools[^1]: Visualizações diferentes de um nó para outro, porque nenhum nó tem uma visão global dos pools.
* Transações ou certificações que não são aprovadas.
  *Certificações que desaparecem.
* Dificuldade em inserir novos (problemas de sincronização entre certificadoras para ter 5 certificadoras disponíveis em simultâneo)[^2].\
    [^1]: pool: se um membro emitir várias certificações em menos de 5 dias, estas permanecerão suspensas e só serão registadas na blockchain uma a uma a cada 5 dias (conforme previsto nas regras do TDC mas sem para que possamos saber em que ordem que passarão).\
    [^2]: Sabendo que as certificações dos membros vêm antes das dos candidatos, um futuro membro terá, portanto, de esperar até que os outros sejam aprovados antes de ser certificado.

<h3 style ="color: #F7A212" id="advantages">2/ Vantagens da v2</h3>
  <img style="float:right; width:18%;" src="../uploads/papillon-avantages.png"/>

* Fácil manutenção e atualizações para os programadores
* Atualização automática sem fork para ferreiros
  *(existirá sempre a possibilidade de recusar uma atualização, mas será voluntária e não mais por esquecimento)*.
* Um bloco a cada 6 segundos

  * Transação validada em 30 segundos
  * Tempo de resposta significativamente melhorado
* Sincronização de nós mais rápida e fiável
  *Certificações validadas de imediato (sem piscinas)*
  Entrada mais fácil de novos (já não há necessidade de sincronização entre as primeiras 5 certificadoras)

<h3 style ="color: #F7A212" id="evolution">3/ Evolução do software, não da moeda</h3>

O software está em constante evolução; até à data, estamos na versão 1.8.7 do Duniter e 1.7.13 do Cesium.
Estes desenvolvimentos foram feitos para se manterem compatíveis com as versões anteriores, e foram sempre implementados sem problemas.

A versão 2 é ainda software livre, é uma nova blockchain que começará com todos os dados da blockchain Ğ1 V1 no momento da mudança (contas, transações, certificações, etc.)\
**Encontrará todos os seus Ğ1s, as suas certificações e transações.**

<alert type="info">
Para os dados do Cesium+ (perfis, mensagens, notificações, etc.), estão em curso, e talvez não desde o início!
</alert>

Uma nova blockchain envolve novo software (Cesium 2, Gecko, Tikka, G1nkgo 2, etc.)

Não será possível trocar entre dois blockchains diferentes, pelo que, após a data de mudança, terá de ter instalada a nova versão do software para continuar a trocar ğ1 com o resto da comunidade.

**Para não perder esta alteração, esteja atento à informação neste site, no fórum e nas “redes sociais”!**

<h2 id="flyers1"><a href="https://forum.monnaie-libre.fr/uploads/short-url/o0csAWriBi0EOTG9khia6vb7Zmc.pdf" target="_blank" width="50%" style="color:#34A1FF; font-weight: bold;">Folheto frente e verso para partilhar <img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>

<p><strong>Clique na imagem para ampliar</strong></p>

<div style="float:left; margin:0 1em 0;"><a href="/uploads/porquê-mudar-para-português_joy5081_page_1.jpg" target="_blank">
<img src="/uploads/porquê-mudar-para-português_joy5081_page_1.jpg" width="200"/></a><br/>traduzido por @Kristo</div>

<div style="float:left; margin:0 1em 0;"><a href="/uploads/porquê-mudar-para-português_joy5081_page_2.jpg" target="_blank">
<img src="/uploads/porquê-mudar-para-português_joy5081_page_2.jpg" width="200"/></a><br/>traduzido por @Kristo</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >

<h2 style="color:#34a1ff" id="evolutions"> Evoluções (o que irá mudar para os utilizadores)</h2>

<h3 style="color:#f7a212" id="software">1/ Novo software</h3>

Para poder continuar a trocar com outros junistas, é essencial que todos mudem de versão na primeira ligação após o início do blockchain V2.

* **Césio** O software mais utilizado oferecerá a sua versão V2. Não há planos para mudar para o Duniter V2 até que este software esteja pronto.
* **Gecko**, mais virado para smartphones, para pagamentos, está pronto, com alguns ajustes.
* **Tikka** em computador, contabilidade orientada para profissionais, em desenvolvimento.
* **Ğ1nko** uma carteira que facilita as transações nos mercados G. A versão 2 pode estar pronta para migração
* **Ğ1superbot** também terá a sua versão para Duniter V2.
* **Duniter-Connect**, extensão do browser que permite transferências a partir de qualquer site, evolui à medida que o Duniter se desenvolve.
* **Ğcli** O cliente de linha de comandos (para técnicos) semelhante ao Silkaj, também evolui à medida que o Duniter se desenvolve
  *...

- - -

<h3 style="color:#f7a212" id="cles">2/ Nova forma de Chaves Públicas</h3>
Com a nova blockchain Duniter V2, aquilo a que chamamos chaves públicas alteram a sua codificação. Assim, a sua chave pública já não será parecida com a que conhece. Provavelmente terá de reimprimir o seu código QR para quem o imprimiu.

<img style="float:right; width:15%;" src="../uploads/adresse-cle.png"/>

Falaremos mais sobre o endereço do que sobre a chave, todos começarão por "g1...", por isso use o fim do endereço em vez do início, para reconhecer a sua conta.

No Duniter v2 iremos encorajar o uso de um endereço em vez de uma simples chave pública. O endereço ajuda a evitar erros ao copiar e utilizar uma chave na rede errada.

**Para o ajudar, para as contas criadas antes da atualização, o Cesium2 irá apresentar a antiga “chave pública” como um lembrete. O mesmo para o G1nkgo, que irá exibir esta chave antiga.**

- - -

<h3 style="color:#f7a212" id="depot">3/ Depósito existencial</h3>

No blockchain v1, as contas com menos de 1 Ğ1 desaparecem sem que o utilizador seja notificado (destruição da moeda).\
No blockchain v2 será impossível descer abaixo da marca 1 × 1 sem solicitar explicitamente o encerramento da conta.

- - -

<h3 style="color:#f7a212" id="piscinas">4/ Desaparecimento das piscinas</h3>

<img style="float:right; width:18%;" src="../uploads/nageur.png"/>

* As certificações serão consideradas imediatamente, quer a pessoa certificada esteja a aguardar outras certificações ou não.
  *Será respeitado o período de 5 dias entre duas certificações pois o DuniterV2 apenas permite uma certificação de 5 em 5 dias.
* Alguns clientes (como a Césium) podem oferecer a adição das suas intenções de certificação num “catálogo de endereços” (ainda não desenvolvido).

- - -

<h3 style="color:#f7a212" id="adhesions">5/ Processo de adesão (tornar-se membro cocriador de moeda)</h3>

* As novas contas serão apenas contas simples de carteira.
* Deve ter alguns Ğ1s para iniciar o processo de certificação. Será, portanto, impossível certificar uma conta com zero ğ1
* A primeira certificação constitui um convite para se tornar membro (sem necessidade de inscrição).
* A aceitação do convite consiste, para o recém-chegado, na escolha de um apelido no prazo de 48 horas (prazo que pode ainda sofrer alterações).
* Uma vez registado o apelido, são possíveis as seguintes certificações.
* Quando a conta tem 5 certificações respeitando a regra da distância torna-se co-criadora de moeda.
* Já não há necessidade de sincronização para as certificações (já não poderá certificar se não estiver disponível).
* O prazo de dois meses para a obtenção das primeiras cinco certificações respeitando a regra do distanciamento será ainda válido. (A duração do atraso pode sofrer alterações)
* Caso os prazos sejam ultrapassados, o certificador recupera a sua certificação do seu stock de certificações a emitir.

- - -

<h3 style="color:#f7a212" id="securite">6/ Novas contas mais seguras</h3>

A autenticação por identificador secreto e palavra-passe é pouco segura para uma moeda. (Hoje os seus bancos exigem que tenha autenticação dupla)

<img style="float:right; width:18%;" src="../uploads/coffre.png"/>

1. As contas antigas, com identificador secreto e palavra-passe, permanecerão sempre utilizáveis ​​com o Césio, como antes da transição para o V2.
2. **As novas contas serão criadas a partir de uma mnemónica de 12 palavras.** Estas 12 palavras devem ser mantidas cuidadosamente fora de vista, uma vez que permitem recuperar as suas contas noutros dispositivos ou outros softwares de gestão de carteiras (Cesium, Gecko, Tikka , etc.)
3. As várias contas podem ser criadas utilizando o mesmo mnemónico (isto constituirá o que chamamos de “cofre”). Um cofre pode conter várias contas ou apenas uma.
4. Depois de criado o cofre, um código de 4 ou 5 letras ou números será suficiente para aceder a todas as contas do cofre, desde que utilize o mesmo dispositivo.
5. Uma conta criada com mnemónica poderá ser utilizada em Cesium, Gecko Tikka, Gcli e provavelmente com outros softwares.   

- - -

<h3 style="color:#f7a212" id="migration">6 bis / Migração para uma conta mais segura</h3>

**Só para os junistas que quiserem.**

6. O Gecko foi concebido para funcionar apenas com contas criadas por mnemónica, por questões de segurança. Para outros softwares, nada de definitivo neste momento.
7. O Gecko oferece-se para “migrar” a sua conta antiga (id/mdp) para uma nova conta que criou anteriormente com uma mnemónica.
8. Isto pode permitir-lhe consolidar todas as suas contas num cofre único e mais fácil de usar.
9. A migração de uma conta de membro é a transferência da sua identidade com apelido, certificações, estatuto de membro ou não, e ğ1.
10. A migração de uma conta de carteira simples resume-se a uma transferência de todos os ğ1.
11. Uma vez migrada, a nova conta (com mnemónica) poderá ser utilizada para todas as suas transações independentemente do software de gestão de carteira (Césium, Gecko, Tikka, Gcli) e a conta antiga não será mais nada além de uma carteira vazia .
12. A conta antiga continua utilizável como qualquer carteira. Mas isso seria perder o sentido de ter migrado.

- - -

<h3 style="color:#f7a212" id="fees">7/ Taxas e quotas</h3>

#### A capacidade de um blockchain não é infinita.

Um blockchain necessita de poder computacional e espaço de armazenamento. Ambos, embora substanciais, não são ilimitados. Um possível ataque é a saturação do poder computacional com o envio de milhares de milhões de transações por segundo.
Outros blockchains cobram uma taxa por cada ação para impedir esta saturação de cálculos e espaço de armazenamento.

#### Mas o Ğ1 não é como os outros.

#### 1- As taxas só serão cobradas em caso de sobrecarga da blockchain.

* **Um número total de ações** (transações, certificações, adesão, etc.) **por bloco** foi avaliado como sendo o **limite de operação "normal"**. **Para além deste limite** o blockchain é considerado **saturação**.
  *Apenas se o número de ações num bloco **exceder este limite** serão cobradas **taxas**. Taxas estimadas em cerca de 0,015 DUĞ1, ou 17 Ğ1 por 100 transações.

#### 2- Além disso, os custos serão reembolsados ​​a todos os membros da web of trust *(esta é a força do nosso Blockchain)*

<img style="float:right; width:18%;" src="../uploads/blockchain_humain.png"/>

* É definida uma quota de ações por membro e por bloco (permitindo ainda muitas transações)
* As contas de não associados podem ser associadas a uma conta de associado e também ter as suas quotas reembolsadas, dentro do limite da quota por associado.
* Um membro pode fazer uma única transação, mas se, ao mesmo tempo, alguém lançar 1 milhão de transações em 1 milhão de contas, a blockchain ficará saturada e, portanto, serão cobradas taxas a este membro.
* Depois será reembolsado porque não ultrapassa a quota por associado.
* Se um membro e as suas contas vinculadas lançarem centenas de transações resultando em saturação, este membro só será reembolsado nas primeiras transações, sendo as seguintes para além da quota.
* Se um membro lançar centenas de transações, mesmo para além da quota por membro, mas estiver sozinho a fazer transações nesse momento, isso pode acontecer sem saturar a blockchain e, portanto, sem taxas.
* No entanto, não existe qualquer possibilidade de reembolso em caso de encerramento da conta!
  *Não haverá lugar a reembolso para contas anónimas.

*É improvável que tal ataque seja desencadeado, pois arruinaria o atacante por um bloqueio temporário. **Estas taxas são, por isso, um desincentivo**.* 

Mais informações no [fórum techinque](https://forum.duniter.org/t/les-frais-ca-devient-concret/11877/1)

- - -

<h3 style="color:#f7a212" id="blacksmiths">8/ Sub-ecrã dos ferreiros</h3>
    
Hoje qualquer membro pode instalar um nó e forjar blocos, o que leva a alguns problemas de atualização e sincronização, bem como de segurança, pois alguns utilizadores desconhecem as vulnerabilidades de segurança da sua instalação e “esquecem-se” de atualizar.     

Com a V2 do Duniter, apenas os membros da subcanvas do ferreiro poderão forjar blocos.\
Qualquer pessoa pode ainda executar um nó espelho que não escreve blocos, mas responde aos pedidos do cliente.\
Como os nós de ferreiro se dedicam a calcular e escrever blocos, já não responderão aos pedidos dos clientes.\
Todos os nós comunicam quase instantaneamente.

<img src="/uploads/blacksmith2medaille.png" width="15%" align ="right" >

As **certificações de ferreiro** devem respeitar uma **licença de ferreiro** que garanta **um bom nível de segurança**, entre outras coisas já ter executado um nó espelho corretamente durante um determinado tempo, podendo **manter o seu servidor está aberto**  24 horas por dia, 7 dias por semana e tem uma boa ligação à internet.
Os membros desta subweb não têm necessariamente de se conhecer ou de se ver fisicamente, pois devem ser primeiro membros da Web de Confiança dos Co-Criadores.

Para fazer parte deste sub-ecrã do ferreiro desde a inicialização, deve executar um nó antes de mudar para gdev ou gtest (as moedas utilizadas para testar a versão 2 antes da inicialização).

**Documentação** que explica como **instalar um nó** e tornar-se um Ferreiro na V2 está a ser escrita e será comunicada assim que estiver concluída.

**Os ferreiros atuais** são convidados, se estiverem interessados, a tentar instalar o Duniter V2, para ver como funciona, detetar quaisquer problemas de instalação e ajudar a escrever a documentação "torne-se um ferreiro".
É/será possível utilizar [Yunohost](https://yunohost.org/fr) ou \[imagens Docker](https://www.ionos.fr/digitalguide/serveur/know-how/les-images - janela de encaixe /).

Mais informações no \[fórum técnico](https://forum.duniter.org/t/video-installer-un-noeud-duniter-v2-en-moins-de-dix- Minutes/11243/1)

- - -

<h3 style="color:#f7a212" id="fontionalites">9/ Próximos recursos</h3>

As novas funcionalidades serão possivelmente implementadas após o lançamento do Duniter V2, se os programadores estiverem disponíveis para começar.

* Faça transferências automáticas
* Delegar autoridade sobre uma conta
  *Conta com múltiplas assinaturas
* Contas de carteira ligadas a uma identidade
  *Direito ao esquecimento* (eliminação de comentário?)*
* Possibilidade de configurar votos!
  *E mais de acordo com a imaginação de cada um...

<details>
  <summary> <b>Autor </b></summary>

[Maaltir](https://forum.nouvelle-libre.fr/u/maaltir/summary) do Coletivo MàJ-V2 validado por [hugotrentesaux](https://duniter.org/team/hugotrentesaux/), [bgallois ](https://duniter.org/team/bgallois/), [Moul](https://duniter.org/team/moul/), [Tuxmain](https://duniter.org/team/tuxmain/)

</details>

<h2 id="flyers2"><a href="https://forum.monnaie-libre.fr/uploads/short-url/57ZxXnsDxxnv6pCBP8ETKDMwIU6.pdf" target="_blank" style="color:#34A1FF; font-weight: bold;">Folheto frente e verso para partilhar<img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>

<p><strong>Clique na imagem para ampliar</strong></p>

<div style="float:left;  margin: 0 1em 0;"><a href="/uploads/o-que-vai-mudar-em-portugues_page_1.jpg" target="_blank">
<img src="/uploads/o-que-vai-mudar-em-portugues_page_1.jpg" width="200"/></a><br/>traduzido por @Kristo</div>

<div style="float:left;  margin: 0 1em 0;"><a href="/uploads/o-que-vai-mudar-em-portugues_page_2.jpg" target="_blank">
<img src="/uploads/o-que-vai-mudar-em-portugues_page_2.jpg" width="200"/></a><br/>traduzido por @Kristo</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>

<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >

<h2 style="color:#34a1ff" id="#identique"> O que não vai mudar</h2>

* Encontrará todos os seus Ǧ1 na nova versão.
* As certificações validadas manterão a mesma data de expiração na V2. 
* As regras de criação de moeda permanecem as mesmas (1 DUğ1 por dia reavaliado a cada 6 meses). 
  <img src="/uploads/rienechange.png" width="18%" align ="right" >
* As regras para ser membro da Web of Trust permanecem as mesmas: mínimo de 5 certificações respeitando a regra da distância.
* O Ğ1 continua a ser a principal moeda livre e continua a cumprir o M.R.T.
* DuniterV2s, Cesium2, Gecko, Tika, G1nkgo, Ğ1superbot continuam a ser software livre.
  **As muitas discussões e questões** em torno desta atualização da V2 trouxeram à luz mais claramente **coisas que já existiam na V1**; 
* **A predominância dos programadores**: desde o início da Ğ1, foram eles que escolheram as actualizações a fazer. Quando a V2 estiver em andamento, poderemos discutir um sistema de tomada de decisões em profundidade. 
* **O poder dos ferreiros**, que decidem se aceitam ou não as actualizações. Na V2, os ferreiros já não terão de decidir se aceitam ou não uma atualização; as actualizações serão feitas automaticamente, a não ser que se recusem (princípio da liberdade).
* **A capacidade de trocar por outras moedas**: Toda a gente sempre foi livre de trocar ğ1 por outras moedas. As outras moedas são objectos aos quais cada um é livre de atribuir um valor. [Ver aqui um extrato em francês do MRT](https://forum.monnaie-libre.fr/t/g1-et-plateformes-de-change/29900/61) 
* **A implementação do Ğ1 nas plataformas de câmbio** não está mais planeada na V2 do que na V1. Mas, de qualquer forma, é impossível impedir que alguém crie essa possibilidade (é o mundo LIVRE). Se ainda não foi feito, é apenas porque ninguém o achou suficientemente interessante.\
  Mais explicações em francês no fórum: https://forum.monnaie-libre.fr/t/g1-et-plateformes-de-change/29900/78

<h2 style="color:#34a1ff" id="bascule">Como é que a transição vai funcionar para os utilizadores?</h2> 

<h3 style="color:#f7a212" id="date">A/ Data da transição</h3>

A data de transição ainda não foi definida, depende do progresso dos desenvolvimentos e, por conseguinte, também do seu financiamento. [Apoie os desenvolvedores](https://www.helloasso.com/associations/axiom-team/collectes/finalisation-de-cesium-v2-et-duniter-v2). 

<img src="/uploads/code-geek.png" width="18%" align ="right" >

Por enquanto, os desenvolvedores estão apontando para 8 de março, o aniversário de Ğ1. 

Esta mudança de Blockchain é um projeto de grande escala que facilitará as coisas para os utilizadores junistas.

O coletivo MàJ-V2 está a comunicar o mais possível sobre esta mudança, para que todos os junistas se sintam confortáveis com esta atualização do seu software e possam falar sobre ela. 

* no fórum: '[O que vai acontecer: mais perguntas?](https://forum.monnaie-libre.fr/t/comment-cela-va-se-passer-encore-des-questions/31042) 
* no Telegram: Telegram: Contacto [@monnaielibrejune](http://t.me/monnaielibrejune/57035)

<h3 style="color:#f7a212" id="scenario">B/ Cenário de transição de V1 para V2</h3>

#### O fim da V1

**O cenário de transição ainda não foi formalmente estabelecido.**

 A hora **0** (data e hora) de início da Duniter 2.0 ainda não foi fixada de forma definitiva.

* 5 ou 6 semanas antes da hora zero, aparecerá uma mensagem no Cesium indicando a data da passagem e informando que haverá uma atualização (automática ou manual) do software cliente. 
* Estará disponível uma última atualização do césio, por razões de segurança, para evitar qualquer ação na V1, que poderia ser perdida. 
* 30 dias antes da passagem efectiva, será efectuado um teste de passagem. Os utilizadores que o desejarem poderão testar o software disponível com o Ğtest. 
* Cerca de 1 hora antes do momento **0**, será tirada uma imagem do Ğ1.  

<img src="/uploads/photo.jpg" width="18%" align ="right" >

* Será preferível evitar qualquer ação sobre o Ğ1 alguns minutos antes de a imagem ser captada (“Don't move!”, foto CLIC). Será lançada uma campanha de comunicação final no maior número possível de plataformas para incentivar as pessoas a deixarem de utilizar o Ğ1 até que este seja transferido para a v2.
* Todas as **acções na V1 após esta foto** serão registadas na blockchain da V1 se (apesar de tudo) os nós continuarem a funcionar na V1, mas **perder-se-ão para a V2**.
* A blockchain V2 será lançada a partir desta foto. 
* **O Ǧ1 ficará indisponível por algumas horas durante esta fase de reinicialização.**

#### Após o lançamento da V2.

* Os utilizadores receberão ou serão convidados a **atualizar cada uma das suas instalações** (computador, tablet, telefone, Firefox, Brave...) da aplicação cliente (Césium 2.0). As versões anteriores do Césium já não devem funcionar.
* Se a atualização do Césium não for imediata\*\*, não tem importância. Basta fazê-lo antes da sua próxima ligação ou instalar outro software cliente (Gecko, Tikka, G1nkgo, etc.). 
* De preferência, deve verificar esta **atualização** antes de se deslocar a um mercado\*\*, para usufruir de uma boa ligação à Internet em casa. 
* Depois de atualizar ou instalar o software cliente V2, os **utilizadores encontrarão as suas contas**, as suas transacções e as suas certificações, exceto o que estava à espera de ser registado na blockchain (pool).

<img src="/uploads/lav2.png" width="18%" align ="right" >

* **O DU continuará** a criar-se a si próprio nas contas dos membros.
* Os pedidos de adesão não validados e as certificações pendentes (no pool) desaparecerão. **Só tem de os refazer**.
* As transacções efectuadas no momento em que a imagem foi tirada (para aqueles que não seguiram as instruções) **podem** não ter tido tempo de ser validadas (foi-lhe dito para **não se mover**, está desfocado) e também terão de ser refeitas. 
* Os utilizadores poderão retomar as suas actividades como antes, com muito maior fluidez, graças às novas aplicações cliente.

Detalhes em francês no [fórum técnico](https://forum.duniter.org/t/chronologie-de-la-migration/12605)

<details> 
  <summary> <b>Autor </b></summary>

[Maaltir](https://forum.monnaie-libre.fr/u/maaltir/summary) do Coletivo MàJ-V2, validado pelos devs [hugotrentesaux](https://duniter.org/team/hugotrentesaux/), [bgallois](https://duniter.org/team/bgallois/), [Moul](https://duniter.org/team/moul/), [Tuxmain](https://duniter.org/team/tuxmain/), [Vit](https://duniter.org/team/vit/)

</details>

<h2 id="flyers2"><a href="https://forum.monnaie-libre.fr/uploads/short-url/tXmLrChkL1QjguGyJtNLNwezzIs.pdf" target="_blank" style="color:#34A1FF; font-weight: bold;">Folheto frente e verso para partilhar<img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>

<p><strong>Clique na imagem para ampliar</strong></p>

<div style="float:left;  margin: 0 1em 0;"><a href="/uploads/pt-o-que-não-vai-mudar-e-como-joy5081_page_1.jpg" target="_blank">
<img src="/uploads/pt-o-que-não-vai-mudar-e-como-joy5081_page_1.jpg" width="200"/></a><br/>traduzido por @Kristo</div>

<div style="float:left;  margin: 0 1em 0;"><a href="/uploads/pt-o-que-não-vai-mudar-e-como-joy5081_page_2.jpg" target="_blank">
<img src="/uploads/pt-o-que-não-vai-mudar-e-como-joy5081_page_2.jpg" width="200"/></a><br/>traduzido por @Kristo</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>

<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >