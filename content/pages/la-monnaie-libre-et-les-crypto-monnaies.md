---
title: La monnaie libre et les crypto-monnaies
description: La Ğ1 est une crypto-monnaie, mais pas comme les autres !
---
## Cryptomonnaie.

Les cryptomonnaies sont les monnaies basées sur la technologie de la blockchain décentralisée.\
La plupart des cryptomonnaies connues sont conçues pour être très spéculatives. Leur création est souvent réservée aux personnes qui en ont les moyens techniques ou financiers.

Ce qui différencie la Ğ1 c'est son mode de création. La Ğ1 est créée à part égale entre tous les membres de la communauté.

## Économique et sobre.

Imprimer des billets ou frapper des pièces est une manière complexe et coûteuse de gérer la monnaie. À l’heure actuelle, on ne sait pas le faire d’une façon qui soit suffisamment fiable, sécurisée et résiliente. Le contrôle de l’émission de la monnaie sur un support matériel est toujours centralisé, et peut donc être corrompu ou violé.

C’est pourquoi la Ğ1 est une cryptomonnaie, c’est-à-dire une monnaie numérique décentralisée et sécurisée par cryptographie, grâce à la technologie blockchain.\
De plus, Duniter (DU-uniter), le logiciel qui génère le DU, qui gère la monnaie et la toile de confiance, est un logiciel sous licence libre : son code est ouvert, accessible et vérifiable par tous.

Certaines personnes se sentent envahies et submergées par le numérique - à raison. Avec la monnaie libre, il est possible de déléguer sa relation avec l'écran pour s'en préserver. Il suffit de tenir un petit carnet de dépenses et recettes. Une personne de confiance, de son choix, peut alors procéder aux transactions pour "copier" les opérations du petit carnet individuel dans le registre collectif décentralisé et partagé par tous. Car une "blockchain", ce n'est rien d'autre que ce registre partagé et réputé inviolable.



