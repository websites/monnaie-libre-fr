---
title: Découvrir la monnaie libre
description: Toutes les informations pour comprendre !
plan: ui/main-flatplan
---
## La monnaie n'est pas le fruit du travail.

La monnaie que vous avez n'est pas le fruit de votre travail.
La monnaie, c'est ce que vous acceptez en **échange** du fruit de votre travail.

## La monnaie n'est pas neutre.

C'est même loin d'être le cas. Et pourtant c'est ce que l'on apprend à l'école, c'est ce que nous croyons communément.

Nous avons vu que dans le système monétaire est régi par les banques :
<https://www.economie.gouv.fr/facileco/creation-monetaire-definition> \
C'est la banque qui décide qui a le droit à la monnaie, selon ses critères, et il faut rembourser plus qu'il n'y a de monnaie en circulation, en permanence. Il y a une programmation des comportements économiques, qui se situe bien en amont des discours, des vœux, des actions ... et des lois.

Quelle programmation ? Compétition, prédation, course au profit, faillite des plus fragiles ou des plus lents (y compris les faillites individuelles que l'on nomme chômage ou précarité), croissance, besoin exponentiel d'énergie, extraction systémique des ressources planétaires, cycles arbitraires d'abondance et de pénurie, etc... 

Cette programmation est simplement mathématique ; elle impacte tout le fonctionnement de l'économie, elle impacte le comportement de tout le monde dans la société.

## Et la monnaie libre ? Un commun.

Dans le système de la monnaie libre, la monnaie est créée à parts égales par tous les membres, sans dette à rembourser et sans intérêts à payer. Chaque membre crée sa propre portion de monnaie, le **dividende universel**. C'est considérer la masse monétaire créée comme **un commun**, où chaque personne serait actionnaire à part égale relative.

On peut y voir un revenu de base, (pas d'existence), ou autre... l'important est la définition précise et mathématique que vous faites de chaque terme.

Le DU - Dividende Universel n'est pas directement, en lui-même, un revenu de subsistance ou de suffisance. Ces notions sont subjectives et relatives, propres à chaque individu. Elles reposent non pas sur la monnaie, mais sur la couverture des besoins pour subsister ou *vivre heureux*. **Nos besoins à couvrir ne sont pas la monnaie elle-même**, mais les biens et services que nous produisons et que nous échangeons dans notre économie.

La création permanente de monnaie, soulage la peur de l'avenir, et favorise un comportement de coopération et d'entraide.

## En vidéo

La monnaie libre sans jargon par Corinne :

<iframe src="https://player.vimeo.com/video/498611617" width="100%" height="420" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

17 membres expliquent la Monnaie Libre :

<iframe width="100%" height="410" sandbox="allow-same-origin allow-scripts allow-popups" src="https://tube.p2p.legal/videos/embed/ea3c06b6-134b-49da-aea2-7a8bacf97dd5" frameborder="0" allowfullscreen></iframe>

- - -

## Une unité de mesure

Nous utilisons la monnaie comme unité, pour estimer les valeurs, pour mesurer l'économie. Et pourtant l'unité de mesure que nous utilisons, la monnaie des banquiers, est très loin de représenter toujours la même valeur dans l'espace et dans le temps ! Elle ne cesse de varier. C'est comme si nous utilisions, pour mesurer les distances, un étalon mètre qui change de longueur en permanence.

Le mètre a été défini comme la distance de l'équateur au pôle divisé par 10 000 000. Le degré Celsius comme la différence de température entre le gel et l'évaporation de l'eau divisé par 100.\
La seconde du SI est définie par la durée d'un certain nombre d'oscillations (9 192 631 770 exactement) liées à un phénomène physique concernant l'atome de césium (fréquence de transition des 2 niveaux d'énergie de sa structure hyperfine : elle est tellement stable qu'il faut plus d'un million d'années pour observer un décalage d'une seconde).\
Ces unités de mesure sont invariantes et universelles pour tous les êtres humains dans le temps et l'espace.

Cependant, nous n'avons rien de stable et fiable pour définir la valeur d'une unité monétaire telle que nous la pratiquons. Cette valeur varie dans le temps et l'espace. Une monnaie peut être dévaluée par simple création d'unités supplémentaires.  

Avec la monnaie libre, nous fabriquons **une nouvelle unité de mesure** : *la quantité de monnaie produite chaque jour par chaque individu*. Quand la monnaie libre sera pleinement en place, cette quantité représentera toujours la même *portion* de monnaie par rapport à la masse monétaire globale. Ce qui en fera un invariant à travers le temps et l'espace, seul moyen d'obtenir une symétrie spatiale et temporelle. 

Il aura toujours la même valeur relative, pour toujours.\
**Le DUğ1 devient ainsi une unité de mesure de valeur universelle.**

Cet invariant était la première recherche mathématique de Stéphane Laborde qui l'a conduit à une solution unique, la production par chaque être humain de cette unité de mesure. Il n'y a selon lui, aucune autre formule mathématique possible pour parvenir à ce résultat.

