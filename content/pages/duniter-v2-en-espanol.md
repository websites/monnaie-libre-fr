---
title: DUNITER V2 en español
description: Versión 2 para el software Ğ1
---
<div style = "display:inline-block;float:left;">
<img src="/uploads/demenagement-v1-v2.jpg" width=400px align="center" style="margin:auto;"/>
</div>
<div style = "display:inline-block;float:left; padding-left : 20px;">
<p style="color:#34A1FF;" id="langues">Otros idiomas &nbsp; &nbsp;&nbsp;</p>

[En français](/maj-v2/)  

[In-english](/duniter-v2-in-english/)

[In-italiano](/duniter-v2-in-italiano/)

[Em português](/duniter-v2-em-portugues/)

[Auf-deutsch](/duniter-v2-auf-deutsch/)

</div>

<div style = "display :inline-block; margin:5px auto 5px; text-align:center; width:400px; border:solid #3FB2FF 1px; background-color:#3FB2FF;border-radius: 10px; padding: 1em;">
<a style="font-size:1.5em; font-weight: bold; color: white;" href="https://www.helloasso.com/associations/axiom-team/collectes/finalisation-de-cesium-v2-et-duniter-v2">financiación colectiva <img src="/uploads/crowfunding100.png" style="padding:5px;margin:auto;"/></a>
</div>

<h2 style="color:#34A1FF;" id="plan">Plan de Comunicación</h2>
¿Cuándo se les informará sobre la progresión del proyecto?

1. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Octubre :** </strong> [   ¿por qué cambiar?](#pourquoi)</p>
2. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Noviembre - diciembre :**  </strong> [lo que va a cambiar](#evolutions) </p>
3. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Enero :** 
   </strong> [lo que no cambiará](#identique) <br/> [y **cómo será el proceso**](#bascule) </p>
4. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Febrero :** </strong> los programas satélites : **descubrir, probar y utilizar** </p> 

<hr style=" border: 2px solid #34A1FF;margin:50px auto;"/>

<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="300" align ="center" >

<h2 style="color:#34A1FF;" id="pourquoi">¿Por qué cambiar?</h2>

<h3 style ="color: #F7A212" id="inconvenients">1/ Desventajas de la V1</h3>

#### Las primeras desventajas son principalmente visibles para los desarrolladores :

* La razón principal que motiva esta migración: : **Dificultades para mantener <lexique>Duniter</lexique> V1 (corregir errores y realizar mejoras).**
* Problemas de seguridad :

  * Duniter puede fácilmente ser bloqueado (ataque por saturación de la red).
  * Todas las cuentas de miembros son potencialmente <lexique title="forgeron">forjadores</lexique>, sin embargo, algunas son fácilmente pirateables, lo que permitiría que un atacante comprometa potencialmente toda la blockchain.
* Obligación para los forjadores de realizar resincronizaciones manualmente para que siga funcionando.
* Riesgo de fork* si los forjadores</lexique> no actualizan <lexique>Duniter</lexique> rápidamente en caso de una corrección de errores.

<alert type="info">
<details> 
  <summary> * Haga clic aquí para saber: Qué es un fork?</summary>
 
 Una bifurcación se produce cuando dos nodos calculan el mismo bloque al mismo tiempo, lo que da lugar a 2 versiones del mismo bloque y, por tanto, a 2 versiones de la cadena de bloques.   
 Una bifurcación puede resolverse sola, sin pérdida de datos (esto es normal cuando los nodos están actualizados),         
 Puede resolverse con pérdida de datos, lo que es bastante habitual en V1 (cuando hay problemas de sincronización o actualización de nodos).      
 Puede no resolverse automáticamente, dando lugar a una bifurcación en la blockchain (algunos usuarios no ven lo mismo que otros).      
 Si la bifurcación no se resuelve (automática o manualmente) puede provocar una división en la comunidad.     

Traducción realizada con la versión gratuita del traductor DeepL.com 

 **Para más detalles, siga estos enlaces:**\
https://journalducoin.com/lexique/fork/\
https://www.coinbase.com/fr/learn/crypto-basics/what-is-a-fork\
https://fr.cryptonews.com/exclusives/c-est-quoi-un-fork-5803.htm    

</details>
</alert>

#### Todos los usuarios ya han notado otras desventajas:

<img style="float:right; width:15%;" src="../uploads/escargot-inconvenients.png"/>

* Lentitud en la ejecución.   

  * Tiempo largo para mostrar las operaciones.
  * Entre 5 y 10 minutos para que se procese una transacción, 30 minutos para que sea validada.
* Problemas de sincronización de las piscinas[^1] : Visiones diferentes de un nodo a otro, ya que ningún nodo tiene una visión global de las piscinas.
* Transacciones o certificaciones que no se validan.
* Certificaciones que desaparecen.
* Dificultad para que entren nuevos junistas (problemas de sincronización entre certificadores para tener 5 certificadores disponibles al mismo tiempo)[^2].

[^1]: piscinas : si un miembro emite varias certificaciones en menos de 5 días, éstas permanecerán suspendidas y sólo se registrarán en la blockchain de una en una cada 5 días (tal y como estipulan las normas del TDC, pero sin indicación del orden en que se registrarán).\
[^2]:Sabiendo que las certificaciones de los miembros son anteriores a las de los solicitantes, un futuro miembro tendrá que esperar a que los demás hayan pasado antes de ser certificado.

<h3 style ="color: #F7A212" id="avantages">2/ Ventajas de la v2</h3>
  <img style="float:right; width:18%;" src="../uploads/papillon-avantages.png"/>

* Mantenimiento y actualizaciones más fáciles para los desarrolladores.
* Actualización automática sin fork para los forjadores (la posibilidad de rechazar una actualización seguirá existiendo, pero será voluntaria y no por olvido).
* Un bloque cada 6 segundos\

  * Transacción validada en 30 segundos.      
  * Tiempo de respuesta considerablemente mejorado.
* Sincronización de nodos más rápida y fiable.
* Certificaciones validadas inmediatamente (sin piscinas o pools).
* Entrada más fácil de nuevos miembros (ya no se necesita sincronización entre los 5 primeros certificadores).

<h3 style ="color: #F7A212" id="evolution">3/ Evolución del software, no de la moneda</h3>

El software evoluciona constantemente. A día de hoy, estamos en la versión 1.8.7 de Duniter y 1.7.13 de Cesium. Estas evoluciones se han realizado de manera que sean compatibles con las versiones anteriores y siempre han transcurrido sin problemas.

La versión 2 sigue siendo software libre; es una nueva blockchain que comenzará con todos los datos de la blockchain Ğ1 V1 en el momento del cambio (cuentas, transacciones, certificaciones...). 
**Encontrarán de nuevo todas sus Ğ1, sus certificaciones y transacciones.**

<alert type="info">

*En cuanto a los datos de Cesium+ (perfiles, mensajería, notificaciones...), el trabajo está en proceso, ¡y tal vez no estarán desde el inicio!*

</alert>

Una nueva blockchain implica nuevos softwares (Cesium 2, Gecko, Tikka, G1nkgo 2, ...).

No será posible intercambiar entre dos blockchains diferentes, por lo tanto, después de la fecha del cambio, deberá tener instalada la nueva versión del software para seguir intercambiando ğ1 con el resto de la comunidad.

**Para asegurarte de que no te pierdes este cambio, ¡permanece atento a este sitio, al foro y a las redes sociales!**

<h2 id="flyers1"><a href="https://forum.monnaie-libre.fr/uploads/short-url/cmB3FkML3bF0BigyQcFyDNL7Yg7.pdf" target="_blank" width="50%" style="color:#34A1FF; font-weight: bold;">Folleto de doble cara para compartir <img src="/uploads/clic40.png" style="padding-left:10px;"/></a></h2>

<p><strong>Haga clic en la imagen para ampliarla</strong></p>

<div style="float:left; margin-left:1em;"><a href="/uploads/por-qué-cambiar-al-español_kapis_page_1.jpg" target="_blank">
<img src="/uploads/por-qué-cambiar-al-español_kapis_page_1.jpg" width="200"/></a><br/> traducido por @Kapis</div>

<div style="float:left; margin:0 1em 0;"><a href="/uploads/por-qué-cambiar-al-español_kapis_page_2.jpg" target="_blank">
<img src="/uploads/por-qué-cambiar-al-español_kapis_page_2.jpg" width="200"/></a><br/> traducido por @Kapis</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >

<h2  style="color:#34a1ff" id="evolutions">Las evoluciones (lo que cambiará para los usuarios)</h2>

<h3 style="color:#f7a212" id="logiciels">1/ Nuevos Software</h3>

Para poder seguir interactuando con los demás junistas, es indispensable que todos cambien de versión al hacer su primera conexión después del arranque de la blockchain V2.

* **Césium**: El software más utilizado ofrecerá su versión V2. No se planea migrar a Duniter V2 antes de que este software esté listo.
* **Gecko**: Orientado a smartphones para los pagos, está listo con algunos ajustes por hacer.
* **Tikka**: En computadora, orientado a la contabilidad para los profesionales, en desarrollo.
* **Ğ1nko**: Un monedero que facilita las transacciones en los G-mercados. La versión 2 podría estar lista para la migración.
* **Ğ1superbot**: También tendrá su versión para Duniter V2.
* **Duniter-Connect**: Extensión del navegador que permite realizar transferencias desde cualquier sitio, evoluciona junto con los desarrollos de Duniter.
* **Ğcli**: El cliente de línea de comandos (para técnicos), similar a Silkaj, también evoluciona junto con los desarrollos de Duniter.
* ...

<h3 style="color:#f7a212" id="cles">2/ Nueva forma de las LLaves Públicas</h3>

Con la nueva blockchain Duniter V2, lo que conocemos como llaves públicas cambiará de codificación. Así que tu llave pública no será la misma que la que conocías. Probablemente tendrás que reimprimir tu código QR si lo tienes impreso.

<img style="float:right; width:15%;" src="../uploads/adresse-cle.png"/>

Hablaremos más bien de direcciones que de llaves, todas empezarán con "g1...", por lo que será mejor usar la parte final de la dirección en lugar del comienzo para reconocer tu cuenta.

En Duniter v2, se fomentará el uso de una dirección en lugar de una llave pública simple. La dirección permite evitar errores de copia y el uso de una llave en la red equivocada.

**Para ayudarte, para las cuentas creadas antes de la actualización, Cesium2 mostrará, como recordatorio, la "llave pública" antigua. Lo mismo ocurrirá con G1nko, que mostrará también esta llave antigua.**

### <span style="color:#f7a212" id="depot">3/ Depósito Existencial</span>

En la blockchain v1, las cuentas con menos de 1 Ğ1 desaparecen sin que el usuario sea advertido (destrucción de moneda).
En la blockchain v2, será imposible caer por debajo de 1 Ğ1 sin solicitar explícitamente el cierre de la cuenta.

<h3 style="color:#f7a212" id="piscines">4/ Desaparición de las certificaciones en cola o espera</h3>
<img style="float:right; width:18%;" src="../uploads/nageur.png"/>

* Las certificaciones serán tomadas en cuenta inmediatamente, independientemente de si la persona certificada está esperando más certificaciones o no.
* El plazo de 5 días entre dos certificaciones será obligatorio, ya que Duniter V2 solo permitirá una certificación cada 5 días.
* Algunos clientes (como Césium) podrían ofrecer la opción de agregar tus promesas de certificación a una "agenda de direcciones" (aún no desarrollado).

<h3 style="color:#f7a212" id="adhesions">5/ Proceso de Adopción (convertirse en miembro co-creador de moneda)</h3>

* Las nuevas cuentas serán solo cuentas de monedero simples.
* Se deberá poseer algo de Ğ1 para comenzar el proceso de certificación. Por lo tanto, será imposible certificar una cuenta con cero Ğ1.
* La primera certificación servirá como invitación para convertirse en miembro (ya no será necesario hacer una solicitud de candidatura).
* Aceptar esta invitación consiste en que el nuevo miembro elija un seudónimo en un plazo de 48 horas (este plazo podría cambiar).
* Una vez registrado el seudónimo, serán posibles las certificaciones siguientes.
* Cuando la cuenta alcance 5 certificaciones que cumplan con la regla de distancia, se convertirá en co-creador de moneda.
* Ya no será necesario sincronizarse para certificar (ya no se podrá certificar si uno está congelado durante 5 días).
* El plazo de dos meses para obtener las primeras cinco certificaciones que cumplan la regla de distancia seguirá siendo válido. (La duración de este plazo podría cambiar).
* Si el plazo expira, el certificador recuperará su certificación de su stock de certificaciones a emitir.

<h3 style="color:#f7a212" id="securite">6/ Nuevas cuentas mejor protegidas</h3>

La autenticación mediante identificador secreto y contraseña es demasiado insegura para una moneda (Hoy en día los bancos requieren una autenticación de doble factor).
<img style="float:right; width:18%;" src="../uploads/coffre.png"/>

1. **Las cuentas antiguas, con identificador secreto y contraseña, seguirán siendo utilizables con Césium 2.** Como antes de la transición a V2.
2. **Las nuevas cuentas serán creadas a partir de un mnemónico de 12 palabras.** Estas 12 palabras deberán guardarse cuidadosamente y fuera del alcance de otros, ya que permiten recuperar las cuentas en otros dispositivos o con otros gestores de monederos (Cesium, Gecko, Tikka, ...).
3. Se podrán crear varias cuentas a partir del mismo mnemónico (esto se denomina "bóveda"). Una "bóveda" puede contener varias cuentas o solo una.
4. Una vez creada la bóveda, bastará con un código o PIN de 4 o 5 letras o números para acceder a todas las cuentas de la bóveda, siempre que se use el mismo dispositivo.
5. Una cuenta creada con mnemónico será utilizable en Cesium, Gecko, Tikka, Gcli y seguramente con otros monederos.

<h3 style="color:#f7a212" id="migration">6 bis / Migración a una cuenta mejor protegida</h3>

**Solo para los junistas que lo deseen.**
   <img style="float:right; width:18%;" src="../uploads/gecko-4244388_640.png"/>

1. Gecko está diseñado para funcionar únicamente con cuentas creadas con mnemónico, por razones de seguridad. Para los otros software no hay nada definitivo por el momento.
2. Gecko ofrece la opción de “migrar” tus cuentas antiguas (id/password) hacia una nueva cuenta que habrás creado previamente con un mnemónico.
3. Esto te permitirá agrupar todas tus cuentas en una sola bóveda, lo que facilita su uso.
4. La migración de una cuenta miembro implica el traslado de tu identidad, seudónimo, certificaciones, estado de miembro o no, y Ğ1.
5. La migración de una cuenta de monedero simple se limita a un traslado de todos los Ğ1.
6. Una vez migrada, la nueva cuenta (con mnemónico) será usable para todas tus transacciones, independientemente del software gestor de monederos (Cesium, Gecko, Tikka, Gcli) y la cuenta antigua será solo un monedero vacío.
7. En caso de migrar una cuenta de miembro, será necesario recargar el documento de revocación.
8. La cuenta antigua seguirá siendo utilizable como cualquier otro monedero, pero perdería el beneficio de haber sido migrada.

<h3 style="color:#f7a212" id="frais">7/ Tarifas y cuotas</h3>

#### La capacidad de una blockchain no es infinita.

Una blockchain necesita potencia de cálculo y espacio de almacenamiento. Aunque ambos son sustanciales, no son ilimitados. Un posible ataque es la saturación de la potencia de cálculo mediante el envío de miles de millones de transacciones por segundo. Otras blockchains cobran tarifas por cada acción para disuadir este tipo de saturación de cálculos y almacenamiento.

#### Pero la Ğ1 no es como las otras.

#### 1- Las comisiones solo se cobrarán en caso de saturación de la blockchain.

* **Un número total de acciones** (transacciones, certificaciones, adhesiones, ...) **por bloque** ha sido evaluado como el **límite de funcionamiento "normal"**. **Más allá de este límite**, la blockchain se considera en **saturación**.
* Solo si el número de acciones en un bloque **supera este límite**, se **cobrarán comisiones**. Se estima que la tarifa será de aproximadamente 0.015 DUĞ1, es decir, 17 Ğ1 por 100 transacciones.

#### 2- Además, las comisiones serán reembolsadas a todos los miembros de la red de confianza *(esa es la fuerza de nuestra Blockchain)*

<img style="float:right; width:18%;" src="../uploads/blockchain_humain.png"/>

* Se definirá un cupo de acciones por miembro y por bloque (que permitirá muchas transacciones).
* Las cuentas no miembros podrán vincularse a una cuenta miembro y también recibir el reembolso de sus comisiones, dentro del límite del cupo por miembro.
* Un miembro puede hacer una sola transacción, pero si en ese momento alguien realiza 1 millón de transacciones en 1 millón de cuentas, la blockchain se saturará, y este miembro será cargado con una comisión.
* Después se le reembolsará porque no supera la cuota por miembro.
* Si se trata de un miembro y de sus cuentas que envian cientos de transacciones que provovan la saturación, este miembro solo será rembolsado en las primeras transacciones, las demás sobrepasando la cuota.
* Si un miembre envia cientos de transacciones incluso por encima de la cuota por miembro, pero que es el único haciendo transacciones en ese momento, puede pasar sin saturar la blockchain, por lo tanto, sin coste.
* Aún así, ¡no hay reembolso posible en caso de cierre de cuenta!
* No habrá reembolso para cuentas anónimas.

*Es poco probable que se lance un ataque de este tipo, ya que llevaría a la ruina al atacante, por un bloqueo temporal. Por tanto, estas tarifas son disuasorias.
Más información en el foro za*

Más información en [el foro técnico](https://forum.duniter.org/t/les-frais-ca-devient-concret/11877/1)  

<h3 style="color:#f7a212" id="forgerons">8/ Subred Herreros</h3> 

Hoy cualquier miembro puede instalar un nodo y forjar bloques, lo cual lo que provoca algunos fallos de actualización y sincronización, así como problemas de seguridad, ya que algunos usuarios desconocen los fallos de seguridad de su instalación y «olvidan» actualizar.\
Con la V2 de Duniter, sólo los miembros de la subred de forjado podrán forjar bloques.\
Cualquiera puede seguir ejecutando un nodo espejo que no escriba bloques pero responda a las peticiones de los clientes.\
Como los nodos forjadores se dedican a calcular y escribir bloques, ya no responderán a las peticiones de los clientes.\
Todos los nodos se comunican entre sí casi instantáneamente.\
<img src="/uploads/blacksmith2medaille.png" width="15%" align ="right" > 

Las **certificaciones de herreros** deberán respetar una **licencia** de herrero que asegure **un buen nivel de seguridad**, entre otras cosas, deben llevar ya algún tiempo gestionando correctamente un nodo espejo, ser capaces de **mantener su servidor abierto** 24 horas al día, 7 días a la semana, y disponer de una buena conexión a Internet.\
Los miembros de esta subred no tienen por qué conocerse ni verse físicamente, ya que antes deben ser miembros de la Red de Confianza de los Cocreadores.    

Para ser parte de esta subred de herreros desde el arranque, hay que gestionar un nodo antes de la  antes de pasar a gdev o gtest (las monedas utilizadas para probar la versión 2 antes de arrancar).   

La **documentación** que explica cómo **instalar un nodo** y convertirse en un Herrero V2 se está escribiendo actualmente y se enviará tan pronto como esté completa.
Los **herreros actuales** están invitados, si están interesados, a probar la instalación de Duniter V2, para ver cómo funciona, detectar cualquier problema de instalación y ayudar a escribir la documentación para «convertirse en herrero». Es/será posible utilizar  [Yunohost](https://yunohost.org/fr) o [imágenes Docker](https://www.ionos.fr/digitalguide/serveur/know-how/les-images-docker/).

Más información en [el foro técnico](https://forum.duniter.org/t/video-installer-un-noeud-duniter-v2-en-moins-de-dix-minutes/11243/1).

<h3 style="color:#f7a212" id="fontionalites">9/Futuras funciones</h3> 

Es posible que se añadan nuevas funciones tras el lanzamiento de Duniter V2, si los desarrolladores están disponibles para asumirlas.

* Realizar transferencias automáticas
* Delegar autoridad sobre una cuenta
* Cuentas con varias firmas
* Cuentas monedero vinculadas a la identidad
* Derecho al olvido (¿borrado de comentarios?)
* Posibilidad de establecer votaciones
* Y mucho más, según la imaginación de cada cual...

<details> 
  <summary> <b>Autor </b></summary>

[Maaltir](https://forum.monnaie-libre.fr/u/maaltir/summary) del Colectivo MàJ-V2, validado por devs [hugotrentesaux](https://duniter.org/team/hugotrentesaux/), [bgallois](https://duniter.org/team/bgallois/), [Moul](https://duniter.org/team/moul/), [Tuxmain](https://duniter.org/team/tuxmain/)

</details>

<h2 id="flyers2"><a href="https://forum.monnaie-libre.fr/uploads/short-url/fZr75W34c2QXxggJd9TLVOymII1.pdf" target="_blank" style="color:#34A1FF; font-weight: bold;">Folleto de doble cara para compartir <img src="/uploads/clic40.png" style="padding-left:10px;"/></a></h2>

<p><strong>Haga clic en la imagen para ampliarla</strong></p>

<div style="float:left; margin: 0 1em 0;"><a href="/uploads/que-cambiara-en-espanol_1.jpg" target="_blank">
<img src="/uploads/que-cambiara-en-espanol_1.jpg" width="200"/></a><br/> traducido por @aldara </div>

<div style="float:left;margin: 0 1em 0;"><a href="/uploads/que-cambiara-en-espanol_2.jpg" target="_blank">
<img src="/uploads/que-cambiara-en-espanol_2.jpg" width="200"/></a><br/> traducido por @aldara </div>

<p style="float:left;"> <strong>Página traducida al español por @Kapis y @isalanzarote</strong></p>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >

<h2 style="color:#34a1ff" id="identique"> Lo que no va a cambiar</h2>

* Encontrarás todos tus Ǧ1 en la nueva versión.
* Las certificaciones validadas mantendrán la misma fecha de caducidad en la V2. 
* Las reglas de creación de moneda siguen siendo las mismas (1 DUğ1 al día revalorizado cada 6 meses). 
  <img src="/uploads/rienechange.png" width="18%" align ="right" >
* Las reglas para ser miembro de la Web de Confianza siguen siendo las mismas: 5 certificaciones como mínimo respetando la regla de distancia.
* El Ğ1 sigue siendo la moneda libre líder y sigue cumpliendo con el M.R.T.
* DuniterV2s, Cesium2, Gecko, Tika, G1nkgo, Ğ1superbot siguen siendo software libre.
  **Los numerosos debates y preguntas** en torno a esta actualización de la V2 han sacado a la luz con mayor claridad **cosas que ya existían en la V1**; 
* **El predominio de los desarrolladores**: desde el inicio de Ğ1, han sido ellos los que han elegido qué actualizaciones hacer. Una vez que la V2 se ponga en marcha, podremos hablar largo y tendido de un sistema de toma de decisiones. 
* **El poder de los herreros**, que deciden si aceptan o no las actualizaciones. En la V2, los herreros ya no tendrán que decidir si aceptan o no una actualización; las actualizaciones se harán automáticamente a menos que se nieguen (principio de libertad).
* **La posibilidad de cambiar por otras monedas**: Todo el mundo ha sido siempre libre de cambiar ğ1 por otras monedas. Las otras monedas son objetos a los que cada uno es libre de dar un valor. [Véase aquí un extracto de la TRM](https://forum.monnaie-libre.fr/t/g1-et-plateformes-de-change/29900/61) 
* **La implantación del Ğ1 en plataformas de cambio** no está más prevista en la V2 que en la V1. Pero en cualquier caso es imposible impedir que alguien cree esa posibilidad (es el mundo LIBRE). Si no se ha hecho ya, es sólo porque nadie lo ha considerado suficientemente interesante.\
  Más explicaciones en francés en el foro: https://forum.monnaie-libre.fr/t/g1-et-plateformes-de-change/29900/78

<h2 style="color:#34a1ff" id="bascule">¿Cómo funcionará la conversión para los usuarios?</h2> 

<h3 style="color:#f7a212" id="date">A/ Fecha de transición</h3>

La fecha de la conversión aún no está definida, depende del avance de los desarrollos y por tanto también de su financiación. [Apoya a los desarrolladores](https://www.helloasso.com/associations/axiom-team/collectes/finalisation-de-cesium-v2-et-duniter-v2). 

<img src="/uploads/code-geek.png" width="18%" align ="right" >

Por ahora, los desarrolladores apuntan al 8 de marzo, aniversario de Ğ1. 

Este cambio de blockchain es un proyecto importante que facilitará las cosas a los junistas.

El colectivo MàJ-V2 está comunicando todo lo posible sobre este cambio, para que todos los junistas se sientan cómodos con esta actualización de su software, y puedan hablar sobre ello. 

* en el foro: '[Qué va a pasar: ¿quedan dudas?](https://forum.monnaie-libre.fr/t/comment-cela-va-se-passer-encore-des-questions/31042)
* en Telegram: Telegram: Contacto [@monnaielibrejune](http://t.me/monnaielibrejune/57035)

<h3 style="color:#f7a212" id="escenario">B/ Escenario de cambio de V1 a V2</h3>

#### El fin de V1

**El escenario de conmutación aún no se ha establecido formalmente** .

 El momento **0** (fecha y hora) en el que comenzará Duniter 2.0 aún no se ha elegido definitivamente.

* 5 ó 6 semanas antes de la hora cero, aparecerá un mensaje en Cesium indicando la fecha de la conmutación e informando de que habrá una actualización (automática o manual) del software cliente. 
* Por razones de seguridad, se dispondrá de una última actualización en Cesium para evitar que se realice cualquier acción en la V1, que podría perderse. 
* 30 días antes de la conmutación real, habrá una conmutación de prueba. Quienes lo deseen podrán probar el software disponible con la prueba Ğtest. 
* Aproximadamente 1 hora antes del momento **0**, se tomará una imagen del Ğ1.  

<img src="/uploads/photo.jpg" width="18%" align ="right" >

* Será preferible evitar cualquier acción sobre el Ğ1 unos minutos antes de que se tome la imagen («¡No te muevas!», foto CLIC). Se lanzará una última campaña de comunicación en tantas plataformas como sea posible para animar a la gente a dejar de utilizar el Ğ1 hasta que se haya trasladado a la v2.
* Todas las **acciones en V1 después de esta foto** quedarán registradas en la blockchain de V1 si (a pesar de todo) los nodos siguen funcionando en V1, pero **se perderán para V2**.
* La blockchain de V2 se lanzará a partir de esta foto. 
* **El Ǧ1 no estará disponible durante unas horas durante esta fase de reinicio.**

#### Después de que la V2 haya sido lanzada.

* Los usuarios recibirán o serán invitados a **actualizar cada una de sus instalaciones** (ordenador, tableta, teléfono, Firefox, Brave...) de la aplicación cliente (Césium 2.0) Si todo va bien, la actualización será automática. Las versiones anteriores de Césium ya no deberían funcionar.
* **Si la actualización de Césium no es inmediata**, no importa. Basta con hacerla antes de la próxima conexión o instalar otro software cliente (Gecko, Tikka, G1nkgo, etc.). 
* Preferiblemente, deberías comprobar esta **actualización antes de ir a un ğmercado**, para disfrutar de una buena conexión a Internet en casa. 
* Tras actualizar o instalar el software cliente V2, los **usuarios encontrarán sus cuentas**, sus transacciones y sus certificaciones, salvo lo que estaba pendiente de registrar en la blockchain (pool).

<img src="/uploads/lav2.png" width="18%" align ="right" >

* **La DU seguirá creándose** en las cuentas de los miembros.
* Las solicitudes de afiliación no validadas y las certificaciones pendientes (en la reserva) desaparecerán. **Todo lo que hay que hacer es rehacerlas**.
* Las transacciones realizadas en el momento en que se tomó la imagen (para aquellos que no siguieron las instrucciones) **pueden** no haber tenido tiempo de ser validadas (se le dijo que no **se moviera**, está fuera de foco) y también tendrán que rehacerse. 
* Los usuarios podrán reanudar sus actividades como antes con mucha mayor fluidez gracias a las nuevas aplicaciones cliente.

Detalles en francés en el [foro técnico](https://forum.duniter.org/t/chronologie-de-la-migration/12605)

<details> 
  <summary> <b>Autor </b></summary>

[Maaltir](https://forum.monnaie-libre.fr/u/maaltir/summary) del Colectivo MàJ-V2, validado por devs  [hugotrentesaux](https://duniter.org/team/hugotrentesaux/), [bgallois](https://duniter.org/team/bgallois/), [Moul](https://duniter.org/team/moul/), [Tuxmain](https://duniter.org/team/tuxmain/), [Vit](https://duniter.org/team/vit/)

</details>

<h2 id="flyers2"><a href="https://forum.monnaie-libre.fr/uploads/short-url/hM9gQynnEpekDPNg1ek5b3TvmM6.pdf" target="_blank" style="color:#34A1FF; font-weight: bold;">Folleto de doble cara para compartir <img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>

<p><strong>Haga clic en la imagen para ampliarla</strong></p>

<div style="float:left; margin: 0 1em 0;"><a href="/uploads/es-lo-que-no-va-a-cambiar-y-como-kapis_page_1.jpg" target="_blank">
<img src="/uploads/es-lo-que-no-va-a-cambiar-y-como-kapis_page_1.jpg" width="200"/></a><br/> traducido por @Kapis </div>

<div style="float:left;margin: 0 1em 0;"><a href="/uploads/es-lo-que-no-va-a-cambiar-y-como-kapis_page_2.jpg" target="_blank">
<img src="/uploads/es-lo-que-no-va-a-cambiar-y-como-kapis_page_2.jpg" width="200"/></a><br/> traducido por @Kapis </div>

<p style="float:left;"> <strong>Página traducida al español por @Kapis y @isalanzarote</strong></p>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >