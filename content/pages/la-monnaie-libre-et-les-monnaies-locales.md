---
title: La monnaie libre et les monnaies locales
description: Différence entre la monnaie libre et les monnaies locales
---

    introduction générale des MLCC et pointeurs vers des références externes pour compléter
    points communs
        intentions affichées (pédagogiques / éducpop, vertus économiques…)
        économie locale, ancrage local
        gouvernance démocratique (citoyenne / contributive)
    différences
        au niveau de la théorie = création monétaire
        au niveau du mode d’organisation = institutionnel / décentralisé-anarchique
        au niveau des pratiques
        localité géographique = approche fédérative / universelle
    conclusion sur la complémentarité des approches
