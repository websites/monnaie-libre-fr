---
title: Migration V2
description: Une version 2 pour les logiciels de la Ğ1.
---
[En français](/maj-v2/)

[En español](/duniter-v2-en-espanol/)

[In-english](/duniter-v2-in-english/)

[In-italiano](/duniter-v2-in-italiano/)

[Em português](/duniter-v2-em-portugues/)

[Auf-deutsch](/duniter-v2-auf-deutsch/)
