---
title: Page test Spiranne
description: "Ceci est une page de tests "
---
<h2 style="color:red;">Ceci est un titre H2</h2>
<p style="color:#7C30FF;"> Comment visualiser la page avant qu'elle soit publiée ?</p>
texte direct sans balises

#### Tous les utilisateurs ont déjà constaté d'autres inconvénients :

<img style="float:right; width:30%;" src="../uploads/blacksmith.jpg"/>

* Lenteur d'exécution.

  * Temps long pour afficher les opérations
  * Entre 5 et 10 min pour qu'une transaction soit traitée, 30 minutes pour qu'elle soit validée
* Problèmes de synchronisation des piscines[1](https://monnaie-libre.fr/maj-v2/#fn-1) : Visions différentes d'un nœud à un autre, car aucun nœud n'a de vision globale des piscines.
* Transactions ou certifications qui ne passent pas.
* Certifications qui disparaissent.
* Difficulté pour faire entrer des nouveaux (problèmes de synchronisation entre certificateurs pour avoir 5 certificateurs disponibles en même temps)[2](https://monnaie-libre.fr/maj-v2/#fn-2).

#### Tous les utilisateurs ont déjà constaté d'autres inconvénients :

<img style="float:right; width:30%;" src="../uploads/blacksmith2medaille.png"/>

* Lenteur d'exécution.

  * Temps long pour afficher les opérations
  * Entre 5 et 10 min pour qu'une transaction soit traitée, 30 minutes pour qu'elle soit validée
* Problèmes de synchronisation des piscines[1](https://monnaie-libre.fr/maj-v2/#fn-1) : Visions différentes d'un nœud à un autre, car aucun nœud n'a de vision globale des piscines.
* Transactions ou certifications qui ne passent pas.
* Certifications qui disparaissent.
* Difficulté pour faire entrer des nouveaux (problèmes de synchronisation entre certificateurs pour avoir 5 certificateurs disponibles en même temps)[2](https://monnaie-libre.fr/maj-v2/#fn-2).

<hr style="border:2px dotted black; margin: 100 auto;"/>

<div style="float:left;"><a href="../uploads/pourquoi-changer-de-version-duniter_page_1.jpg" target=_blank><img src="../uploads/pourquoi-changer-de-version-duniter_page_1.jpg" width="400"/> </a></div>

<div style="float:left;"><a href="../uploads/pourquoi-changer-de-version-duniter_page_2.jpg" target=_blank><img src="../uploads/pourquoi-changer-de-version-duniter_page_2.jpg" width="400"/> </a></div>

<hr style="border:2px dotted black; margin: 100 auto;float:none;width:100%"/>

<img style="float:left; width:400;" src="../uploads/ce-qui-va-changer-synthetique-page_1.jpg"/>

<img style="float:left; width:400;" src="../uploads/ce-qui-va-changer-synthetique-page_2.jpg"/>