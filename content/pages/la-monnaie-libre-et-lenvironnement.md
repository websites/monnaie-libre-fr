---
title: La monnaie libre et l'environnement
description: La  Ğ1 supporte très bien la décroissance et est sobre en énergie.
---
## La croissance.

La monnaie des banquiers est créée par les crédits accordés de façon très hiérachique, aux Etats, aux Grandes Entreprises ou multinationales, aux PME, enfin aux particuliers. Les crédits sont accordés selon la capacité de remboursement, donc la capacité à créer une valeur agréée par cette hiérarchie que la banque domine ; et sur l'aptitude à faire des profits, car il faut en plus payer les intérêts.\

Cette monnaie des banquiers a besoin de croissance perpétuelle pour continuer d'exister. Puisqu'il faut rendre plus de monnaie qu'il en existe, il faut en permanence créer de la nouvelle monnaie. Or cette nouvelle monnaie n'est créée qu'en contrepartie d'une dette, c'est-à-dire un engagement à créer une valeur qui n'existe pas encore, donc en contrepartie d'une production supplémentaire.

Sans cet engagement, sans cette dette, sans cette croissance, la monnaie disparaît. 

La monnaie libre est créée en continu, tant qu'il y a des êtres humains vivants. Elle n'a donc pas besoin de croissance, ni de course au profit pour continuer d'exister. Elle est prévisible, stable, directement corrélée au flux de la vie humaine. Dans ce référentiel, il ne peut plus y avoir de crise monétaire.\
Ainsi, elle est une monnaie idéale pour concevoir et bâtir un monde plus sobre, plus équilibré, adapté et paisible. 

## La sobriété

La Ğ1 n'utilise pas de billets infalsifiables coûteux en ressources, et dont il faudrait régulièrement recalculer la valeur relative. Elle préfère utiliser la blockchain.\
L'algorithme de la Ğ1 a été particulièrement étudié pour consommer très peu d'énergie.\
<https://duniter.fr/faq/duniter/duniter-est-il-energivore/>\
La prochaine version sera encore plus économe.

NOTA : Pour les personnes qui ne veulent pas, ou ne peuvent pas, avoir de relation avec un écran, il suffit d'utiliser un petit carnet et de déléguer cette relation à une personne de confiance, de son choix.

