---
title: Les flux RSS du site
description: Les liens de tous les flux
---
Vous pouvez vous abonner aux [flux RSS](https://fr.wikipedia.org/wiki/RSS) pour recevoir les derniers articles publiés sur le site.

## Flux principal

Contient tous les nouveaux articles ajoutés sur le site : page, FAQ, lexique.

<div class="flex items-center -mt-3">
  <a class="no-underline bg-gray-200 dark:bg-black rounded px-2 mr-2 hover:bg-hover hover:text-white transition" href="/feed.xml" target="_blank">RSS (.xml)</a>
  <a class="no-underline bg-gray-200 dark:bg-black rounded px-2 mr-2 hover:bg-hover hover:text-white transition" href="/feed.rss" target="_blank">RSS (.rss)</a>
  <a class="no-underline bg-gray-200 dark:bg-black rounded px-2 mr-2 hover:bg-hover hover:text-white transition" href="/feed.json" target="_blank">RSS (.json)</a>
</div>

## Flux des pages

Contient les pages de premier niveau (à la racine du site).

<div class="flex items-center -mt-3">
  <a class="no-underline bg-gray-200 dark:bg-black rounded px-2 mr-2 hover:bg-hover hover:text-white transition" href="/feed/pages.xml" target="_blank">RSS (.xml)</a>
  <a class="no-underline bg-gray-200 dark:bg-black rounded px-2 mr-2 hover:bg-hover hover:text-white transition" href="/feed/pages.rss" target="_blank">RSS (.rss)</a>
  <a class="no-underline bg-gray-200 dark:bg-black rounded px-2 mr-2 hover:bg-hover hover:text-white transition" href="/feed/pages.json" target="_blank">RSS (.json)</a>
</div>

## Flux de la FAQ

Contient les questions de la [FAQ](/faq).

<div class="flex items-center -mt-3">
  <a class="no-underline bg-gray-200 dark:bg-black rounded px-2 mr-2 hover:bg-hover hover:text-white transition" href="/feed/faq.xml" target="_blank">RSS (.xml)</a>
  <a class="no-underline bg-gray-200 dark:bg-black rounded px-2 mr-2 hover:bg-hover hover:text-white transition" href="/feed/faq.rss" target="_blank">RSS (.rss)</a>
  <a class="no-underline bg-gray-200 dark:bg-black rounded px-2 mr-2 hover:bg-hover hover:text-white transition" href="/feed/faq.json" target="_blank">RSS (.json)</a>
</div>

## Flux du lexique

Contient les termes du [lexique](/lexique).

<div class="flex items-center -mt-3">
  <a class="no-underline bg-gray-200 dark:bg-black rounded px-2 mr-2 hover:bg-hover hover:text-white transition" href="/feed/lexique.xml" target="_blank">RSS (.xml)</a>
  <a class="no-underline bg-gray-200 dark:bg-black rounded px-2 mr-2 hover:bg-hover hover:text-white transition" href="/feed/lexique.rss" target="_blank">RSS (.rss)</a>
  <a class="no-underline bg-gray-200 dark:bg-black rounded px-2 mr-2 hover:bg-hover hover:text-white transition" href="/feed/lexique.json" target="_blank">RSS (.json)</a>
</div>