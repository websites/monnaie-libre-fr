---
title: DUNITER V2 in italiano
description: Versione 2 per il solftware Ğ1
---
<div style = "display:inline-block;float:left;">
<img src="/uploads/demenagement-v1-v2.jpg" width=400px align="center" style="margin:auto;"/>
</div>
<div style = "display:inline-block;float:left; padding-left : 20px;">
<p style="color:#34A1FF;" id="langues">Altre lingue &nbsp; </p>

[En français](/maj-v2/)  

[En español](/duniter-v2-en-espanol/) 

[In-english](/duniter-v2-in-english/)

[Em português](/duniter-v2-em-portugues/)

[Auf-deutsch](/duniter-v2-auf-deutsch/)

</div>

<p style="float:left;"><strong>Traduzione automatica con \\[deepl](https://www.deepl.com/fr/translator)</strong> </p>
<div style = "display :inline-block; margin:5px auto 5px; text-align:center; width:400px; border:solid #3FB2FF 1px; background-color:#3FB2FF;border-radius: 10px; padding: 1em;">
<a style="font-size:1.5em; font-weight: bold; color: white;" href="https://www.helloasso.com/associations/axiom-team/collectes/finalisation-de-cesium-v2-et-duniter-v2">Raccolta di fondi (crowdfunding)<img src="/uploads/crowfunding100.png" style="padding:5px;margin:auto;"/></a>
</div>

<h2 style="color:#34A1FF;" id="plan">Piano di comunicazione</h2>

Quando sarete aggiornati sull'andamento del progetto? 

1. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Ottobre :**</strong> [perché cambiare?](#pourquoi)</p>
2. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Novembre - dicembre :** </strong> [cosa cambierà](#evolutions) </p>
3. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Gennaio :**</strong> [cosa non cambierà](#identique) <br/> [e **come avverrà**](#bascule) </p>
4. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Febbraio :**</strong> il software satellitare: **découvrir, tester et utiliser** </p> 

</div>

<hr style=" border: 2px solid #34A1FF;margin:50px auto;"/>

<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="300" align ="center" >

<h2 style="color:#34A1FF;" id="pourquoi">Perché cambiare?</h2>

<h3 style ="color: #F7A212" id="inconvenients">1/ Svantaggi di V1</h3>

#### Gli svantaggi iniziali sono più evidenti per gli sviluppatori:

* La ragione principale che ha motivato questa migrazione: \*\*Difficoltà di manutenzione di <lexique>Duniter</lexique> V1 (correzione di bug e miglioramenti).
* Problemi di sicurezza,

  * Duniter può essere facilmente bloccato (attacco di saturazione della rete).
  * Tutti gli account dei membri sono potenzialmente dei falsari[^1], ma alcuni sono facilmente violabili, il che permetterebbe a un aggressore di compromettere potenzialmente l'intera blockchain.
* Necessità per i <lexicon title=“forgeron”>fabbri</lexicon> di risincronizzarsi manualmente per far funzionare le cose.
* Rischio di fork* se i <lexicon title=“forgeron”>fabbri</lexicon> non aggiornano rapidamente <lexicon>Duniter</lexicon> in caso di correzione di un bug.

[^1]: un fabbro è un membro della TDC (Toile De Confiance) che ha scaricato il software Duniter sul proprio dispositivo (computer o altro), che diventa un server (o nodo) per eseguire June.

<alert type="info">

<details> 
  <summary> *Clicca qui per scoprirlo: Cos'è una biforcazione</summary>

 Una biforcazione avviene quando due nodi calcolano lo stesso blocco nello stesso momento, dando luogo a 2 versioni dello stesso blocco, quindi a 2 versioni della blockchain da seguire.
 Un fork può risolversi da solo, senza alcuna perdita di dati (è normale quando i nodi sono aggiornati), 
 può risolversi con una perdita di dati, cosa abbastanza comune nella V1 (quando ci sono problemi di sincronizzazione o di aggiornamento dei nodi).
 Può non risolversi automaticamente, dando luogo a una biforcazione della blockchain (alcuni utenti non vedono la stessa cosa di altri).
 Se la biforcazione non viene risolta (automaticamente o manualmente) può portare a una spaccatura della comunità.
 **Per maggiori dettagli, seguire questi link in francese :**\
https://journalducoin.com/lexique/fork/\
https://www.coinbase.com/fr/learn/crypto-basics/what-is-a-fork\
https://fr.cryptonews.com/exclusives/c-est-quoi-un-fork-5803.htm

</details>
</alert>

#### Tutti gli utenti hanno già notato altri svantaggi:

<img style="float:right; width:15%;" src="../uploads/escargot-inconvenients.png"/>

* Esecuzione lenta.

  * Tempi lunghi per la visualizzazione delle transazioni
  * Tra i 5 e i 10 minuti per l'elaborazione di una transazione, 30 minuti per la sua convalida.
* Problemi di sincronizzazione dei pool[^1]: viste diverse da un nodo all'altro, poiché nessun nodo ha una vista globale dei pool.
* Transazioni o certificazioni che non vengono eseguite.
* Certificazioni che scompaiono.
* Difficoltà di inserimento di nuovi certificatori (problemi di sincronizzazione tra i certificatori per avere 5 certificatori disponibili allo stesso tempo)[^2].  

[^1]: pool: se un membro rilascia più certificazioni in meno di 5 giorni, queste rimarranno sospese e saranno registrate nella blockchain solo una alla volta ogni 5 giorni (come stabilito dalle regole TDC, ma senza alcuna indicazione dell'ordine in cui saranno registrate).\
[^2]: Sapendo che le certificazioni dei membri vengono prima di quelle dei candidati, un futuro membro dovrà aspettare che gli altri siano passati prima di essere certificato.

<h3 style ="color: #F7A212" id="avantages">2/ Vantaggi di v2</h3>
<img style="float:right; width:18%;" src="../uploads/papillon-avantages.png"/>

* Manutenzione e aggiornamenti facili per gli sviluppatori
* Aggiornamenti automatici senza fork per i fabbri 
  *<i>(la possibilità di rifiutare un aggiornamento esisterà ancora, ma sarà volontaria e non più dovuta a dimenticanza)</i>*.
* Un blocco ogni 6 secondi
* Transazione convalidata in 30 secondi
* Tempo di risposta significativamente migliorato
* Sincronizzazione dei nodi più veloce e affidabile
* Certificazioni convalidate immediatamente (senza pool)
* Ingresso più semplice per i nuovi certificatori (non è necessaria la sincronizzazione tra i primi 5 certificatori)

<h3 style ="color: #F7A212" id="evolution">3/ Evoluzione del software, non del denaro</h3>

Les logiciels évoluent en permanence, à ce jour, nous sommes sur la version 1.8.7 de Duniter e 1.7.13 di Cesium. 
Queste modifiche sono state apportate in modo da rimanere compatibili con le versioni precedenti e sono sempre passate senza problemi. 

La versione 2 è ancora un software open source ed è una nuova blockchain che partirà con tutti i dati della blockchain Ğ1 V1 al momento del passaggio (conti, transazioni, certificazioni, ecc.).
**Tutti i vostri Ğ1, certificazioni e transazioni saranno ancora lì** .

<alert type="info">
<i>I dati di Cesium+ (profili, messaggistica, notifiche, ecc.) sono attualmente in fase di elaborazione e potrebbero non essere disponibili dal lancio!</i>
</alert>

Una nuova blockchain richiede un nuovo software (Césium 2, Gecko, Tikka, G1nkgo 2, ....).

Non sarà possibile fare trading tra due blockchain diverse, quindi dopo la data di passaggio sarà necessario aver installato la nuova versione del software per continuare a fare trading ğ1 con il resto della comunità.

**Per essere sicuri di non perdere questo passaggio, rimanete sintonizzati su questo sito, sul forum e sui social network!**

<h2 id="flyers1"><a href="https://forum.monnaie-libre.fr/uploads/short-url/x5Hf7qk6fPkD7HP2oibhPJ0Zc0J.pdf" target="_blank" width="50%" style="color:#34A1FF; font-weight: bold;">Volantino fronte-retro da condividere <img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>

<p><strong>Fare clic sull'immagine per ingrandirla</strong></p>

<div style="float:left;margin:0 1em 0;"><a href="/uploads/it-perche-cambiare-italpaola-20241010_page_1.jpg" target="_blank">
<img src="/uploads/it-perche-cambiare-italpaola-20241010_page_1.jpg" width="200"/></a><br/> tradotto da @italpaola su Telegram</div>

<div style="float:left; margin:0 1em 0;"><a href="/uploads/it-perche-cambiare-italpaola-20241010_page_2.jpg" target="_blank">
<img src="/uploads/it-perche-cambiare-italpaola-20241010_page_2.jpg" width="200"/></a><br/> tradotto da @italpaola su Telegram</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>

<p><img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" ></p>

<h2 style="color:#34a1ff" id="evolutions"> Sviluppi (cosa cambierà per gli utenti)</h2>

<h3 style="color:#f7a212" id="logiciels">1/ Nuovo software</h3>

Per poter continuare a scambiare con gli altri giunisti, è essenziale che tutti cambino versione non appena si connettono per la prima volta dopo il lancio della blockchain V2. 

* **Césium** Il software più diffuso offrirà la sua versione V2. Non è previsto il passaggio a Duniter V2 prima che questo software sia pronto. 
* **Gecko** per i pagamenti tramite smartphone è pronto, con alcuni aggiustamenti.
* **Tikka**, un software di contabilità computerizzato per professionisti, è attualmente in fase di sviluppo. 
* **Ğ1nko** un portafoglio per facilitare le transazioni nei mercati G. La versione 2 potrebbe essere pronta per la migrazione.
* **Ğ1superbot** avrà anche una versione per Duniter V2. 
* **Duniter-Connect**, un'estensione del browser che consente di effettuare trasferimenti da qualsiasi sito, si sta evolvendo con lo sviluppo di Duniter.
* **Ğcli** Il client a riga di comando (per i tecnici), simile a Silkaj, si sta evolvendo con lo sviluppo di Duniter.
* ...

- - -

<h3 style="color:#f7a212" id="cles">2/ Nuova forma di chiavi pubbliche</h3>

Con la nuova blockchain Duniter V2, le cosiddette chiavi pubbliche cambieranno la loro codifica. Quindi la vostra chiave pubblica non assomiglierà più a quella che conoscete. Chi ha stampato il proprio codice QR dovrà probabilmente ristamparlo.
<img style="float:right; width:15%;" src="../uploads/adresse-cle.png"/>

Inizieranno tutti con “g1...”, quindi utilizzate la fine dell'indirizzo piuttosto che l'inizio, per riconoscere il vostro account.

In Duniter v2, incoraggeremo l'uso di un indirizzo piuttosto che di una semplice chiave pubblica. L'uso di un indirizzo evita errori come la copia e l'uso di una chiave sulla rete sbagliata.

**Per aiutarvi, per gli account creati prima dell'aggiornamento, Cesium2 mostrerà la vecchia “chiave pubblica” come promemoria. Lo stesso vale per G1nkgo, che visualizzerà la vecchia chiave**.

- - -

<h3 style="color:#f7a212" id="depot">3/ Deposito esistenziale</h3>

Nella blockchain v1, i conti con meno di 1 Ğ1 scompaiono senza che l'utente ne sia informato (distruzione della valuta).
Nella blockchain v2, sarà impossibile scendere sotto 1 Ğ1 senza richiedere esplicitamente la chiusura del conto.

- - -

<h3 style="color:#f7a212" id="piscines">4/ Scomparsa dei tempi di attesa nelle piscine</h3> 
<img style="float:right; width:18%;" src="../uploads/nageur.png"/>

* Le certificazioni saranno prese in considerazione immediatamente, indipendentemente dal fatto che la persona certificata sia o meno in attesa di altre certificazioni.
* Il periodo di 5 giorni tra due certificazioni deve essere rispettato, perché DuniterV2 consente solo una certificazione ogni 5 giorni.
* Alcuni clienti (come Césium) possono proporre di aggiungere le vostre intenzioni di certificazione a una “rubrica” (non ancora sviluppata).

- - -

<h3 style="color:#f7a212" id="adhesions">5/ Processo di adesione (per diventare co-creatori di moneta)</h3>

* I nuovi conti saranno solo conti di portafoglio semplici. 
* Per avviare il processo di certificazione è necessario disporre di alcuni Ğ1. Sarà quindi impossibile certificare un conto con zero Ğ1.
* La prima certificazione costituisce un invito a diventare membro (non è necessario richiedere l'adesione).
* L'accettazione dell'invito richiede che il nuovo membro scelga un nickname entro 48 ore (questo periodo è soggetto a modifiche).
* Una volta registrato il nickname, è possibile ottenere le seguenti certificazioni.
* Quando il conto ha 5 certificazioni che rispettano la regola della distanza, diventa un co-creatore di valuta.
* Non è necessario sincronizzarsi per le certificazioni (non si potrà certificare se non si è disponibili).
* La scadenza di due mesi per ottenere le prime cinque certificazioni che soddisfano la regola della distanza sarà ancora valida. (La durata della scadenza potrebbe cambiare)
* Se le scadenze vengono superate, il certificatore recupererà la sua certificazione dal suo stock di certificazioni da rilasciare.

- - -

<h3 style="color:#f7a212" id="securite">6/ Nuovi conti più sicuri</h3>

L'authentification par identifiant secret et mot de passe est trop faiblement sécurisant pour une monnaie. (Aujourd'hui vos banques vous imposent une double authentification)

<img style="float:right; width:18%;" src="../uploads/coffre.png"/>

1. I vecchi account, con login e password segrete, saranno ancora utilizzabili con Césium, come prima del passaggio alla V2.
2. **I nuovi account verranno creati utilizzando un mnemonico di 12 parole**  Queste 12 parole devono essere tenute accuratamente nascoste, in quanto consentiranno di recuperare i propri account su altri dispositivi o su altri software di gestione del portafoglio (Cesium, Gecko, Tikka, ecc.).
3. È possibile creare più conti utilizzando lo stesso mnemonico (si tratta di una “cassaforte”). Una cassaforte può contenere più conti o uno solo. 
4. Una volta creata la cassaforte, un codice di 4 o 5 lettere o numeri sarà sufficiente per accedere a tutti gli account della cassaforte, purché si utilizzi lo stesso dispositivo. 
5. Un account creato con un mnemonico sarà utilizzabile su Cesium, Gecko Tikka, Gcli e probabilmente su altri software.   

- - -

<h3 style="color:#f7a212" id="migration">6 bis / Migrazione a un account più sicuro</h3>

**Solo per gli juniores che lo desiderano.**
<img style="float:right; width:18%;" src="../uploads/gecko-4244388_640.png"/>

1. Gecko è progettato per funzionare solo con gli account creati da mnemonic, per motivi di sicurezza. Per altri software non c'è ancora nulla di definito.
2. Gecko offre la possibilità di “migrare” i vostri vecchi account (id/mdp) in un nuovo account che avete creato con un mnemonico.
3. In questo modo è possibile raggruppare tutti i conti in un'unica cassetta di sicurezza, più facile da usare.
4. La migrazione di un conto membro comporta il trasferimento della vostra identità con nickname, certificazioni, se siete o meno membri e ğ1. 
5. La migrazione di un account di un singolo portafoglio è semplicemente un trasferimento di tutti i ğ1.
6. Una volta migrato, il nuovo conto (con mnemonico) sarà utilizzabile per tutte le vostre transazioni qualunque sia il software di gestione del portafoglio (Césium, Gecko, Tikka, Gcli) e il vecchio conto sarà solo un portafoglio vuoto.
7. Il vecchio conto può ancora essere utilizzato come qualsiasi altro portafoglio. Ma questo vanificherebbe lo scopo della migrazione.

- - -

<h3 style="color:#f7a212" id="frais">7/ Tasse e quote</h3>

#### La capacità di una blockchain non è infinita.

Una blockchain ha bisogno di potenza di calcolo e di spazio di archiviazione. Sebbene entrambi siano sostanziali, non sono illimitati. Un possibile attacco consiste nel saturare la potenza di calcolo inviando miliardi di transazioni al secondo. 
Altre blockchain fanno pagare una tassa per ogni azione per scoraggiare questa saturazione dello spazio di calcolo e di archiviazione.

#### Ma Ğ1 non è come le altre.

#### 1- Le tariffe saranno addebitate solo se la blockchain è sovraccarica.

* Un numero totale di azioni **(transazioni, certificazioni, affiliazioni, ecc.)** per blocco **è stato valutato come** limite per il “normale ” **funzionamento.** Oltre questo limite **la blockchain è considerata** satura\*\*. 
* È solo se il numero di azioni in un blocco **supera questo limite** che vengono applicate le **tasse**. Le commissioni sono stimate in circa 0,015 DUĞ1, ovvero 17 Ğ1 per 100 transazioni.

#### 2- Inoltre, le tasse saranno rimborsate a tutti i membri della rete di fiducia *(questa è la forza della nostra Blockchain)*.

  <img style="float:right; width:18%;" src="../uploads/blockchain_humain.png"/>

* Viene definita una quota di azioni per membro e per blocco (che consente molte transazioni).
* I conti dei non membri possono essere collegati a un conto di un membro e le loro commissioni possono essere rimborsate, nel rispetto della quota per membro. 
* Un membro può effettuare una singola transazione, ma se nello stesso momento qualcuno avvia 1 milione di transazioni su 1 milione di conti, la blockchain si satura e al membro vengono addebitate le spese.
* Poi verrà rimborsato perché non ha superato la quota per membro.
* Se un membro e i suoi conti collegati lanciano centinaia di transazioni, portando alla saturazione, il membro sarà rimborsato solo per le prime transazioni, mentre le transazioni successive supereranno la quota.
* Se un membro avvia centinaia di transazioni, anche oltre la quota per membro, ma è l'unico a effettuare transazioni in quel momento, ciò può essere fatto senza saturare la blockchain, quindi non c'è alcun costo.
* Tuttavia, non è previsto alcun rimborso se l'account viene chiuso!
* Non ci saranno rimborsi per i conti anonimi.

\*È improbabile che un attacco di questo tipo venga sferrato, poiché rovinerebbe l'attaccante e provocherebbe un blocco temporaneo. \**Queste tariffe sono quindi un deterrente.

Ulteriori informazioni in francese su [il forum tecnico](https://forum.duniter.org/t/les-frais-ca-devient-concret/11877/1).

- - -

<h3 style="color:#f7a212" id="forgerons">8/ Sottostella dei fabbri</h3> 
    

Oggi qualsiasi membro può installare un nodo e forgiare blocchi, il che comporta alcuni errori di aggiornamento e sincronizzazione, nonché problemi di sicurezza, poiché alcuni utenti non sono consapevoli delle falle di sicurezza della loro installazione e “dimenticano” di aggiornare.        

Con Duniter V2, solo i membri della sottorete dei fabbri potranno forgiare i blocchi.\
Chiunque può comunque gestire un nodo mirror che non scrive blocchi ma risponde alle richieste dei clienti.\
Poiché i nodi forgiatori si concentrano sul calcolo e sulla scrittura dei blocchi, non risponderanno più alle richieste dei clienti.\
Tutti i nodi comunicano tra loro quasi istantaneamente.    

<img src="/uploads/blacksmith2medaille.png" width="15%" align ="right" >

I fabbri **certificati** dovranno rispettare una **licenza** di fabbro che garantisce un **buon livello di sicurezza**, tra cui l'aver già gestito correttamente un nodo mirror per un certo periodo di tempo, essere in grado di **mantenere il proprio server aperto** 24 ore su 24 e 7 giorni su 7 e avere una buona connessione a Internet.
I membri di questa sottorete non devono necessariamente conoscersi o vedersi fisicamente, poiché devono prima essere membri della Rete di fiducia dei co-creatori. 

Per far parte di questa sotto-rete di falsari fin dall'inizio, è necessario eseguire un nodo prima del passaggio su gdev o gtest (le valute usate per testare la versione 2 prima dell'avvio).

Una **documentazione** che spiega come **installare un nodo** e diventare Fabbro nella V2 è attualmente in fase di stesura e verrà comunicata non appena sarà completata.

Gli attuali **smith** sono invitati, se interessati, a provare a installare Duniter V2, per vedere come funziona, individuare eventuali problemi di installazione e contribuire alla stesura della documentazione su come “diventare un fabbro”.    

È/sarà possibile utilizzare [Yunohost](https://yunohost.org/fr) o [immagini Docker](https://www.ionos.fr/digitalguide/serveur/know-how/les-images-docker/).

Ulteriori informazioni in francese su [il forum tecnico](https://forum.duniter.org/t/video-installer-un-noeud-duniter-v2-en-moins-de-dix-minutes/11243/1)

- - -

<h3 style="color:#f7a212" id="fontionalites">9/ Caratteristiche future</h3> 
Nuove funzionalità possono essere implementate dopo il lancio di Duniter V2, se gli sviluppatori sono disponibili a lavorarci.

* Trasferimenti automatici
* Delega dell'autorità su un conto
* Conti a firma multipla
* Conti di portafoglio legati all'identità
* Diritto all'oblio (cancellazione dei commenti?).
* Possibilità di istituire votazioni!
* E altro ancora, a seconda della vostra immaginazione...

<details> 
  <summary> <b>Autore </b></summary>

[Maaltir](https://forum.monnaie-libre.fr/u/maaltir/summary) di Collectif MàJ-V2, convalidato da [hugotrentesaux](https://duniter.org/team/hugotrentesaux/), [bgallois](https://duniter.org/team/bgallois/), [Moul](https://duniter.org/team/moul/), [Tuxmain](https://duniter.org/team/tuxmain/)

</details>

<h2 id="flyers1"><a href="https://forum.monnaie-libre.fr/uploads/short-url/uxuSTfBivLt0WnUZfAeLVQWzLTf.pdf" target="_blank" width="50%" style="color:#34A1FF; font-weight: bold;">Volantino fronte-retro da condividere <img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>

<p><strong>Fare clic sull'immagine per ingrandirla</strong></p>

<div style="float:left; margin:0 1em 0;"><a href="/uploads/it-cosa-cambia-g1-v2-italpaola-20241108_page_1.jpg" target="_blank">
<img src="/uploads/it-cosa-cambia-g1-v2-italpaola-20241108_page_1.jpg" width="200"/></a><br/> tradotto da @italpaola</div>

<div style="float:left; margin:0 1em 0;"><a href="/uploads/it-cosa-cambia-g1-v2-italpaola-20241108_page_2.jpg" target="_blank">
<img src="/uploads/it-cosa-cambia-g1-v2-italpaola-20241108_page_2.jpg" width="200"/></a><br/>tradotto da @italpaola</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >

<h2 style="color:#34a1ff" id="identique"> Cosa non cambierà</h2>

* Troverete tutti i vostri Ǧ1 nella nuova versione.
* Le certificazioni convalidate manterranno la stessa data di scadenza nella V2. 
* Le regole per la creazione di valuta rimangono invariate (1 DUğ1 al giorno rivalutato ogni 6 mesi). 
<img src="/uploads/rienechange.png" width="18%" align ="right" >
* Le regole per diventare membro della Rete della fiducia rimangono invariate: 5 certificazioni minime rispettando la regola della distanza.
* L'Ğ1 è ancora la principale moneta libera e rispetta il M.R.T.
* DuniterV2s, Cesium2, Gecko, Tika, G1nkgo, Ğ1superbot sono ancora software liberi.

**Le numerose discussioni e domande** che circondano questo aggiornamento della V2 hanno portato alla luce più chiaramente **cose che esistevano già nella V1**; 

* **La predominanza degli sviluppatori**: fin dall'inizio della Ğ1, sono stati loro a scegliere quali aggiornamenti apportare. Una volta avviata la V2, potremo discutere a lungo di un sistema decisionale. 
* **Il potere dei fabbri**, che decidono se accettare o meno gli aggiornamenti. Nella V2, i fabbri non dovranno più decidere se accettare o meno un aggiornamento; gli aggiornamenti saranno effettuati automaticamente, a meno che non si rifiutino (principio di libertà).
* **La possibilità di scambiare con altre valute**: ognuno è sempre stato libero di scambiare il ğ1 con altre valute. Le altre valute sono oggetti a cui ognuno è libero di assegnare un valore. [Vedi qui un estratto del TRM in francese](https://forum.monnaie-libre.fr/t/g1-et-plateformes-de-change/29900/61) 
* **L'implementazione del Ğ1 sulle piattaforme di scambio** non è più prevista nella V2 che nella V1. Ma è in ogni caso impossibile impedire a chiunque di creare una tale possibilità (è il mondo LIBERO). Se non è ancora stato fatto, è solo perché nessuno lo ha ritenuto sufficientemente interessante.\
    Ulteriori spiegazioni in francese sul forum : https://forum.monnaie-libre.fr/t/g1-et-plateformes-de-change/29900/78

<h2 style="color:#34a1ff" id="bascule">Come funzionerà il passaggio al digitale per gli utenti? </h2> 

<h3 style="color:#f7a212" id="date">A/ Data di passaggio</h3>

La data di passaggio non è ancora stata definita, dipende dall'avanzamento degli sviluppi e quindi anche dal loro finanziamento. [Supporto agli sviluppatori](https://www.helloasso.com/associations/axiom-team/collectes/finalisation-de-cesium-v2-et-duniter-v2). 

<img src="/uploads/code-geek.png" width="18%" align ="right" >

Per ora gli sviluppatori puntano all'8 marzo, anniversario di Ğ1. 

Questo passaggio alla blockchain è un progetto importante che renderà le cose più semplici per gli utenti di giugno.

Il collettivo MàJ-V2 sta comunicando il più possibile su questo passaggio, in modo che tutti gli junisti siano a proprio agio con questo aggiornamento del software e possano parlarne. 

* sul forum: '[Cosa succederà: altre domande?](https://forum.monnaie-libre.fr/t/comment-cela-va-se-passer-encore-des-questions/31042) 
* su Telegram: Telegram: Contatto [@monnaielibrejune](http://t.me/monnaielibrejune/57035)

<h3 style="color:#f7a212" id="scenario">B/ Scenario di commutazione da V1 a V2</h3>

#### La fine della V1

**Lo scenario di commutazione non è acora stato formalmente stabilito** .

 Il tempo **0** (data e ora) in cui Duniter 2.0 inizierà non è ancora stato scelto definitivamente.

* 5 o 6 settimane prima dell'ora zero, in Cesium apparirà un messaggio che indica la data del passaggio e informa che ci sarà un aggiornamento (automatico o manuale) del software client. 
* Per motivi di sicurezza, sarà disponibile un aggiornamento finale di Cesium per evitare che vengano intraprese azioni sulla V1, che potrebbero andare perse. 
* 30 giorni prima del passaggio effettivo, sarà effettuato un passaggio di prova. Chi lo desidera potrà testare il software disponibile con il Ğtest. 
* Circa 1 ora prima del momento **0**, verrà scattata un'immagine dell'Ğ1.  

<img src="/uploads/photo.jpg" width="18%" align ="right" >

* Sarà preferibile evitare di compiere qualsiasi azione sull'Ğ1 pochi minuti prima dello scatto dell'immagine (“Non muoverti!”, foto CLIC). Verrà lanciata un'ultima campagna di comunicazione sul maggior numero possibile di piattaforme per incoraggiare le persone a non utilizzare l'Ğ1 fino al suo passaggio alla v2.
* Tutte le **azioni su V1 dopo questa foto** saranno registrate nella blockchain V1 se (nonostante tutto) i nodi continueranno a funzionare in V1, ma **saranno perse per V2**.
* La blockchain V2 sarà lanciata da questa foto. 
* **Il sito Ǧ1 non sarà disponibile per alcune ore durante questa fase di riavvio.**

#### Dopo il lancio della V2.

* Gli utenti riceveranno o saranno invitati ad **aggiornare ciascuna delle loro installazioni** (computer, tablet, telefono, Firefox, Brave...) dell'applicazione client (Césium 2.0) Se tutto va bene, l'aggiornamento sarà automatico. Le versioni precedenti di Césium non dovrebbero più funzionare.
* **Se l'aggiornamento di Césium non è immediato**, non importa. Basta farlo prima della prossima connessione o installare un altro software client (Gecko, Tikka, G1nkgo, ecc.). 
* Preferibilmente, si dovrebbe controllare questo **aggiornamento prima di recarsi in un ğmarket**, per godere di una buona connessione a Internet a casa. 
* Dopo l'aggiornamento o l'installazione del software client V2, gli **utenti troveranno i loro conti**, le loro transazioni e le loro certificazioni, ad eccezione di ciò che era in attesa di essere registrato nella blockchain (pool).

<img src="/uploads/lav2.png" width="18%" align ="right" >

* **Il DU continuerà** a crearsi sugli account dei membri.
* Le domande di adesione non convalidate e le certificazioni in sospeso (nel pool) scompariranno. **L'unica cosa da fare è rifarle**.
* Le transazioni effettuate al momento dello scatto dell'immagine (per coloro che non hanno seguito le istruzioni) **potrebbero** non aver avuto il tempo di essere convalidate (vi è stato detto di non **muovervi**, siete fuori fuoco) e dovranno essere rifatte. 
* Gli utenti potranno riprendere le loro attività come prima con una fluidità molto maggiore grazie alle nuove applicazioni client.

Dettagli in francese sul [forum tecnico](https://forum.duniter.org/t/chronologie-de-la-migration/12605)

<details> 
  <summary> <b>Autore </b></summary>

[Maaltir](https://forum.monnaie-libre.fr/u/maaltir/summary) del Collettivo MàJ-V2, convalidato dai devs [hugotrentesaux](https://duniter.org/team/hugotrentesaux/), [bgallois](https://duniter.org/team/bgallois/), [Moul](https://duniter.org/team/moul/), [Tuxmain](https://duniter.org/team/tuxmain/), [Vit](https://duniter.org/team/vit/)

</details>

<h2 id="flyers1"><a href="https://forum.monnaie-libre.fr/uploads/short-url/5TgQJGuaeONs7zyBRrfCAle7010.pdf" target="_blank" width="50%" style="color:#34A1FF; font-weight: bold;">Volantino fronte-retro da condividere <img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>

<p><strong>Fare clic sull'immagine per ingrandirla</strong></p>

<div style="float:left; margin:0 1em 0;"><a href="/uploads/it-pdf3-cosa-non-cambia-e-come-accadra-italpaola_page_1.jpg" target="_blank">
<img src="/uploads/it-pdf3-cosa-non-cambia-e-come-accadra-italpaola_page_1.jpg" width="200"/></a><br/> tradotto da @italpaola</div>

<div style="float:left; margin:0 1em 0;"><a href="/uploads/it-pdf3-cosa-non-cambia-e-come-accadra-italpaola_page_2.jpg" target="_blank">
<img src="/uploads/it-pdf3-cosa-non-cambia-e-come-accadra-italpaola_page_2.jpg" width="200"/></a><br/>tradotto da @italpaola</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >