---
title: Comprendre
description: Les explications théoriques
plan: ui/main-flatplan
---
## La TRM - Théorie Relative de la Monnaie

Une monnaie libre est une application rigoureuse de la Théorie Relative de la Monnaie, écrite en 2010 et complétée en 2014 par Stéphane Laborde.

### 4 libertés fondamentales

Stéphane Laborde pose 4 libertés économiques en tant qu'axiomes de la TRM.

1. Liberté de choisir sa monnaie, son système monétaire.
2. Liberté d’accéder aux ressources.
3. La liberté de produire toute valeur économique.
4. La liberté d’échanger, donc estimer, comptabiliser et afficher ces valeurs, “dans la monnaie”.

Ces libertés s'entendent - *très important* - au sens de la non-nuisance. La nature de cette liberté est développée par John Locke, conditionnée par la non-nuisance à soi-même et à autrui. S'il y a nuisance, il y a privation de liberté.

Remarquez que si vous trouvez qu'actuellement vous êtes privés d'une ou plusieurs de ces libertés, ou que vous vivez une nuisance sur l'un de ces points, c'est que vous ne faites pas l'expérience d'une économie libre.

### Renouvellement des générations

![Les humains du passé n'existent plus, les humains du futur n'existe pas encore](https://www.creationmonetaire.info/wp-content/uploads/2013/05/TRM35_6.png "Espace-Temps humain (espace de vie(t) des âges de 0 à ev en vert)")
Un humain du passé n'existe plus, un humain du futur n'existe pas encore. Thomas Paine disait qu'il n'y avait pas plus éloignés que ces deux individus.

Les vivants qui arrivent en fin de vie (ev≈80ans), ont pu échanger avec des humains du passé (en rouge), mais n'échangeront jamais avec les humains du futur.\
Les humains en début de vie n'ont jamais échangé avec les humains du passé, mais échangeront un jour avec les humains du futur (en jaune).\
Seul les humains vivants (en vert) peuvent faire des échanges entre eux, il n'y a pas de raison que leurs échanges influent sur les générations futures, ni qu'ils soient impactés par les échanges des générations passées.

### Égalité spatio-temporelle

Pour respecter les 4 libertés, il faut que chaque humain crée la même part de monnaie (la même portion).\
Chaque génération crée la monnaie qu'elle utilise sans que cette monnaie ait un impact sur les générations futures.

Le flux monétaire doit suivre le flux de la vie humaine, individuellement et collectivement. Pour y parvenir, la croissance monétaire (création de nouvelle monnaie) est une simple fonction logarithmique de l'espérance de vie.

### La formule

DU=c(M/N)\
La part que chaque être humain crée (DU) est une portion, un coefficient (c) de la moyenne de la masse monétaire par membre (M/N).\
Ce coefficient **c**, avec la fonction logarithmique sur l'espérance de vie, est proche de 10% par an. Elle ne privilégie pas les plus jeunes, ni les plus âgés.

Remarquez qu'une espérance de vie à 70 ans ou à 90 ans ne fait varier que très peu ce taux de croissance ; il faudrait une espérance de vie de 30 ans ou de 120 ans pour avoir une différence significative.

NOTA : le DU doit donc être réévalué régulièrement. Les développeurs de la Ğ1 ont décidé de le faire 2 fois par an, aux équinoxes. Et pour obtenir une meilleure symétrie au long d'une vie humaine, le calcul logarithmique du taux de croissance se fait sur une demi espérance de vie, soit 40 ans. Ce sont les choix humains qui ont été faits avec Stéphane Laborde pour la première monnaie libre, une autre monnaie libre pourrait en choisir d'autres.

### Convergence des comptes

Chaque créateur créant la même quantité de monnaie, leurs comptes se rapprochent relativement. Un peu comme une différence d'âge qui devient de moins en moins sensible avec le temps.

![](/uploads/convergence-des-soldes.png)

## Plus d'informations

#### La TRM.

Pour en savoir plus vous pouvez lire la Théorie Relative de la Monnaie. Ce livre écrit par Stéphane Laborde est disponible en ligne : <http://trm.creationmonetaire.info/>   

#### La TRM en détail.

Emmanuel Bultot, docteur en mathématiques, a publié [« la TRM en détail »](http://monnaie.ploc.be/#trm-en-detail), une revisite de la Théorie Relative de la Monnaie depuis un nouveau point de vue, très bien réalisée et vivement recommandée.

#### La TRM pour les enfants.

David Chazalviel, ingénieur informatique, a réalisé [« la TRM pour les enfants »](http://cuckooland.free.fr/LaTrmPourLesEnfants.html) une explication de la TRM interactive plus facile à comprendre. 

### En vidéo

Une vidéo explicative de la TRM en 1 h 15.

<iframe width="560" height="315" src="https://www.youtube.com/embed/PdSEpQ8ZtY4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## La Ğ1

La Ğ1 (la "June") est la première MONNAIE LIBRE de l'histoire de l'humanité.\
Conformément à la Théorie Relative de la Monnaie (TRM), elle est cocréée sans dette, et à parts égales, entre tous les êtres humains de toutes les générations présentes et à venir, sous la forme d'un "paquet" de monnaie, une quantité de Ğ1 (une quantité de "junes"), appelée DIVIDENDE UNIVERSEL (DU).

Oui c'est déjà écrit sur d'autres pages, mais nous aimons bien le redire ;-).

Les enfants sont des êtres humains à part entière, ils peuvent participer à la création monétaire en créant tous les jours leur part de monnaie ! Ces comptes créateurs sont dans les mains de leurs parents, cela pose donc des questions de société (les parents vont-ils utiliser cette création monétaire à leur guise ? ce compte créateur va-t-il servir à certifier d'autres comptes ?...), mais pas des questions de création monétaire.

### Le DUğ1

Pour la monnaie libre Ğ1 le DU est calculé tous les semestres (182.625 jours) le coefficient est donc de 4,88% par semestre, et il est réparti sur tous les jours.\
Tous les jours, chaque cocréateur crée une petite portion de Dividende Universel.\
La masse monétaire de départ étant à zéro, le premier DU a été fixé arbitrairement à 10 Ğ1 par jour, et depuis il augmente.

### La formule adaptée

La formule de calcul du DUğ1 est : **DU<sub>t+1</sub> = DU<sub>t</sub> + (c<sup>2</sup> × (M/N)<sub>t</sub> / 182.625)**\
Ce calcul est fait tous les six mois (182,625 jours) il tient compte de l'évolution du nombre de membres pendant la phase d'installation de la monnaie.

### La blockchain

La technologie blockchain a été choisie pour sa simplicité de mise en place, sa sécurisation et sa décentralisation.\
Contrairement aux idées reçues ce n'est pas la blockchain qui consomme de l'énergie, mais la preuve de travail. \
Pour la Ğ1 l'algorithme a été **[conçu pour consommer vraiment très peu d'énergie](https://duniter.fr/faq/duniter/duniter-est-il-energivore/)**, un ordinateur de la taille d'un paquet de cigarette suffit pour calculer les blocs, bien loin des immenses fermes à bitcoin.

### La toile de confiance

La monnaie étant créée sur les comptes des utilisateurs membres, il faut s'assurer que chacun ne possède qu'un seul compte créateur de monnaie.


Pour respecter la décentralisation et ne donner le pouvoir à aucun organisme, ce sont les membres eux-mêmes qui identifient les autres membres