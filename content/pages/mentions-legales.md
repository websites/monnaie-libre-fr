---
title: Mentions légales
---

## Hébergeur

1&1 / IONOS
7 Place de la Gare
57200 Sarreguemines
Tél : 09.70.80.89.11

## Limitations de responsabilité

Ne pourra être tenu pour responsable des dommages directs et indirects causés au matériel de l’utilisateur, lors de l’accès au site https://monnaie-libre.fr.

Décline toute responsabilité quant à l’utilisation qui pourrait être faite des informations et contenus présents sur https://monnaie-libre.fr.

S’engage à sécuriser au mieux le site https://monnaie-libre.fr, cependant sa responsabilité ne pourra être mise en cause si des données indésirables sont importées et installées sur son site à son insu.

Des espaces interactifs (espace contact ou commentaires) sont à la disposition des utilisateurs. se réserve le droit de supprimer, sans mise en demeure préalable, tout contenu déposé dans cet espace qui contreviendrait à la législation applicable en France, en particulier aux dispositions relatives à la protection des données.

Le cas échéant, se réserve également la possibilité de mettre en cause la responsabilité civile et/ou pénale de l’utilisateur, notamment en cas de message à caractère raciste, injurieux, diffamant, ou pornographique, quel que soit le support utilisé (texte, photographie …).

## CNIL et gestion des données personnelles.

Le site https://monnaie-libre.fr ne collecte aucune donnée des visiteurs.

## Liens hypertextes et cookies

Le site https://monnaie-libre.fr contient des liens hypertextes vers d’autres sites et dégage toute responsabilité à propos de ces liens externes ou des liens créés par d’autres sites vers https://monnaie-libre.fr.

La navigation sur le site https://monnaie-libre.fr installe des cookies sur l’ordinateur de l’utilisateur uniquement pour :

- la gestion de la langue affichée (i18n)
- l'authentification des administrateurs

Un "cookie" est un fichier de petite taille qui enregistre des informations relatives à la navigation d’un utilisateur sur un site.
