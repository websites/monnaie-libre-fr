---
title: Videos à partager
description: Quelques vidéo très bien faites à partager
---
## La TRM

### RML11 - Théorie relative de la monnaie, métrique, topologie et libertés (1 h 50) 

<iframe title="RML11 - Théorie relative de la monnaie, métrique, topologie et libertés" width="560" height="315" src="https://tube.p2p.legal/videos/embed/62ea65c4-a9e9-4d7f-88ae-37f273e6898f" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

### La Théorie Relative de la Monnaie par S. Laborde (2014)  université d'été du MFRB. 1 h 13

<iframe width="560" height="315" src="https://www.youtube.com/embed/PdSEpQ8ZtY4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Présentations de la monnaie libre

### La monnaie libre sans jargon par Corinne (15 minutes)

<iframe title="La monnaie libre sans jargon" width="560" height="315" src="https://tube.p2p.legal/videos/embed/98e11a49-83f5-4974-ad88-c9b4854dacfd" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

### Une nouvelle vision de la monnaie avec La Ğ1 (vision crypto-monnaie) (37 minutes)

<iframe width="560" height="315" src="https://www.youtube.com/embed/jZ-ZeFmSiMI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Crypto-monnaies et revenu universel - 29mn

<iframe width="560" height="315" src="https://www.youtube.com/embed/U1qbHaLCQNU?si=OelphB1s3H0DIyuC" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## La toile de confiance 
### RML11 Douarnenez 2018 la Toile de Confiance @elois 1h26
<iframe width="560" height="315" src="https://www.youtube.com/embed/8GaTKfa-ADU?si=7aFWJJYHKR3UwzeI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## Quelques tutoriels

### G1nkgo
G1nkgo Présentation du porte-monnaie : 12 min 44 sec
<iframe title="Application G1nkgo : Porte Monnaie pour vos Evénements, Gmarchés, Apéro ..." width="560" height="315" src="https://runtube.re/videos/embed/8bbf176d-417c-4a16-9f53-0e89b77cec7b" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

g1nkgo : export/import de portefeuille. 3 min 15 sec
<iframe title="tuto g1nkgo : export/import" width="560" height="315" src="https://tube.p2p.legal/videos/embed/106ba155-4a90-4181-9223-fd7e8f021536" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## E﻿xpériences utilisateurs

### 17 membres expliquent la Monnaie Libre : (6 min 18)

<iframe width="100%" height="410" sandbox="allow-same-origin allow-scripts allow-popups" src="https://tube.p2p.legal/videos/embed/ea3c06b6-134b-49da-aea2-7a8bacf97dd5" frameborder="0" allowfullscreen></iframe>

### [RML12] Mon expérience utilisateur : 1 an 1/2 d'échanges avec la june Ǧ1 - Pi Nguyen (43 min)

<iframe title="[RML12] Mon expérience utilisateur : 1 an 1/2 d'échanges avec la june Ǧ1 - Pi Nguyen" width="560" height="315" src="https://tube.p2p.legal/videos/embed/716af47c-9f91-4d2e-babf-0110f99e1ae6" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

## Informations sur les monnaies

###  Monnaies, monnaies, monnaies ! - #DATAGUEULE 30.  3 min 38 sec

<iframe width="560" height="315" src="https://www.youtube.com/embed/l3ZS2OZN988?si=SC2a9QjrZEReYNQI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Qui crée l'argent? Et comment? - Heu?reka #6. 11 min 18 sec.
<iframe width="560" height="315" src="https://www.youtube.com/embed/lZ6CmwquKKQ?si=I6RGzmeqm_CgtAUR" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Le double visage de l'argent - Heu?reka #7. 12 min 53 sec
<iframe width="560" height="315" src="https://www.youtube.com/embed/NKYBz-E_g9U?si=Ucru6uP7UV-TAcIG" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Une petite introduction sur les monnaies alternatives. 18 min 28 sec

<iframe width="560" height="315" src="https://www.youtube.com/embed/iy3nEy4nPvI?si=GCwvI5OWkTY0YJa-" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


## Film sympa à voir


### La monnaie miraculeuse 1 h 28
<iframe width="560" height="315" src="https://www.youtube.com/embed/Zy56N2kE7lc?si=991mnHA8vDy_BeGI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
I﻿nspire d'une histoire vraie  
En 1932, l'Europe est ravagée par une crise économique sans précédent. Il n'y a plus de travail nulle part. Le village de Wörgl, en Autriche, meurt à petit feu, faute de pouvoir faire repartir son usine. Le nouveau bourgmestre, Michael Unterguggenberger, décide de créer une monnaie locale et relance l'activité de la commune.

## D'autres vidéo vous semblent intéressantes à partager ? 
Si vous avez d'autres vidéos intéressantes et bien claires pour être partagées, faites le savoir à [Maaltir sur le forum](https://forum.monnaie-libre.fr/u/maaltir/activity) ou par <a href="mailto:maatril-g1june@yahoo.com?subject=Proposition de vidéeo à ajouter sur le site web">mail </a>