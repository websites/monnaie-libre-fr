---
title: La monnaie libre et la démocratie
description: Qu'est-ce que la monnaie libre peut apporter à la démocratie ?
---
## Demos (peuple) Kratos (pouvoir)

La monnaie, depuis qu'elle existe, a toujours été source de pouvoir. A titre individuel, plus on a de monnaie, plus on a de pouvoir. Chacun(e) peut le vivre, directement ou indirectement.
En tant qu'autorité, ou structure de pouvoir, les monarches ou les États se sont toujours appropriés ce pouvoir de création monétaire. Depuis peu c'est la banque qui se l'est arrogé. Avec Woodrow Wilson aux Etats-Unis, puis pays par pays, du moins les pays occidentaux ou sous le controle des pays occidentaux. En France c'était en 1973 (un 3 janvier après le foie gras ;-) avec Pompidou et son ministre Giscard d’Estaing. En Europe, ce pouvoir a été placé dans une zone intouchable, par plus aucun Etat, avec le traité de Maastricht (art.104) versus traité de Lisbonne (art. 123), imposé par les élus parce que le peuple avait mal voté la première fois.

## Prendre ce pouvoir.

Avec la monnaie libre, le choix de création et l'injection de monnaie dans l'économie pour financer une chose ou une autre, n'est plus le privilège des banquiers ou des ministres (via la création monétaire qui se fait actuellement, massivement, par crédit bancaire à nos dépends). 
Elle est réalisée par chaque individu, uniquement via le DU qui est créé chaque jour (sans dette ni contre-partie de quelque nature que ce soit) sur son compte créateur. Chaque individu injecte lui-même la monnaie dans l'économie quand il dépense ses Ğ1 nouvellement créées. 

Ce faisant, au jour le jour et sans représentant, il vote de façon active et exprime ses valeurs ; il influence lui-même et directement la société dans laquelle il s’inscrit. On peut y voir une condition fondamentale d'une démocratie.

## Égalité.

La déclaration des Droits de l'Homme, nous déclare tous (hommes et femmes ;-) égaux en droits. L'idée est belle et vaut la peine d'être défendue. Dès lors que la justice ou la relation avec le droit, est monétisé, de quelque manière que ce soit, elle bafoue notre constitution. La monnaie libre ne résout pas ce problème, mais...

La monnaie libre nous déclare tous égaux devant la création monétaire, ici et maintenant, là-bas et à jamais. 
De plus, la convergence des comptes vers la moyenne, propriété mathématique de cette formule de création monétaire, garantit un équilibre dans la longue durée du point de vue du pouvoir monétaire individuel.

L'égalité n'est pas de ce monde, il faut donc créer de toute pièce des équilibres. Il y aura toujours des déséquilibres de richesses, d'influences, mais la monnaie libre nous offre une convergeance automatique, un équilibre monétaire alimenté tous les jours.
