---
title: DUNITER V2 in english
description: Version 2 for Ğ1 software
---
<div style = "display:inline-block;float:left;">
<img src="/uploads/demenagement-v1-v2.jpg" width=400px align="center" style="margin:auto;"/>
</div>
<div style = "display:inline-block;float:left; padding-left : 20px;">
<p style="color:#34A1FF;" id="langues">Other languages &nbsp; &nbsp;&nbsp;</p>

[En français](/maj-v2/)  

[En español](/duniter-v2-en-espanol/) 

[In-italiano](/duniter-v2-in-italiano/)

[Em português](/duniter-v2-em-portugues/)

[Auf-deutsch](/duniter-v2-auf-deutsch/)

</div>

<p style="float:left;"><strong> Machine translation with \[deepl](https://www.deepl.com/fr/translator).</strong> </p>

<div style = "display :inline-block; margin:5px auto 5px; text-align:center; width:400px; border:solid #3FB2FF 1px; background-color:#3FB2FF;border-radius: 10px; padding: 1em;">
<a style="font-size:1.5em; font-weight: bold; color: white;" href="https://www.helloasso.com/associations/axiom-team/collectes/finalisation-de-cesium-v2-et-duniter-v2">crowdfunding<img src="/uploads/crowfunding100.png" style="padding:5px;margin:auto;"/></a>
</div>

<h2 style="color:#34A1FF;" id="plan">Communication plan</h2>

When will you be informed of the project's progress? 

1. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">October :**</strong> [Why change?](#pourquoi)</p>
2. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">November - December :** </strong> [what will change](#evolutions) </p>
3. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">January :**</strong> [what won't change](#identique) <br/> [and **how it will happen**](#bascule) </p>
4. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">February :**</strong> satellite software : **discovering, testing and using** </p> 

</div>

<hr style=" border: 2px solid #34A1FF;margin:50px auto;"/>

<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="300" align ="center" >

<h2 style="color:#34A1FF;" id="pourquoi">Why change?</h2>

<h3 style ="color: #F7A212" id="inconvenients">1/ Disadvantages of V1</h3>

#### The initial disadvantages are most obvious to developers:

* The main reason motivating this migration: **Difficulties in maintaining <lexique>Duniter</lexique> V1 (fixing bugs and making improvements).**
* Security issues,

  * Duniter can easily be blocked (network saturation attack).
  * All member accounts are potentially forgerons[^1], yet some are easily hackable, which would allow an attacker to potentially compromise the entire blockchain.
* Requirement for <lexique title=‘forgeron’>blacksmiths</lexique> to manually resynchronise to keep things running.
* Risk of fork* if the <lexique title=‘forgeron’>blacksmiths</lexique> do not update <lexique>Duniter</lexique> quickly in the event of a bug fix.

[^1]: a blacksmith is a member of the TDC (Trusted Web) who has downloaded the Duniter software onto his device (computer or other), which becomes a server (or node) to run the June.

<alert type="info">

<details> 
  <summary> *Click here to find out : What is a fork</summary>

 A fork is when two nodes calculate the same block at the same time, resulting in 2 versions of the same block, and therefore 2 versions of the blockchain to follow.
 A fork can resolve itself, without any loss of data (this is normal when the nodes are up to date),\
 It can be resolved with a loss of data, which is quite common in V1 (when there are problems with synchronisation or updating nodes).\
 It may not resolve automatically, resulting in a split in the blockchain (some users do not see the same thing as others).\
 If the fork is not resolved (automatically or manually) this can lead to a split in the community.\
 **More details in French by following these links:**\
https://journalducoin.com/lexique/fork/\
https://www.coinbase.com/fr/learn/crypto-basics/what-is-a-fork\
https://fr.cryptonews.com/exclusives/c-est-quoi-un-fork-5803.htm

</details>
</alert>

#### All users have already noted other disadvantages:

<img style="float:right; width:15%;" src="../uploads/escargot-inconvenients.png"/>
* Slow execution.

* Long time to display transactions
* Between 5 and 10 min for a transaction to be processed, 30 minutes for it to be validated
* Pool synchronisation problems[^1]: Different views from one node to another, as no node has a global view of the pools.
* Transactions or certifications that do not go through.
* Certifications that disappear.
* Difficulty getting new certifiers in (synchronisation problems between certifiers to have 5 certifiers available at the same time)[^2].    

    [^1]: pool: if a member issues several certifications in less than 5 days, they will remain suspended and will only be recorded in the blockchain one by one every 5 days (as stipulated by the TDC rules but without it being known in what order they will be recorded).\
    [^2]:Knowing that member certifications come before those of applicants, a future member will have to wait until the others have passed before being certified.

<h3 style ="color: #F7A212" id="avantages">2/ Advantages of v2</h3>

* Easy maintenance and updates for developers
* Automatic updates without a fork for blacksmiths 
  *(the possibility of refusing an update will still exist, but it will be voluntary and no longer due to forgetfulness)*.
* One block every 6 seconds\
  <img style="float:right; width:18%;" src="../uploads/papillon-avantages.png"/>

  * Transaction validated in 30 seconds
  * Significantly improved response time
* Faster and more reliable node synchronisation
* Certifications validated immediately (no pools)
* Easier entry for new certifiers (no need for synchronisation between the first 5 certifiers)

<h3 style ="color: #F7A212" id="evolution">3/ Evolution of software, not money</h3>

The software is constantly evolving, and to date we are on version 1.8.7 of Duniter, and 1.7.13 of Cesium. 
These upgrades have been made in such a way as to remain compatible with previous versions, and have always passed without a problem. 

Version 2 is still open source software, and is a new blockchain that will start with all the data from the Ğ1 V1 blockchain at the time of the switchover (accounts, transactions, certifications, etc.).
**All your Ğ1s, certifications and transactions will still be there** .

<alert type="info"> 
\*Cesium+ data (profiles, messaging, notifications, etc.) is currently being processed, and may not be available from the start-up date!\*
</alert>

A new blockchain requires new software (Césium 2, Gecko, Tikka, G1nkgo 2, ....).

It will not be possible to trade between two different blockchains, so after the switchover date you will need to have installed the new version of the software to continue trading ğ1 with the rest of the community.

**To make sure you don't miss out on this changeover, stay tuned to this site, the forum and the social networks!**

<h2 id="flyers1"><a href="https://forum.monnaie-libre.fr/uploads/short-url/uM8FyjFvUqHt6mfYAKbCOfBDj8x.pdf" target="_blank" width="50%" style="color:#34A1FF; font-weight: bold;">Double-sided flyer to share
<img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>

<p><strong>Click on the image to enlarge</strong></p>

<div style="float:left; margin:0 1em 0;"><a href="/uploads/why-change-to-english-joam1.jpg" target="_blank">
<img src="/uploads/why-change-to-english-joam1.jpg" width="200"/></a><br/>translated by @jooam</div>

<div style="float:left; margin:0 1em 0;"><a href="/uploads/why-change-to-english-joam2.jpg" target="_blank">
<img src="/uploads/why-change-to-english-joam2.jpg" width="200"/></a><br/> translated by @jooam</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >

<h2 style="color:#34a1ff" id="evolutions"> Developments (what will change for users)</h2>

<h3 style="color:#f7a212" id="logiciels">1/ New software</h3>
In order to be able to continue exchanging with the other junists, it is essential that they all change version as soon as they connect for the first time after the V2 blockchain is launched. 

* **Cesium** The most widely used software will offer its V2 version. There are no plans to switch to Duniter V2 before this software is ready. 
* **Gecko** for smartphone payments is ready, with a few adjustments.
* **Tikka**, a computer-based accounting software for professionals, is currently under development. 
* **Ğ1nko** a wallet to facilitate transactions in G-markets. Version 2 may be ready for migration.
* **Ğ1superbot** will also have a version for Duniter V2. 
* **Duniter-Connect**, a browser extension enabling transfers to be made from any site, is evolving as Duniter develops.
* **Ğcli** The command-line client (for technicians), similar to Silkaj, is also evolving as Duniter develops.
* ...

- - -

<h3 style="color:#f7a212" id="cles">2/ New form of Public Keys</h3>
With the new Duniter V2 blockchain, what we call public keys will change their encoding. So your public key will no longer look like the one you know. You'll probably have to reprint your QR-code for those who printed it.

<img style="float:right; width:15%;" src="../uploads/adresse-cle.png"/>

We'll be talking about addresses rather than keys, they'll all start with ‘g1...’, so use the end of the address rather than the beginning, to recognise your account.

In Duniter v2, we will be encouraging the use of an address rather than a simple public key. Using an address avoids mistakes such as copying and using a key on the wrong network.

**To assist you, for accounts created before the update, Cesium2 will display the old ‘public key’ as a reminder. The same applies to G1nkgo, which will display this old key**.

- - -

<h3 style="color:#f7a212" id="depot">3/ Existential deposit</h3>

In blockchain v1, accounts with less than 1 Ğ1 disappear without the user being notified (currency destruction).\
In blockchain v2, it will be impossible to go below 1 Ğ1 without explicitly requesting that the account be closed.

- - -

<h3 style="color:#f7a212" id="piscines">4/ Disappearance of waiting times at  pools</h3>
 <img style="float:right; width:18%;" src="../uploads/nageur.png"/>

* Certifications will be taken into account immediately, whether the certified person is waiting for other certifications or not.
* The 5-day period between two certifications must be respected because DuniterV2 only allows one certification every 5 days.   
* Some clients (such as Césium) may offer to add your certification intentions to an ‘address book’ (not yet developed).

- - -

<h3 style="color:#f7a212" id="adhesions">5/ Membership process (to become a co-creator of currency)</h3>

* New accounts will be simple portfolio accounts only. 
* You must have a few Ğ1 to start the certification process. It will therefore be impossible to certify an account with zero ğ1
* The first certification constitutes an invitation to become a member (no need to apply for membership).
* Acceptance of the invitation requires the new member to choose a nickname within 48 hours (this period is subject to change).
* Once the nickname has been registered, the following certifications are possible.
* When the account has 5 certifications that comply with the distance rule, it becomes a co-creator of currency.
* No need to synchronise for certifications (you won't be able to certify if you're not available).
* The two-month deadline for obtaining the first five certifications meeting the distance rule will still apply. (The length of the deadline may change)
* If the deadlines are exceeded, the certifier will recover his certification from his stock of certifications to be issued.

- - -

<h3 style="color:#f7a212" id="securite">6/ New, more secure accounts</h3>

Authentication by secret identifier and password is not sufficiently secure for a currency (your banks now require double authentication).

<img style="float:right; width:18%;" src="../uploads/coffre.png"/>

1. The old accounts, with secret login and password, will still be usable with Césium, as before the V2 switchover.
2. **New accounts will be created using a 12-word mnemonic.** These 12 words should be kept carefully out of sight, as they will enable you to retrieve your accounts on other devices or other portfolio management software (Cesium, Gecko, Tikka, etc.).
3. Several accounts can be created using the same mnemonic (this is known as a ‘trunk’). A trunk can contain several accounts or just one. 
4. Once the trunk has been created, a code of 4 or 5 letters or numbers will suffice to access all the accounts in the trunk as long as you use the same device. 
5. An account created with a mnemonic will be usable on Cesium, Gecko Tikka, Gcli and probably other software.   

- - -

<h3 style="color:#f7a212" id="migration">6 bis / Migration to a more secure account</h3>

**Only for those who want it** .
<img style="float:right; width:18%;" src="../uploads/gecko-4244388_640.png"/>

1. Gecko is designed to work only with accounts created by mnemonic, for security reasons. For other software, nothing definitive yet.
2. Gecko offers to ‘migrate’ your old accounts (id/mdp) to a new account that you have created with a mnemonic.
3. This can allow you to group all your accounts in a single safe deposit box, which is easier to use.
4. Migration of a member account means transferring your identity with nickname, certifications, whether or not you are a member, and ğ1. 
5. Migration of a single wallet account, is simply a transfer of all ğ1.
6. Once migrated, the new account (with mnemonic) will be usable for all your transactions whatever the portfolio management software (Césium, Gecko, Tikka, Gcli) and the old account will only be an empty portfolio.
7. The old account can still be used like any other portfolio. But this would defeat the purpose of having migrated it.

- - -

<h3 style="color:#f7a212" id="frais">7/ Fees and quotas</h3>

#### The capacity of a blockchain is not infinite.

A blockchain needs computing power and storage space. Although both are substantial, they are not unlimited. One possible attack is to saturate the computing power by sending billions of transactions per second. 
Other blockchains charge a fee for each action to discourage this saturation of computing and storage space.

#### But Ğ1 is not like the others.

#### 1- Fees will only be charged if the blockchain is overloaded.

* A total number of actions **(transactions, certifications, membership, etc.)** per block **has been assessed as the** limit for ‘normal ’ **operation.** Beyond this limit **the blockchain is considered to be** saturated\*\*. 
* It is only if the number of shares in a block **exceeds this limit** that **fees are levied**. Fees estimated at approximately 0.015 DUĞ1, or 17 Ğ1 per 100 transactions.

#### 2- In addition, the fees will be reimbursed to all members of the web of trust *(this is the strength of our Blockchain)*

<img style="float:right; width:18%;" src="../uploads/blockchain_humain.png"/>

* A quota of actions per member and per block is defined (still allowing many transactions)
* Non-member accounts may be linked to a member account and also be reimbursed for their fees, up to the limit of the quota per member.
* A member can make a single transaction, but if, at the same time, someone initiates 1 million transactions on 1 million accounts, the blockchain will be saturated, so that member will be charged fees.
* Then it will be refunded because it does not exceed the quota per member.
* If it is a member and their linked accounts that initiate hundreds of transactions resulting in saturation, that member will only be reimbursed on their first transactions, subsequent ones being above the quota.
* If a member initiates hundreds of transactions even beyond the quota per member but is alone in making transactions at that time, it can pass without saturating the blockchain, so no fees. 
* However, there is no refund possible in the event of account closure! 
* There will be no refunds for anonymous accounts.

*It is unlikely that such an attack would be launched, as it would lead to the attacker's ruin, for a temporary blockage. **These costs are therefore a deterrent**.*

More information in French on [the technical forum](https://forum.duniter.org/t/les-frais-ca-devient-concret/11877/1)

- - -

<h3 style="color:#f7a212" id="forgerons">8/ Blacksmiths sub-web</h3> 
    
Today, any member can install a node and forge blocks, which leads to some update and synchronisation faults as well as security problems, as some users are unaware of the security flaws in their installation and ‘forget’ to update.       

With Duniter V2, only members of the blacksmiths subnet will be able to forge blocks.\
Anyone can still run a mirror node that doesn't write blocks but responds to client requests.\
As the forger nodes focus on calculating and writing blocks, they will no longer respond to client requests.\
All the nodes communicate with each other almost instantaneously.   

<img src="/uploads/blacksmith2medaille.png" width="15%" align ="right" >

Blacksmith **certifications** will have to comply with a blacksmith **licence** which ensures **a good level of security**, among other things having already been running a mirror node correctly for some time, being able to **keep your server open** 24/7 and having a good internet connection.\
Members of this sub-net do not necessarily need to know each other or see each other physically, as they must first be members of the Co-Creators' Web of Trust. 

To be part of this forger sub-web from the start, you need to run a node before switching to gdev or gtest (the currencies used to test version 2 before starting up).

A **documentation** explaining how to **install a node** and become a Blacksmith in V2 is currently being written and will be communicated as soon as it is completed.

**Current smiths** are invited, if interested, to try installing Duniter V2, to see how it works, detect any installation problems and help write the ‘becoming a blacksmith’ documentation.     

It is/will be possible to use [Yunohost](https://yunohost.org/fr) or [Docker images](https://www.ionos.fr/digitalguide/serveur/know-how/les-images-docker/).

More information in French on [the technical forum](https://forum.duniter.org/t/video-installer-un-noeud-duniter-v2-en-moins-de-dix-minutes/11243/1)

- - -

<h3 style="color:#f7a212" id="fontionalites">9/ Future features</h3> 

New features may be implemented after the launch of Duniter V2, if developers are available to work on them.

* Automatic transfers
* Delegate authority over an account
* Multiple signature accounts
* Identity-linked portfolio accounts
* Right to be forgotten *(deleting comments?)*
* Possibility of setting up votes!
* And more, depending on your imagination...

<details> 
  <summary> <b>Author </b></summary>

[Maaltir](https://forum.monnaie-libre.fr/u/maaltir/summary)  by Collectif MàJ-V2, validated by  [hugotrentesaux](https://duniter.org/team/hugotrentesaux/), [bgallois](https://duniter.org/team/bgallois/), [Moul](https://duniter.org/team/moul/), [Tuxmain](https://duniter.org/team/tuxmain/)

</details>

<h2 id="flyers1"><a href="https://forum.monnaie-libre.fr/uploads/short-url/3uKlR64JYN3BiGAleP8EFyRc1W9.pdf" target="_blank" style="color:#34A1FF; font-weight: bold;">Double-sided flyer to share
<img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>

<p><strong>Click on the image to enlarge</strong></p>

<div style="float:left; margin: 0 1em 0;"><a href="/uploads/what-will-change-in-english1.jpg" target="_blank">
<img src="/uploads/what-will-change-in-english1.jpg" width="200"/></a><br/> translated by @Jooam</div>

<div style="float:left; margin: 0 1em 0;"><a href="/uploads/what-will-change-in-english2.jpg" target="_blank">
<img src="/uploads/what-will-change-in-english2.jpg" width="200"/></a><br/>translated by @Jooam</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >

<h2 style="color:#34a1ff" id="identique"> What's not going to change</h2>

* You will find all your Ǧ1 on the new version.
* Validated certifications will keep the same expiry date in V2. 
* Currency creation rules remain the same (1 DUğ1 per day revalued every 6 months). 
  <img src="/uploads/rienechange.png" width="18%" align ="right" >
* The rules for being a member of the Web of Trust remain the same: 5 certifications minimum respecting the distance rule.
* The Ğ1 is still the leading free currency and still complies with the M.R.T.
* DuniterV2s, Cesium2, Gecko, Tika, G1nkgo, Ğ1superbot are still free software.
  **The many discussions and questions** surrounding this V2 update have brought to light more clearly **things that already existed in V1**; 
* **The predominance of the developers**: since the start of Ğ1, they have been the ones choosing which updates to make. Once V2 gets underway, we'll be able to discuss a decision-making system at length. 
* **The power of the blacksmiths**, who decide whether or not to accept updates. In V2, blacksmiths will no longer need to decide whether or not to accept an update; updates will be made automatically unless they refuse (principle of freedom).
* **The ability to exchange for other currencies**: Everyone has always been free to exchange ğ1 for other currencies. Other currencies are objects to which everyone is free to give a value. [See here an extract from the TRM in French](https://forum.monnaie-libre.fr/t/g1-et-plateformes-de-change/29900/61) 
* **The implementation of the Ğ1 on exchange platforms** is no more planned in V2 than in V1. But it is in any case impossible to prevent anyone from creating such a possibility (it's the FREE world). If it hasn't already been done, it's only because no-one has found it sufficiently interesting.\
  Further explanations in French on the forum: https://forum.monnaie-libre.fr/t/g1-et-plateformes-de-change/29900/78

<h2 style="color:#34a1ff" id="bascule">How will the switchover work for users?</h2> 

<h3 style="color:#f7a212" id="date">A/ Switchover date</h3>

The switchover date has not yet been defined, it depends on the progress of developments and therefore also on their funding. [Support the devs](https://www.helloasso.com/associations/axiom-team/collectes/finalisation-de-cesium-v2-et-duniter-v2). 

<img src="/uploads/code-geek.png" width="18%" align ="right" >

For now, developers are aiming for 8 March, the anniversary of Ğ1. 

This blockchain changeover is a major project that will make things easier for June users.

The MàJ-V2 collective is communicating as much as possible about this switchover, so that all junists are comfortable with this update to their software, and can talk about it. 

* on the forum: ‘[What's going to happen: any questions left?](https://forum.monnaie-libre.fr/t/comment-cela-va-se-passer-encore-des-questions/31042) (invisible topic for the moment)
* on Telegram: Telegram: Contact [@monnaielibrejune](http://t.me/monnaielibrejune/57035)

<h3 style="color:#f7a212" id="scenario">B/ V1 to V2 switchover scenario</h3>

#### The end of V1

**The switchover scenario has not yet been formally established** .

 The time **0** (date and time) at which Duniter 2.0 will start has not yet been definitively chosen.

* 5 or 6 weeks before time zero, a message will appear in Cesium indicating the date of the switchover and informing that there will be an update (automatic or manual) of the client software. 
* A final Cesium update will be available, for security reasons, to prevent any action being taken on V1, which could be lost. 
* 30 days before the actual switchover, there will be a test switchover. Those who so wish will be able to test the available software with the Ğtest. 
* Approximately 1 hour before the **0** moment, an image of the Ğ1 will be taken.  

<img src="/uploads/photo.jpg" width="18%" align ="right" >

* It will be preferable to avoid taking any action on the Ğ1 a few minutes before the image is taken (‘Don't move!’, CLIC photo). A final communication campaign will be launched on as many platforms as possible to encourage people to stop using the Ğ1 until it has been moved to v2.
* All **actions on V1 after this photo** will be recorded in the V1 blockchain if (despite everything) nodes continue to run in V1, but **will be lost for V2**.
* The V2 blockchain will be launched from this picture. 
* **The Ǧ1 will be unavailable for a few hours during this reboot phase.**

#### After V2 has been launched.

* Users will receive or will be invited to **update each of their installations** (computer, tablet, phone, Firefox, Brave...) of the client application (Césium 2.0) If all goes well, the update will be automatic. Normally, previous versions of Césium will no longer work.
* **If the cesium update is not immediate**, it doesn't matter. Simply do it before your next connection, or install another client software (Gecko, Tikka, G1nkgo, etc.). 
* Preferably, you should check this **update before going to a ğmarket**, to enjoy a good internet connection at home. 
* After updating or installing V2 client software, **users will find their accounts**, their transactions and their certifications, except for what was waiting to be recorded in the blockchain (pool).

<img src="/uploads/lav2.png" width="18%" align ="right" >

* The DU will continue to create itself on member accounts.
* Unvalidated membership applications and pending certifications (in the pool) will disappear. **All you have to do is redo them**.
* Transactions made at the time the image was taken (for those who didn't follow the instructions) **may** not have had time to be validated (you were told **not to move**, you're out of focus) and will also have to be redone. 
* Users will be able to resume their activities as before with much greater fluidity thanks to the new client applications.

Details in French on the [technical forum](https://forum.duniter.org/t/chronologie-de-la-migration/12605)

<details> 
  <summary> <b>Author </b></summary>

[Maaltir](https://forum.monnaie-libre.fr/u/maaltir/summary) by Collectif MàJ-V2, validated by devs [hugotrentesaux](https://duniter.org/team/hugotrentesaux/), [bgallois](https://duniter.org/team/bgallois/), [Moul](https://duniter.org/team/moul/), [Tuxmain](https://duniter.org/team/tuxmain/), [Vit](https://duniter.org/team/vit/)

</details>

<h2 id="flyers1"><a href="https://forum.monnaie-libre.fr/uploads/short-url/k1W5u46pQSnrVqKOClRwdjTFpWd.pdf" target="_blank" style="color:#34A1FF; font-weight: bold;">Double-sided flyer to share <img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>

<p><strong>Click on the image to enlarge</strong></p>

<div style="float:left; margin: 0 1em 0;"><a href="/uploads/gb-_what_remains_the_same_and_transition_process-sophie-t_page_1.jpg" target="_blank">
<img src="/uploads/gb-_what_remains_the_same_and_transition_process-sophie-t_page_1.jpg" width="200"/></a><br/> translated by @Sophie T</div>

<div style="float:left; margin: 0 1em 0;"><a href="/uploads/gb-_what_remains_the_same_and_transition_process-sophie-t_page_2.jpg" target="_blank">
<img src="/uploads/gb-_what_remains_the_same_and_transition_process-sophie-t_page_2.jpg" width="200"/></a><br/>translated by @Sophie T</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >