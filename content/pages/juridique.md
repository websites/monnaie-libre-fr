---
title: Juridique
description: Documentation juridique française
---

Crypto-monnaie, troc, salaire en Ğ1, actifs numériques, loi PACTE, directive MiCA, portefeuille de jetons comptable... Afin de comprendre quelque-chose à ce charabia, cette page recence des travaux et de la documentation qui permettent d'y voir plus clair.

# Documentation juridique et comptable financée par l'ADEME

Un ensemble de documents juridiques à propos de la Ğ1 a été financé par l'ADEME.

Cette documentation a pour base des travaux réalisés pour l'occasion par le cabinet d'avocats [Metalaw](https://www.metalaw-avocats.fr/). Elle réalise un état des lieux de la législation française par rapport à la monnaie libre et plus spécifiquement par rapport à la Ğ1. La dernière version date de juin 2022 et a été publiée en novembre 2023. Le groupe de travail à l'origine de ces documents prévoit de réaliser une révision ponctuelle afin de tenir cette documentation relativement à jour au fil de l'évolution de la loi.

Elle est composée des documents suivants&nbsp;:

*   La note juridique, fiscale et comptable réalisée par Metalaw
*   Le guide juridique de la Ğ1
*   Un flyer qui résume les points essentiels pour un usage quotidien

Une section du forum a été réservée à la discussion autour de ces documents.
