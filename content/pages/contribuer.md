---
title: Contribuer
description: Toutes les informations pour participer !
plan: ui/main-flatplan
---
## Ils changent le monde

On entend parfois certains <lexique>junistes</lexique> dire que la Ğ1 est "*une œuvre d'art collective*".

Et c’est vrai qu’il y a beaucoup de gens qui font des choses autour des monnaies libres en général ou de la Ğ1 en particulier. Une formule mathématique et un développement informatique qui touche et peut modifier la vie des gens, construire le réel. C’est toute une symphonie qui se met en place, sans que personne sache trop comment. 

C'est en cela une véritable performance artistique collective.

Si la Ğ1 existe aujourd’hui, c’est grâce à tous ces gens :

Ceux qui développent les logiciels : vous trouverez leurs noms sur le site de [Duniter](https://duniter.fr/contributeurs/), le moteur de la blockchain.

Ceux qui valident les transactions de la blockchain en [forgeant des blocs](https://duniter.org/fr/miner-des-blocs/).

Ceux qui traduisent les logiciels.

Ceux qui rédigent de la documentation.

Ceux qui rédigent des rapports de bug.

Ceux qui développent l’économie : ce sont tous ceux qui, [par leurs échanges](https://www.gchange.fr/#/app/market/lg?last), valorisent la Ğ1, et prouvent au quotidien qu’une monnaie libre permet d’échanger.

Ceux qui en parlent autour d’eux.

Ceux qui créent des groupes locaux.

Ceux qui organisent des parties de [Ğeconomicus](http://geconomicus.glibre.org/).

Ceux qui créent des mèmes rigolos.

Ceux qui donnent des conférences pour expliquer la création monétaire.

Ceux qui créent du contenu pédagogique en ligne.

Ceux qui écrivent des livres.

Ceux qui font de la création graphique.

Ceux qui mettent à disposition des infrastructures permettant d’héberger des contenus.

Ceux qui soutiennent les développeurs.

Et tous ceux que cette liste a oublié de lister.

## À vous de jouer !

La Ğ1 n’appartient à personne.

D’ailleurs, si on vous a dit que ce site était le “*site officiel*” des monnaies libres ou de la Ğ1, alors mettons ça au clair toute de suite : ni la monnaie libre ni la Ğ1 ne peuvent avoir de “*site officiel*“, car il n’y a pas d’autorité qui les gouverne.

Certes, ce site est souvent celui qui sort en premier dans les moteurs de recherche lorsque vous tapez “*monnaie libre*“, c'est pourquoi nous nous sommes attachés à vous fournir des informations justes, avec un minimum d'interprétation subjective.
D’autres sites parlent des monnaies libres ou de la Ğ1 et ils ne sont pas moins légitimes que celui-ci pour le faire.

Comme pour “gilet jaune”, certaines personnes peuvent essayer de déposer la marque “monnaie libre”, mais ils se feront probablement retoquer, car “monnaie libre” n’est que l’association de deux noms communs.

La Ğ1 n’a pas de tête.

La Ğ1 n’a pas de centre.

Vous êtes donc libres de contribuer comme bon vous semble, sans attendre que qui que ce soit vous en donne l’autorisation.

Si vous souhaitez collaborer avec d’autres gens sur certains projets, vous pouvez vous manifester sur n’importe quelle plateforme ou réseau sur lequel se trouvent des junistes : ils sauront en général assez bien vous mettre en relation avec d’autres junistes pour que vous puissiez joindre vos efforts 😉



## Comment aider la monnaie libre ?

### En parler autour de vous.

Parlez de la monnaie libre Ğ1 à vos amis, votre famille, vos connaissances. Demandez-leur ce qu'ils en pensent. Vous pouvez même leur demander de vous aider à comprendre, à voir s'il n'y aurait pas un piège. 

### Organiser des rencontres.

Vous pouvez proposer des rencontres, même si vous n'avez pas tout compris, le plus important est d'en discuter et faire connaissance avec d'autres potentiels utilisateurs.   
Vous pouvez commencer par des rencontres en petits comités chez vous ou dans un lieu public.    
I﻿l suffit de choisir une date et un lieu et de le proposer sur le forum des utilisateurs, en suivant ce tuto :  [comment annoncer un évènement](https://forum.monnaie-libre.fr/t/comment-annoncer-un-evenement/21278/1)
Vous pouvez choisir de donner votre adresse exacte uniquement à ceux qui s'inscrive via message direct. 

### Aider aux traductions

Vous pouvez aider à traduire les logiciels dans différentes langues, et même en français quand le logiciel a été écrit dans une langue étrangère.  
Notamment avec [weblate](https://weblate.duniter.org/projects/)

### Participer au développement des logiciels.

S﻿i vous avez des compétences en informatique, présentez-vous sur le [forum des développeurs](https://forum.duniter.org/)  
Toutes les compétences sont bienvenues, on vous expliquera comment contribuer en fonction de vos compétences et des besoins du moment. 




## Faire des dons.

### Des dons en DUğ1.
Vous pouvez donner des DUğ1 aux contributeurs techniques et aux <lexique title="forgeron">forgeurs</lexique> en copiant collant les clés suivantes :  
Contributeurs tech. Duniter/Ğ1 : 78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8      
Remuniter, pour les forgerons : TENGx7WtzFsTXwnbrPEvb6odX2WnqYcnnrjiiLvp1mS

Il existe tout un tas de cagnottes de cotisation pour divers projets, vous en trouverez une [liste non exhaustive sur le forum](https://forum.monnaie-libre.fr/t/liste-des-cagnottes-de-cotisations-de-la-g1/13828).  
Attention, avant de faire un don, assurez-vous de qui gère vraiment la cagnotte.

### Donner d'autres monnaies
Il existe aussi des associations qui aident au développement de la Ğ1, et acceptent les dons et cotisations dans d'autres monnaies :    
[axiom-team](https://www.helloasso.com/associations/axiom-team)    
[axiom-team](https://axiom-team.fr)  
[éconolibre](https://www.helloasso.com/associations/econolibre/formulaires/1/widget)  
Toute aide même minime est importante.


## Soutenir financièrement la Ğ1v2
La Ğ1 évolue, pour avoir des logiciels plus fluides, plus pratiques, plus résilients, aidez financièrement en euros les développeurs de la Ğ1 du futur.   
Vous pouvez aider via la campagne de [financement participatif](https://www.helloasso.com/associations/axiom-team/collectes/finalisation-de-cesium-v2-et-duniter-v2 ). Même une toute petite somme montrera votre soutien.  
Vous pouvez consulter les résultats de [la première campagne](https://duniter.fr/blog/financement-participatif/)  


