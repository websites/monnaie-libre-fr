---
title: DUNITER V2 auf deutsch
description: Eine Version 2 für die Software der Ğ1
---
<div style = "display:inline-block;float:left;">
<img src="/uploads/demenagement-v1-v2.jpg" width=400px align="center" style="margin:auto;"/>
</div>
<div style = "display:inline-block;float:left; padding-left : 20px;">
<p style="color:#34A1FF;" id="langues">Andere Sprachen &nbsp; &nbsp;&nbsp;</p>

[En français](/maj-v2/)  

[En español](/duniter-v2-en-espanol/) 

[In-english](/duniter-v2-in-english/)

[In-italiano](/duniter-v2-in-italiano/)

[Em português](/duniter-v2-em-portugues/)

</div>

<p style="float:left;"><strong> Maschinelle Übersetzung mit [deepl](https://www.deepl.com/fr/translator)</strong> </p>

<div style = "display :inline-block; margin:5px auto 5px; text-align:center; width:400px; border:solid #3FB2FF 1px; background-color:#3FB2FF;border-radius: 10px; padding: 1em;">
<a style="font-size:1.5em; font-weight: bold;" href="https://www.helloasso.com/associations/axiom-team/collectes/finalisation-de-cesium-v2-et-duniter-v2">Partizipative Finanzierung<img src="/uploads/crowfunding100.png" style="padding:5px;margin:auto;"/></a>
</div>

<h2 style="color:#34A1FF;" id="plan">Kommunikationsplan</h2>

Wann werden Sie über den Fortschritt des Projekts informiert? 

1. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Oktober :**</strong> [warum ändern?](#pourquoi)</p>
2. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">November – Dezember :** </strong> [was sich ändern wird](#evolutions) </p>
3. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Januar :**</strong> [was sich nicht ändern wird](#identique) <br/> [und **wie es weitergehen wird**.](#bascule) </p>
4. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Februar :**</strong> die Satellitensoftware : **entdecken, testen und nutzen** </p> 

<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="300" align ="center" >

<h2 style="color:#34A1FF;" id="pourquoi">Warum ändern?</h2>

<h3 style ="color: #F7A212" id="inconvenients">1/ Nachteile von V1</h3>

#### Die ersten Nachteile sind vor allem für Entwickler sichtbar:

* Der Hauptgrund für die Migration: **Schwierigkeiten bei der Pflege von <lexique>Duniter</lexique> V1 (Fehler beheben und Verbesserungen einführen).**
* Sicherheitsprobleme,

  * Duniter kann leicht blockiert werden (Angriff durch Netzwerksättigung).
  * Alle Mitgliedskonten sind potenziell Schmiede[^1], doch einige sind leicht zu hacken, wodurch ein Angreifer potenziell die gesamte Blockchain kompromittieren könnte.
* Zwang für die <lexique title=„forgeron“>Schmiede</lexique>, manuell Resynchronisierungen vorzunehmen, damit es weiterhin funktioniert.
* Gefahr einer Abspaltung*, wenn die <lexique title=„forgeron“>Schmiede</lexique> <lexique>Duniter</lexique> im Falle einer Fehlerbehebung nicht schnell aktualisieren.

[^1]: Ein Schmied ist ein Mitglied des TDC (Web of Trust), das die Duniter-Software auf sein Gerät (Computer oder ein anderes Gerät) heruntergeladen hat, das zum Server (oder Knoten) für den Betrieb des June wird.

<alert type="info">

<details> 
  <summary> *Klicken Sie hier, um zu erfahren: Was ist ein Fork</summary>?

 Eine Fork ist eine Gabelung, wenn zwei Knoten gleichzeitig denselben Block berechnen, was zu zwei Versionen desselben Blocks und damit zu zwei Versionen der Blockchain führt, die weiterverfolgt werden sollen.
 Ein Fork kann sich selbst auflösen, ohne dass Daten verloren gehen (das ist der normale Betrieb, wenn die Knoten auf dem neuesten Stand sind), 
 Er kann sich mit Datenverlust auflösen, ein Fall, der in V1 recht häufig vorkommt (wenn es Probleme mit der Synchronisation oder der Aktualisierung der Knoten gibt).<.
 Er kann sich nicht automatisch auflösen, was zu einer Spaltung der Blockchain führt (ein Teil der Nutzer sieht nicht das Gleiche wie die anderen).<.
 Wenn der Fork nicht aufgelöst wird (automatisch oder manuell), kann dies zu einer Spaltung der Gemeinschaft führen.
 **Weitere Details finden Sie unter diesen Links:**\
https://journalducoin.com/lexique/fork/\
https://www.coinbase.com/fr/learn/crypto-basics/what-is-a-fork\
https://fr.cryptonews.com/exclusives/c-est-quoi-un-fork-5803.htm

</details>
</alert>

#### Alle Nutzer haben bereits andere Nachteile festgestellt:

<img style="float:right; width:15%;" src="../uploads/escargot-inconvenients.png"/>

* Langsame Ausführung.

  * Lange Zeit, um Transaktionen anzuzeigen.
  * Zwischen 5 und 10 min, um eine Transaktion zu verarbeiten, 30 min, um sie zu bestätigen.
* Probleme bei der Synchronisierung der Pools[^1]: Unterschiedliche Sichtweisen von Knoten zu Knoten, da kein Knoten eine globale Sicht auf die Pools hat.
* Transaktionen oder Zertifizierungen, die nicht durchlaufen.
* Zertifizierungen, die verschwinden.
* Schwierigkeiten, neue hinzuzufügen (Synchronisationsprobleme zwischen Zertifizierern, um 5 Zertifizierer gleichzeitig verfügbar zu haben)[^2].    

[^1]: Pool: Wenn ein Mitglied mehrere Zertifizierungen in weniger als 5 Tagen ausstellt, bleiben sie in der Schwebe und werden nur alle 5 Tage einzeln in die Blockchain eingetragen (wie in den TDC-Regeln vorgesehen, aber ohne dass die Reihenfolge bekannt ist, in der sie durchlaufen werden).\
[^2]:  Da die Zertifizierungen von Mitgliedern vor denen von Antragstellern kommen, muss ein zukünftiges Mitglied warten, bis alle anderen zertifiziert sind, bevor es zertifiziert werden kann.

<h3 style ="color: #F7A212" id="avantages">2/ Vorteile von v2</h3>
 <img style="float:right; width:18%;" src="../uploads/papillon-avantages.png"/>

* Erleichterte Wartung und Updates für Entwickler.
* Automatische Updates ohne Fork für Schmiede. 
  *(Die Möglichkeit, ein Update abzulehnen, wird immer noch existieren, aber es wird freiwillig sein und nicht mehr aus Vergesslichkeit)*.
* Ein Block alle 6 Sekunden

  * Bestätigte Transaktion in 30 Sekunden
  * Stark verbesserte Antwortzeit
* Schnellere und zuverlässigere Synchronisierung der Knoten.
* Zertifizierungen werden sofort validiert (keine Pools).
* Einfachere Eingabe von Neuen (keine Synchronisierung zwischen den ersten 5 Zertifizierern mehr erforderlich)

<h3 style ="color: #F7A212" id="evolution">3/ Entwicklung der Software, nicht der Währung</h3>

Die Software wird ständig weiterentwickelt. Derzeit befinden wir uns bei Duniter in der Version 1.8.7 und bei Cesium in der Version 1.7.13. 
Diese Entwicklungen wurden so vorgenommen, dass sie mit den vorherigen Versionen kompatibel sind, und wurden immer ohne Probleme weitergegeben. 

Die Version 2 ist immer noch freie Software, es ist eine neue Blockchain, die mit allen Daten der Blockchain Ğ1 V1 zum Zeitpunkt der Umstellung starten wird (Konten, Transaktionen, Zertifizierungen...).\
**Sie werden alle Ihre Ğ1, Zertifizierungen und Transaktionen gut wiederfinden.**

<alert type=„info“>.
*Für die Cesium+ Daten (Profile, Nachrichten, Benachrichtigungen...) ist das noch in Arbeit, und vielleicht nicht gleich beim Start!*
</alert>.

Eine neue Blockchain setzt neue Software voraus (Cäsium 2, Gecko, Tikka, G1nkgo 2, ....).

Es wird nicht möglich sein, zwischen zwei verschiedenen Blockchains zu handeln, daher muss man nach dem Umstellungsdatum die neue Version der Software installiert haben, um weiterhin ğ1 mit dem Rest der Gemeinschaft auszutauschen.

**Um diese Umstellung nicht zu verpassen, bleiben Sie auf dieser Seite, im Forum und in den „sozialen Netzwerken“ auf dem Laufenden!**

<h2 id="flyers1"><a href="https://forum.monnaie-libre.fr/uploads/short-url/35uaBZjICd9ufyAhwvh9bvAKnKc.pdf" target="_blank" width="50%" style="color:#34A1FF; font-weight: bold;">Beidseitiger Flyer zum Weitergeben <img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>
<p><strong>Zum Vergrößern auf das Bild klicken</strong></p>

<div style="float:left;  margin:0 1em 0;"><a href="/uploads/warum-auf-deutsch-wechseln-rolf_page_1.jpg" target="_blank">
<img src="/uploads/warum-auf-deutsch-wechseln-rolf_page_1.jpg" width="200"/></a><br/> Übersetzt von @Rolf.</div>

<div style="float:left;  margin:0 1em 0;"><a href="/uploads/warum-auf-deutsch-wechseln-rolf_page_2.jpg" target="_blank">
<img src="/uploads/warum-auf-deutsch-wechseln-rolf_page_2.jpg" width="200"/></a><br/> Übersetzt von @Rolf.</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >

<h2 style="color:#34a1ff" id="evolutions"> Entwicklungen (was sich für die Nutzer ändern wird)</h2>

<h3 style="color:#f7a212" id="logiciels">1/ Neue Software</h3>

Um sich weiterhin mit anderen Junkies austauschen zu können, ist es unerlässlich, dass alle bei der ersten Verbindung nach dem Start der Blockchain V2 ihre Version wechseln. 

* **Cäsium** Die meistgenutzte Software wird ihre V2-Version anbieten. Es ist nicht geplant, auf Duniter V2 umzusteigen, bevor diese Software bereit ist. 
* **Gecko** Eher Smartphone-orientiert, für Zahlungen, ist bis auf einige Anpassungen bereit.
  \*\* **Tikka** auf dem Computer, buchhaltungsorientiert für Profis, ist in der Entwicklung. 
* **Ğ1nko** eine Geldbörse, die Transaktionen in G-Märkten erleichtert. Version 2 wird vielleicht schon für die Migration bereit sein.
* **Ğ1superbot** wird auch seine Version für Duniter V2 haben. 
* **Duniter-Connect**, eine Browser-Erweiterung, mit der man von jedem Standort aus Überweisungen tätigen kann, entwickelt sich im Zuge der Duniter-Entwicklungen weiter.
* **Ğcli** Der Kommandozeilen-Client (für Techniker) ähnlich wie Silkaj, entwickelt sich ebenfalls im Zuge der Duniter-Entwicklungen weiter.
* ...

- - -

<h3 style="color:#f7a212" id="cles">2/ Neue Form der Öffentlichen Schlüssel</h3>
<img style="float:right; width:15%;" src="../uploads/adresse-cle.png"/>

Mit der neuen Blockchain Duniter V2 ändert sich die Kodierung dessen, was wir öffentliche Schlüssel nennen. Ihr öffentlicher Schlüssel wird also nicht mehr so aussehen wie der, den Sie kennen. Wer seinen QR-Code ausgedruckt hat, wird ihn wahrscheinlich neu ausdrucken müssen.

Wir werden eher von einer Adresse als von einem Schlüssel sprechen, sie beginnen alle mit „g1...“, verwenden Sie also das Ende der Adresse statt des Anfangs, um Ihr Konto zu erkennen.

In Duniter v2 werden wir die Verwendung einer Adresse anstelle eines einfachen öffentlichen Schlüssels fördern. Die Adresse hilft, Fehler beim Kopieren und Verwenden eines Schlüssels im falschen Netzwerk zu vermeiden.

\*\*Um Sie zu begleiten, wird Cesium2 für Konten, die vor dem Update erstellt wurden, zur Erinnerung den alten „öffentlichen Schlüssel“ anzeigen. Dasselbe gilt für G1nkgo, das den alten Schlüssel anzeigt.

- - -

<h3 style="color:#f7a212" id="depot">3/ Existenzielles Depot</h3>

In der Blockchain v1 verschwinden Konten mit weniger als 1 Ğ1, ohne dass der Nutzer darüber informiert wird (Geldvernichtung).\
In Blockchain v2 wird es unmöglich sein, unter 1 Ğ1 zu fallen, ohne explizit die Schließung des Kontos zu verlangen.

- - -

<h3 style="color:#f7a212" id="piscines">4/ Verschwinden der Wartezeiten in Schwimmbädern</h3>  
<img style="float:right; width:18%;" src="../uploads/nageur.png"/>

* Zertifizierungen werden sofort berücksichtigt, unabhängig davon, ob der Zertifizierte noch auf andere Zertifizierungen wartet oder nicht.
* Die 5-Tages-Frist zwischen zwei Zertifizierungen muss zwingend eingehalten werden, da DuniterV2 nur eine Zertifizierung alle 5 Tage zulässt.
* Einige Kunden (wie Césium) bieten möglicherweise an, Ihre Zertifizierungsabsichten in ein „Adressbuch“ aufzunehmen (noch nicht entwickelt).

- - -

<h3 style="color:#f7a212" id="adhesions">5/ Beitrittsprozess (Mitglied werden Mitschöpfer der Währung)</h3>

* Neue Konten werden nur einfache Portfoliokonten sein. 
* Man muss einige Ğ1 besitzen, um den Zertifizierungsprozess zu beginnen. Es wird so unmöglich sein, ein Konto mit null ğ1 zu zertifizieren.
* Die erste Zertifizierung gilt als Einladung, Mitglied zu werden (kein Antrag mehr nötig).
* Die Annahme der Einladung besteht für den Neuen darin, innerhalb von 48 Stunden einen Nickname zu wählen (diese Frist kann sich noch ändern).
* Sobald der Nickname registriert wurde, sind die folgenden Zertifizierungen möglich.
* Wenn das Konto 5 Zertifizierungen besitzt, die die Abstandsregel einhalten, wird es zum Mitschöpfer der Währung.
* Es ist nicht mehr nötig, sich für Zertifizierungen zu synchronisieren (man kann nicht mehr zertifizieren, wenn man nicht verfügbar ist).
* Die zweimonatige Frist für die ersten fünf Zertifizierungen, die die Distanzregel erfüllen, wird weiterhin gültig sein. (Die Länge der Frist könnte sich ändern).
* Wenn die Fristen abgelaufen sind, erhält der Zertifizierer seine Zertifizierung aus seinem Bestand an auszustellenden Zertifizierungen zurück.

- - -

<h3 style="color:#f7a212" id="securite">6/ Neue Konten besser gesichert</h3>

Die Authentifizierung durch geheime Kennung und Passwort ist für eine Währung zu wenig sicher (heute verlangen Ihre Banken von Ihnen eine doppelte Authentifizierung).

<img style="float:right; width:18%;" src="../uploads/coffre.png"/>

1. Die alten Konten mit geheimer Benutzerkennung und Passwort werden immer mit Cäsium nutzbar bleiben, wie vor der V2-Umstellung.
2. **Neue Konten werden mit einer Mnemonik aus 12 Wörtern erstellt.** Diese 12 Wörter sollten sorgfältig vor Blicken geschützt aufbewahrt werden, sie ermöglichen es, Ihre Konten auf anderen Geräten oder mit anderer Portfolioverwaltungssoftware (Cesium, Gecko, Tikka, ...) wiederherzustellen.
3. Es können mehrere Konten mit der gleichen Mnemonik erstellt werden (dies wird als „Tresor“ bezeichnet). Ein Tresor kann mehrere Konten oder ein einzelnes Konto enthalten. 
4. Sobald Sie eine Dokumentenverwaltung eingerichtet haben, können Sie mit einem Code aus vier oder fünf Buchstaben oder Zahlen auf alle Konten in der Dokumentenverwaltung zugreifen, solange Sie das gleiche Gerät verwenden. 
5. Ein mit Mnemonik erstelltes Konto kann in Cesium, Gecko Tikka, Gcli und wahrscheinlich auch in anderen Programmen verwendet werden.   

- - -

<h3 style="color:#f7a212" id="migration">6a / Migration zu einem besser gesicherten Konto</h3>

**Nur für Junisten, die es wollen.**
<img style="float:right; width:18%;" src="../uploads/gecko-4244388_640.png"/>

1. Gecko soll aus Sicherheitsgründen nur mit Konten funktionieren, die mit einer Mnemonik erstellt wurden. Für andere Software steht noch nichts fest.
2. Gecko bietet an, Ihre alten Konten (id/mdp) auf ein neues Konto zu „migrieren“, das Sie zuvor mit einer Mnemonik erstellt haben.
3. Dies kann dazu führen, dass Sie alle Ihre Konten in einem einzigen Schließfach zusammenfassen, das einfacher zu bedienen ist.
4. Die Migration eines Mitgliedskontos ist die Übertragung Ihrer Identität mit Nickname, Zertifizierungen, Mitgliedsstatus oder nicht, und ğ1. 
5. Die Migration eines einfachen Wallet-Kontos, ist nichts anderes als die Übertragung aller ğ1.
6. Nach der Migration kann das neue Konto (mit Mnemonik) für alle Ihre Transaktionen verwendet werden, unabhängig von der Software, die die Portfolios verwaltet (Cäsium, Gecko, Tikka, Gcli), und das alte Konto wird nur noch ein leeres Portfolio sein.
7. Das alte Konto bleibt wie jede andere Brieftasche nutzbar. Damit würde aber der Nutzen der Migration verloren gehen.

- - -

<h3 style="color:#f7a212" id="frais">7/ Gebühren und Quoten</h3>

#### Die Kapazität einer Blockchain ist nicht unendlich.

Eine Blockchain benötigt Rechenleistung und Speicherplatz. Beide sind zwar groß, aber nicht unbegrenzt. Ein möglicher Angriff ist die Überlastung der Rechenleistung durch das Senden von Milliarden von Transaktionen pro Sekunde. 
Andere Blockchains erheben für jede Aktion eine Gebühr, um diese Überlastung der Rechenleistung und des Speicherplatzes zu verhindern.

#### Aber die Ğ1 ist nicht wie die anderen.

#### 1- Gebühren werden nur erhoben, wenn die Blockchain überlastet ist.

**Eine Gesamtzahl von Aktionen** (Transaktionen, Zertifizierungen, Mitgliedschaft, ...) **pro Block** wurde als **Grenze für einen „normalen “** Betrieb geschätzt. **Über diese Grenze hinaus** wird die Blockchain als **gesättigt** betrachtet. 

* Nur wenn die Anzahl der Aktien in einem Block **diese Grenze** überschreitet, wird eine **Gebühr** erhoben. Geschätzte Gebühren von etwa 0,015 DUĞ1 oder 17 Ğ1 für 100 Transaktionen.

#### 2- Zusätzlich werden die Gebühren an alle Mitglieder des Vertrauensnetzes *(das ist die Stärke unserer Blockchain)* zurückerstattet.

  <img style="float:right; width:18%;" src="../uploads/blockchain_humain.png"/>

* Eine Quote von Aktionen pro Mitglied und Block wird festgelegt (ermöglicht trotzdem viele Transaktionen).
* Nicht-Mitgliedskonten können mit einem Mitgliedskonto verknüpft werden und auch ihre Gebühren zurückerstattet bekommen, solange die Quote pro Mitglied nicht überschritten wird. 
* Ein Mitglied kann eine einzige Transaktion durchführen, aber wenn jemand zur gleichen Zeit 1 Million Transaktionen auf 1 Million Konten durchführt, wird die Blockchain gesättigt, und dieses Mitglied wird mit Gebühren belastet.
* Dann wird ihm das Geld zurückerstattet, weil er die Quote pro Mitglied nicht überschreitet.
* Wenn ein Mitglied und seine verknüpften Konten Hunderte von Transaktionen starten, die zur Sättigung führen, wird dieses Mitglied nur für seine ersten Transaktionen entschädigt, da die folgenden Transaktionen über die Quote hinausgehen.
* Wenn ein Mitglied Hunderte von Transaktionen startet, die sogar über die Quote pro Mitglied hinausgehen, aber zu diesem Zeitpunkt die einzige Person ist, die Transaktionen tätigt, kann dies passieren, ohne die Blockchain zu sättigen, so dass keine Gebühren anfallen.
* Nichtsdestotrotz gibt es keine Rückerstattung, wenn ein Konto geschlossen wird!
* Es wird keine Rückerstattung für anonyme Konten geben.

*Es ist unwahrscheinlich, dass ein solcher Angriff ausgelöst wird, da er den Angreifer in den Ruin treiben würde, für eine vorübergehende Sperrung. **Diese Gebühren dienen also der Abschreckung**.*.

Weitere Informationen über [das technische Forum](https://forum.duniter.org/t/les-frais-ca-devient-concret/11877/1).

- - -

<h3 style="color:#f7a212" id="forgerons">8/ Unterleuchtturm Schmiede</h3> 
    
Heute kann jedes Mitglied einen Knoten installieren und Blöcke schmieden, was zu einigen Update- und Synchronisationsfehlern sowie zu Sicherheitsproblemen führt, da einige Nutzer sich der Sicherheitslücken in ihrer Installation nicht bewusst sind und das Update „vergessen“.     

Mit Duniter V2 werden nur Mitglieder des Unternetzes Schmiede Blöcke schmieden können. 
Jeder kann weiterhin einen Spiegelknoten betreiben, der keine Blöcke schreibt, sondern auf Kundenanfragen reagiert.\
Da sich die Schmiedeknoten auf das Berechnen und Schreiben von Blöcken konzentrieren, werden sie nicht mehr auf Kundenanfragen reagieren. 
Alle Knoten kommunizieren nahezu sofort miteinander.

<img src="/uploads/blacksmith2medaille.png" width="15%" align ="right" >

Die **Zertifizierten Schmiede** müssen eine **Schmiedelizenz** einhalten, die **ein gutes Maß an Sicherheit** gewährleistet, u.a. müssen sie bereits seit einiger Zeit einen Spiegelknoten korrekt laufen lassen, ihren Server rund um die Uhr **offen halten** können und eine gute Internetverbindung haben.
Die Mitglieder dieses Subnetzes müssen sich nicht unbedingt kennen oder physisch sehen, da sie zunächst Mitglieder des Vertrauensnetzes der Mitschöpfer sein müssen. 

Um von Anfang an Teil dieses Schmiede-Subnetzes zu sein, muss man vor der Umstellung auf gdev oder gtest (die Währungen, mit denen die Version 2 vor dem Start getestet wird) einen Knoten laufen lassen.

Eine **Dokumentation**, die erklärt, wie man **einen Knoten** installiert und Schmied in V2 wird, wird derzeit erstellt und wird bekannt gegeben, sobald sie fertiggestellt ist.

Aktuelle **Schmiede** sind eingeladen, bei Interesse zu versuchen, Duniter V2 zu installieren, um zu sehen, wie es funktioniert, alle Installationsprobleme zu erkennen und bei der Erstellung der Dokumentation „Schmied werden“ zu helfen. 

Es ist/wird möglich sein, [Yunohost](https://yunohost.org/fr) oder [Docker-Images](https://www.ionos.fr/digitalguide/serveur/know-how/les-images-docker/) zu verwenden.

Weitere Informationen auf Französisch unter [das technische Forum](https://forum.duniter.org/t/video-installer-un-noeud-duniter-v2-en-moins-de-dix-minutes/11243/1).

- - -

<h3 style="color:#f7a212" id="fontionalites">9/ Zukünftige Funktionen</h3> 
Neue Funktionen werden möglicherweise nach dem Start von Duniter V2 implementiert, wenn Entwickler zur Verfügung stehen, die sich damit beschäftigen.

* Automatische Überweisungen tätigen
* Vollmachten für ein Konto delegieren
* Konten mit mehreren Unterschriften
* Identitätsgebundene Portfoliokonten
* Recht auf Vergessen *(Kommentar löschen?)*
* Möglichkeit, Abstimmungen einzurichten!
* Und mehr nach der Fantasie eines jeden ...

<details> 
  <summary> <b>Autor </b></summary>

[Maaltir](https://forum.monnaie-libre.fr/u/maaltir/summary) des MàJ-V2-Kollektivs,  bestätigt durch [hugotrentesaux](https://duniter.org/team/hugotrentesaux/), [bgallois](https://duniter.org/team/bgallois/), [Moul](https://duniter.org/team/moul/), [Tuxmain](https://duniter.org/team/tuxmain/)

</details>

<h2 id="flyers1"><a href="https://forum.monnaie-libre.fr/uploads/short-url/2f2FmBHo7nj3khh9UGuyp6BBI2l.pdf" target="_blank" style="color:#34A1FF; font-weight: bold;">Beidseitiger Flyer zum Weitergeben  <img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a> </h2>

<p><strong>Zum Vergrößern auf das Bild klicken</strong></p>

<div style="float:left; margin: 0 1em 0;"><a href="/uploads/de-weiche-anderungen-gibt-es-von-rolf_page_1.jpg" target="_blank">
<img src="/uploads/de-weiche-anderungen-gibt-es-von-rolf_page_1.jpg" width="200"/></a><br/> erstellt von @Spiranne </br> Übersetzt von Rolf</div>

<div style="float:left; margin: 0 1em 0;"><a href="/uploads/de-weiche-anderungen-gibt-es-von-rolf_page_2.jpg" target="_blank">
<img src="/uploads/de-weiche-anderungen-gibt-es-von-rolf_page_2.jpg" width="200"/></a><br/> erstellt von @Spiranne </br> Übersetzt von Rolf</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >

<h2 style="color:#34a1ff" id="identique"> Was sich nicht ändern wird</h2>

* Sie werden alle Ihre Ǧ1 auf der neuen Version wiederfinden.
* Bestätigte Zertifizierungen behalten in V2 das gleiche Enddatum. 
* Die Regeln für die Geldschöpfung bleiben gleich (1 DUğ1 pro Tag, der alle 6 Monate neu bewertet wird). 
  <img src="/uploads/rienechange.png" width="18%" align ="right" >
* Die Regeln für die Mitgliedschaft im Vertrauensnetz bleiben gleich: mindestens 5 Zertifizierungen, die die Abstandsregel einhalten.
* Ğ1 bleibt immer die erste Freie Währung und hält sich immer an die T.R.M.
* DuniterV2s, Cesium2, Gecko, Tika, G1nkgo, Ğ1superbot sind immer noch freie Software.
  **Die vielen Diskussionen und Fragen** rund um dieses V2-Update lassen **Dinge, die bereits in V1** vorhanden waren, deutlicher hervortreten; 
* **Die Dominanz der Entwickler**: Seit dem Start von Ğ1 sind sie es, die entscheiden, welche Updates gemacht werden. Nach dem Start von V2 können wir ausführlich über ein System für die Entscheidungsfindung diskutieren. 
* **Die Macht der Schmiede**, sie sind es, die Updates annehmen oder ablehnen. In V2 müssen die Schmiede nicht mehr entscheiden, ob sie ein Update annehmen oder nicht, die Updates werden automatisch durchgeführt, es sei denn, sie lehnen es ab (Prinzip der Freiheit).
* **Die Möglichkeit, in andere Währungen zu tauschen**: Es war schon immer jedem freigestellt, ğ1 gegen andere Währungen zu tauschen. Andere Währungen sind Gegenstände, denen jeder frei einen Wert zuweisen kann. [Sehen Sie hier einen Auszug aus der TRM](https://forum.monnaie-libre.fr/t/g1-et-plateformes-de-change/29900/61) 
* **Die Einführung von Ğ1 auf Börsenplattformen** ist in V2 ebenso wenig vorgesehen wie in V1. Aber es ist ohnehin unmöglich, irgendjemanden daran zu hindern, eine solche Möglichkeit zu schaffen (das ist die Welt der FREIEN). Wenn es nicht bereits geschehen ist, liegt es nur daran, dass niemand ein ausreichendes Interesse daran hatte.\
  Einige weitere Erklärungen auf Französisch im Forum : https://forum.monnaie-libre.fr/t/g1-et-plateformes-de-change/29900/78

<h2 style="color:#34a1ff" id="bascule">Wie wird die Umstellung für die Nutzer ablaufen?</h2>

<h3 style="color:#f7a212" id="date">A/ Umschaltdatum</h3>

Das Datum der Umstellung ist noch nicht festgelegt, es hängt vom Fortschritt der Entwicklungen und damit auch von deren Finanzierung ab. [Unterstützen Sie die Entwickler](https://www.helloasso.com/associations/axiom-team/collectes/finalisation-de-cesium-v2-et-duniter-v2). 

<img src="/uploads/code-geek.png" width="18%" align ="right" >

Im Moment zielen die Entwickler auf den 8. März, den Jahrestag von Ğ1. 

Diese Blockchain-Umstellung ist ein großes Projekt, das es den June-Nutzern leichter machen wird.

Das MàJ-V2-Kollektiv kommuniziert so viel wie möglich über diese Umstellung, damit sich alle Juneisten mit diesem Update ihrer Software wohlfühlen und darüber sprechen können. 

* im Forum: '[Wie wird es ablaufen: noch Fragen?](https://forum.monnaie-libre.fr/t/comment-cela-va-se-passer-encore-des-questions/31042) 
* auf Telegram: Telegram: Kontakt [@monnaielibrejune](http://t.me/monnaielibrejune/57035)

<h3 style="color:#f7a212" id="scenario">B/ Szenario der Umstellung von V1 auf V2</h3>

#### Das Ende von V1

**Das Umschaltszenario ist noch nicht formell festgelegt.**.

 Der Zeitpunkt **0** (Datum und Uhrzeit) für den Start von Duniter 2.0 ist noch nicht endgültig festgelegt.

* 5 oder 6 Wochen vor dem Zeitpunkt Null wird in Cesium eine Nachricht erscheinen, die das Datum der Umstellung anzeigt und darüber informiert, dass es ein (automatisches oder manuelles) Update der Client-Software geben wird. 
* Ein letztes Cäsium-Update wird verfügbar sein, zur Sicherheit vermeidet dieses alle Aktionen auf der V1, die verloren gehen könnten. 
* 30 Tage vor der eigentlichen Umstellung wird es eine Testumstellung geben. Wer möchte, kann die verfügbare Software mit dem Ğtest testen. 
* Etwa 1 Stunde vor dem Zeitpunkt **0** wird es eine Bildaufnahme der Ğ1 geben.  

<img src="/uploads/photo.jpg" width="18%" align ="right" >

* Es wird am besten sein, jede Aktion auf der Ğ1 einige Minuten vor der Aufnahme des Bildes zu vermeiden („Keine Bewegung!“, KLICK Foto). Eine letzte Kommunikationskampagne wird auf möglichst vielen Plattformen gestartet, um dazu zu ermutigen, die Nutzung der Ğ1 einzustellen, bis sie auf die v2 verschoben wird.
* Alle **Aktionen auf V1 nach diesem Foto** werden in der Blockchain V1 gespeichert, falls (trotzdem) Knoten weiterhin auf V1 laufen, aber **gehen für V2 verloren**.
* Die Blockchain V2 wird nach diesem Bild gestartet. 
* **Die Ǧ1 wird während dieser Neustartphase für einige Stunden nicht verfügbar sein.**

#### Nach dem Start von V2.

* Die Nutzer erhalten oder werden aufgefordert, **jede ihrer Installationen** (Computer, Tablet, Telefon, Firefox, Brave...) der Client-Anwendung (Cäsium 2.0) zu aktualisieren. Wenn alles gut läuft, wird die Aktualisierung automatisch durchgeführt. Normalerweise werden frühere Versionen von Cäsium nicht mehr funktionieren.
  **Wenn die Cäsium-Aktualisierung nicht sofort** erfolgt, spielt das keine Rolle. Es genügt, sie vor der nächsten Verbindung durchzuführen oder eine andere Client-Software zu installieren (Gecko, Tikka, G1nkgo, ...). 
* Sie sollten diese **Aktualisierung vorzugsweise vor dem Besuch eines ğ-Marktes** überprüfen, um eine gute Internetverbindung zu Hause zu nutzen. 
* Nach dem Update oder der Installation einer V2-Client-Software **finden die Nutzer ihre Konten**, Transaktionen und Zertifizierungen wieder, mit Ausnahme dessen, was noch darauf wartete, in die Blockchain (Pool) aufgenommen zu werden.

<img src="/uploads/lav2.png" width="18%" align ="right" >

* **Das DU wird weiterhin** selbstständig auf den Mitgliederkonten erstellt.
  *Nicht bestätigte Mitgliedsanträge und ausstehende Zertifizierungen (im Pool) werden verschwunden sein. **Sie müssen nur noch neu beantragt werden**.
* Transaktionen, die zum Zeitpunkt der Aufnahme des Bildes getätigt wurden (für diejenigen, die die Anweisungen nicht befolgt haben), **könnten** nicht genug Zeit gehabt haben, um validiert zu werden (man hatte Ihnen gesagt, dass Sie sich nicht bewegen sollen, Sie sind verschwommen), sie müssen ebenfalls erneut durchgeführt werden. 
  *Die Nutzer können dank der neuen Client-Anwendungen ihre Aktivitäten wie zuvor viel flüssiger wieder aufnehmen.

Details auf Französisch im [technischen Forum](https://forum.duniter.org/t/chronologie-de-la-migration/12605)

<details> 
  <summary> <b>Autor </b></summary>

[Maaltir](https://forum.monnaie-libre.fr/u/maaltir/summary) vom Kollektiv MàJ-V2, validiert von den Devs [hugotrentesaux](https://duniter.org/team/hugotrentesaux/), [bgallois](https://duniter.org/team/bgallois/), [Moul](https://duniter.org/team/moul/), [Tuxmain](https://duniter.org/team/tuxmain/), [Vit](https://duniter.org/team/vit/)

</details>

<h2 id="flyers1"><a href="https://forum.monnaie-libre.fr/uploads/short-url/de9qH1mw6eIX0fyucT2KiaTWlAN.pdf" target="_blank" style="color:#34A1FF; font-weight: bold;">Beidseitiger Flyer zum Weitergeben <img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>
<p> Warten auf Übersetzung</p>

<p><strong>Zum Vergrößern auf das Bild klicken</strong></p>

<div style="float:left; margin: 0 1em 0;"><a href="/uploads/de-was-wird-bleiden-und-wie-rolf_page_1.jpg" target="_blank">
<img src="/uploads/de-was-wird-bleiden-und-wie-rolf_page_1.jpg" width="200"/></a><br/> erstellt von @Spiranne </br> Übersetzt von Rolf</div>

<div style="float:left; margin: 0 1em 0;"><a href="/uploads/de-was-wird-bleiden-und-wie-rolf_page_2.jpg" target="_blank">
<img src="/uploads/de-was-wird-bleiden-und-wie-rolf_page_2.jpg" width="200"/></a><br/> erstellt von @Spiranne </br> Übersetzt von Rolf</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >