---
title: MàJ V2
description: Une version 2 pour les logiciels de la Ğ1.
---
<div style = "display:inline-block;float:left;">
<img src="/uploads/demenagement-v1-v2.jpg" width=400px align="center" style="margin:auto;"/>
</div>
<div style = "display:inline-block;float:left; padding-left : 20px;">
<p style="color:#34A1FF;" id="langues">Autres langues &nbsp; &nbsp;&nbsp;</p>

[En español](/duniter-v2-en-espanol/) 

[In-english](/duniter-v2-in-english/)

[In-italiano](/duniter-v2-in-italiano/)

[Em português](/duniter-v2-em-portugues/)

[Auf-deutsch](/duniter-v2-auf-deutsch/)

</div>

<div style = "display :inline-block; margin:5px auto 5px; text-align:center; width:400px; border:solid #3FB2FF 1px; background-color:#3FB2FF;border-radius: 10px; padding: 1em;">
<a style="font-size:1.5em; font-weight: bold; color: white;" href="https://www.helloasso.com/associations/axiom-team/collectes/finalisation-de-cesium-v2-et-duniter-v2">Financement participatif<img src="/uploads/crowfunding100.png" style="padding:5px;margin:auto;"/></a>
</div>

<h2 style="color:#34A1FF;" id="plan">Plan de Communication</h2>

Quand serez-vous mis au courant de l'avancée du projet ? 

1. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Octobre :**</strong> [pourquoi changer ?](#pourquoi)</p>
2. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Novembre – décembre :** </strong> [ce qui va changer](#evolutions) </p>
3. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Janvier :**</strong> [ce qui ne changera pas](#identique) <br/> et **[comment ça va se passer](#bascule)** </p>
4. **<p style="font-size:1.1em;"><strong style="color :#F7A212 ;">Février :**</strong> les logiciels satellites : **découvrir, tester et utiliser** </p> 

</div>

<hr style=" border: 2px solid #34A1FF;margin:50px auto;"/>

<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="300" align ="center" >

<h2 style="color:#34A1FF;" id="pourquoi">Pourquoi changer ?</h2>

<h3 style ="color: #F7A212" id="inconvenients">1/ Inconvénients de la V1</h3>

#### Les premiers inconvénients sont surtout visibles par les développeurs :

* La raison principale motivant cette migration : **Difficultés à maintenir <lexique>Duniter</lexique> V1 (corriger les bugs et apporter des améliorations).**
* Problèmes de sécurité,

  * Duniter peut facilement être bloqué (attaque par saturation du réseau).
  * Tous les comptes membres sont potentiellement forgerons[^1], or certains sont facilement piratables, ce qui permettrait à un attaquant de potentiellement compromettre toute la blockchain
* Obligation pour les <lexique title="forgeron">forgerons</lexique> de faire des resynchronisations manuellement pour que ça continue de fonctionner.
* Risque de fork* si les <lexique title="forgeron">forgerons</lexique> ne mettent pas à jour <lexique>Duniter</lexique> rapidement dans l'hypothèse d'une correction de bug.

[^1]: un forgeron est un membre de la TDC (Toile De Confiance) qui a téléchargé le logiciel Duniter sur son appareil (ordinateur ou autre), lequel devient un serveur (ou nœud) pour faire fonctionner la June.

<alert type="info">

<details> 
  <summary> *Cliquez ici pour savoir : Qu'est-ce qu'un fork</summary>

 Un fork, c'est une fourche quand deux nœuds calculent en même temps un même bloc, entrainant 2 versions du même bloc, donc 2 versions de la blockchain à suivre.\
 Un fork peut se résoudre tout seul, sans perte de données (c'est le fonctionnement normal quand les nœuds sont bien à jour), 
 Il peut se résoudre avec perte de données, cas que l'on constate assez souvent en V1 (Quand il y a des problèmes de synchronisation ou de mise à jour des nœuds).\
 Il peut ne pas se résoudre automatiquement, ce qui entraine une division de la chaine de blocs (une partie des utilisateurs ne voient pas la même chose que les autres).\
 Si le fork n'est pas résolu (automatiquement ou manuellement) cela peut aboutir à une division de la communauté\
 **Plus de détails en suivant ces liens :**\
https://journalducoin.com/lexique/fork/\
https://www.coinbase.com/fr/learn/crypto-basics/what-is-a-fork\
https://fr.cryptonews.com/exclusives/c-est-quoi-un-fork-5803.htm

</details>
</alert>

#### Tous les utilisateurs ont déjà constaté d'autres inconvénients :

<img style="float:right; width:15%;" src="../uploads/escargot-inconvenients.png"/>

* Lenteur d'exécution.

  * Temps long pour afficher les opérations
  * Entre 5 et 10 min pour qu'une transaction soit traitée, 30 minutes pour qu'elle soit validée
* Problèmes de synchronisation des piscines[^1] : Visions différentes d'un nœud à un autre, car aucun nœud n'a de vision globale des piscines.
* Transactions ou certifications qui ne passent pas.
* Certifications qui disparaissent.
* Difficulté pour faire entrer des nouveaux (problèmes de synchronisation entre certificateurs pour avoir 5 certificateurs disponibles en même temps)[^2].\
    [^1]: piscine : si un membre émet plusieurs certifications en moins de 5 jours, elles resteront en suspend et ne seront enregistrées dans la blockchain qu’une par une tous les 5 jours (comme prévu par les règles de la TDC mais sans que l’on sache dans quel ordre elles passeront).\
    [^2]:Sachant que les certifications de membres passent avant celles des demandeurs, un futur membre devra donc attendre que les autres soient passés avant d’être certifié.

<h3 style ="color: #F7A212" id="avantages">2/ Avantages de la v2</h3>
<img style="float:right; width:18%;" src="../uploads/papillon-avantages.png"/>

* Maintenance et mises à jour facilitées pour les développeurs
* Mise à jour automatique sans fork pour les forgerons 
  *(la possibilité de refuser une mise à jour existera toujours, mais ce sera volontaire et non plus par oubli)*.
* Un bloc toutes les 6 secondes

  * Transaction validée en 30 secondes
  * Temps de réponse largement amélioré
* Synchronisation des nœuds plus rapide et fiable
* Certifications validées immédiatement (pas de piscines)
* Entrée plus facile des nouveaux (plus besoin de synchronisation entre les 5 premiers certificateurs)

<h3 style ="color: #F7A212" id="evolution">3/ Évolution des logiciels, pas de la monnaie</h3>

Les logiciels évoluent en permanence, à ce jour, nous sommes sur la version 1.8.7 de Duniter, et 1.7.13 de Cesium. 
Ces évolutions ont été faites de manière à rester compatibles avec les versions précédentes, et sont toujours passées sans problème. 

La version 2 ce sont toujours des logiciels libres, c'est une nouvelle blockchain qui démarrera avec toutes les données de la blockchain Ğ1 V1 au moment de la bascule (comptes, transactions, certifications…)\
**Vous retrouverez bien toutes vos Ğ1, vos certifications et transactions.**

<alert type="info">

*Pour les données Cesium+ (profils, messagerie, notifications…) c'est en cours, et peut-être pas dès le démarrage !*

</alert>

Une nouvelle blockchain implique de nouveaux logiciels (Césium 2, Gecko, Tikka, G1nkgo 2, ....)

Il ne sera pas possible d'échanger entre deux blockchains différentes, donc après la date de bascule, il faudra avoir installé la nouvelle version des logiciels pour continuer d'échanger des ğ1 avec le reste de la communauté.

**Pour ne pas rater cette bascule, restez à l’écoute des informations sur ce site, le forum, et les « réseaux sociaux » !**

<h2 id="flyer1"><a href="https://forum.monnaie-libre.fr/uploads/short-url/mNByObNAkw4Vt38hkW3mQGD3okd.pdf" target="_blank" width="50%" style="color:#34A1FF; font-weight: bold;">Flyer recto-verso à partager
<img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>

<p><strong>Cliquer sur l'image pour agrandir</strong></p>

<div style="float:left; margin:0 1em 0;"><a href="/uploads/pourquoi-changer-de-version-duniter_page_1.jpg" target="_blank">
<img src="/uploads/pourquoi-changer-de-version-duniter_page_1.jpg" width="200"/></a><br/> réalisé par @Spiranne></div>

<div style="float:left;  margin:0 1em 0;"><a href="/uploads/pourquoi-changer-de-version-duniter_page_2.jpg" target="_blank">
<img src="/uploads/pourquoi-changer-de-version-duniter_page_2.jpg" width="200"/></a><br/> réalisé par @Spiranne</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >

<h2 style="color:#34a1ff" id="evolutions"> Les évolutions (ce qui va changer pour les utilisateurs)</h2>

<h3 style="color:#f7a212" id="logiciels">1/ Nouveaux Logiciels</h3>

Pour pouvoir continuer à échanger avec les autres junistes, il est indispensable que tous changent de version dès leur première connexion après le démarrage de la blockchain V2. 

* **Césium** Le plus utilisé des logiciels offrira sa version V2. Il n'est pas prévu de basculer sur Duniter V2 avant que ce logiciel soit prêt. 
* **Gecko** plutôt orienté smartphone, pour les paiements, est prêt, à quelques ajustements près.
* **Tikka** sur ordinateur, orienté comptabilité pour les pros, en cours de développement. 
* **Ğ1nko** un porte-monnaie facilitant les transactions dans les G-marchés. La version 2 sera peut-être prête pour la migration
* **Ğ1superbot** aura aussi sa version pour Duniter V2. 
* **Duniter-Connect**, extension navigateur permettant de réaliser des virements depuis n'importe quel site, évolue au fil des développements de Duniter.
* **Ğcli** Le client en ligne de commande (pour les techniciens) similaire à Silkaj, évolue lui aussi au fil des développements de Duniter
* ...

- - -

<h3 style="color:#f7a212" id="cles">2/ Nouvelle forme des Clés Publiques</h3>
<img style="float:right; width:15%;" src="../uploads/adresse-cle.png"/>

Avec la nouvelle blockchain Duniter V2 ce que nous appelons les clés publiques changent de codage. Donc votre clé publique ne ressemblera plus à celle que vous connaissez. Il faudra sûrement réimprimer votre QR-code pour ceux qui l'on imprimé.

On parlera plutôt d'adresse que de clé, elles commenceront toutes par "g1...", donc utilisez la fin de l'adresse plutôt que le début, pour reconnaitre votre compte.

Dans Duniter v2, nous encouragerons l’utilisation d’une adresse plutôt que d’une clé publique simple. L’adresse permet d’éviter les erreurs de copie et d’utilisation d’une clé sur le mauvais réseau.

**Pour vous accompagner, pour les comptes créés avant la mise à jour, Cesium2 affichera pour rappel l’ancienne "clef publique". Idem pour G1nkgo qui affichera cette ancienne clef.**

- - -

<h3 style="color:#f7a212" id="depot">3/ Dépôt existentiel</h3>

Dans la blockchain v1 les comptes ayant moins de 1 Ğ1 disparaissent sans que l'utilisateur en soit averti (destruction de monnaie).\
En blockchain v2 il sera impossible de passer sous la barre de 1 Ğ1 sans demander explicitement la fermeture du compte.

- - -

<h3 style="color:#f7a212" id="piscines">4/ Disparition des délais d'attente en  piscines</h3> 
 <img style="float:right; width:18%;" src="../uploads/nageur.png"/>

* Les certifications seront prises en compte immédiatement, que le certifié soit en attente d'autres certifications ou pas.
* Le délai de 5 jours entre deux certifications sera obligatoirement respecté car DuniterV2 ne permet qu’une certification tous les 5 jours.
* Certains clients (comme Césium) proposeront peut-être d'ajouter vos intentions de certifications dans un "carnet d'adresse" (pas encore développé).

- - -

<h3 style="color:#f7a212" id="adhesions">5/ Processus d'adhésion (devenir membre cocréateur de monnaie)</h3>

* Les nouveaux comptes ne seront que des comptes simples portefeuilles. 
* Il faut posséder quelques Ğ1 pour entamer le processus de certification. Il sera donc impossible de certifier un compte ayant zéro ğ1
* La première certification vaut invitation à devenir membre (plus besoin d'en faire la demande).
* L'acceptation de l'invitation consiste, pour le nouveau, à choisir un pseudo sous 48h (délai qui peut encore changer).
* Une fois que le pseudo a été enregistré, les certifications suivantes sont possibles.
* Quand le compte détient 5 certifications respectant la règle de distance il devient cocréateur de monnaie.
* Plus besoin de se synchroniser pour les certifications (on ne pourra plus certifier si on n'est pas disponible).
* Le délai de deux mois pour obtenir les cinq premières certifications respectant la règle de distance sera toujours valable. (La durée du délai pourrait changer)
* Si les délais sont dépassés, le certificateur récupère sa certification dans son stock de certifications à émettre.

- - -

<h3 style="color:#f7a212" id="securite">6/ Nouveaux comptes mieux sécurisée</h3>

L'authentification par identifiant secret et mot de passe est trop faiblement sécurisant pour une monnaie. (Aujourd'hui vos banques vous imposent une double authentification)
<img style="float:right; width:18%;" src="../uploads/coffre.png"/>

1. Les anciens comptes, avec identifiant secret et mot de passe, resteront toujours utilisables avec Césium, comme avant la bascule V2.
2. **Les nouveaux comptes seront créés à partir d'un mnémonique de 12 mots.** Ces 12 mots seront à conserver précieusement à l'abri des regards, ils permettent de récupérer vos comptes sur d'autres appareils ou d'autres logiciels gestionnaires de portefeuilles (Cesium, Gecko, Tikka, ...)
3. Plusieurs comptes pourront être créés à partir du même mnémonique (cela constituera ce qu'on appelle un "coffre"). Un coffre peut contenir plusieurs comptes ou un seul. 
4. Une fois le coffre créé, un code de 4 ou 5 lettres ou chiffres suffira pour accéder à tous les comptes du coffre tant que vous utilisez le même appareil. 
5. Un compte créé avec mnémonique sera utilisable sur Cesium, Gecko Tikka, Gcli et surement avec les autres logiciels.   

- - -

<h3 style="color:#f7a212" id="migration">6 bis / Migration vers un compte mieux sécurisé</h3>

**Uniquement pour les junistes qui le veulent.**
<img style="float:right; width:18%;" src="../uploads/gecko-4244388_640.png"/>

1. Gecko est conçu pour ne fonctionner qu'avec des comptes créés par mnémonique, pour raison de sécurité. Pour les autres logiciels rien de définitif à ce jour.
2. Gecko propose de “migrer” vos anciens compte (id/mdp) vers un nouveau compte que vous aurez préalablement créé avec un mnémonique.
3. Cela peut vous permettre de regrouper tous vos comptes dans un seul coffre, plus simple d'utilisation.
4. La migration d'un compte membre c'est le transfert de votre identité avec pseudo, certifications, statut de membre ou pas, et ğ1. 
5. La migration d'un compte simple portefeuille, se résume à un transfert de toutes les ğ1.
6. Une fois migré, le nouveau compte (avec mnémonique) sera utilisable pour toutes vos transactions quel que soit le logiciel gestionnaire de portefeuilles (Césium, Gecko, Tikka, Gcli) et l’ancien compte ne sera plus qu'un portefeuille vide.
7. L'ancien compte reste utilisable comme n'importe quel portefeuille. Mais ce serait perdre l'intérêt de l'avoir migré.

- - -

<h3 style="color:#f7a212" id="frais">7/ Frais et quotas</h3>

#### La capacité d'une blockchain n'est pas infinie.

Une blockchain à besoin d'une puissance de calcul et d'un espace de stockage. Les deux bien que conséquents, ne sont pas illimités. Une attaque possible est la saturation de la puissance de calcul par envoi de milliards de transactions à la seconde. 
Les autres blockchains prélèvent des frais pour chaque action afin de dissuader cette saturation des calculs et de l'espace de stockage.

#### Mais la Ğ1 n'est pas comme les autres.

#### 1- Les frais ne seront prélevés qu'en cas de surcharge de la blockchain.

* **Un nombre total d'actions** (transactions, certifications, adhésion, ...) **par block** a été évalué comme étant la **limite d'un fonctionnement "normal"**. **Au-delà de cette limite** la blockchain est considérée en **saturation**. 
* C’est uniquement si le nombre d’actions dans un bloc **dépasse cette limite** que des **frais sont prélevés**. Frais estimés à environ 0.015 DUĞ1, soit 17 Ğ1 pour 100 transactions.

#### 2- De plus les frais seront remboursés à tous les membres de la toile de confiance *(c'est la force de notre Blockchain)*

<img style="float:right; width:18%;" src="../uploads/blockchain_humain.png"/>

* Un quota d'actions par membre et par bloc est défini (permettant quand même de nombreuses transactions)
* Les comptes non membres pourront être liés à un compte membre et se faire rembourser aussi leur frais, dans la limite du quota par membre. 
* Un membre peut faire une seule transaction mais si, au même moment, quelqu’un lance 1 millions de transactions sur 1 million de comptes, la blockchain sera saturée, ce membre  sera donc prélevé de frais.
* Puis il sera remboursé parce qu'il ne dépasse pas le quota par membre.
* Si ce sont un membre et ses comptes liés qui lancent des centaines de transactions entraînant la saturation, ce membre ne sera remboursé que sur ses premières transactions, les suivantes étant au-delà du quota.
* Si un membre lance des centaines de transactions même au-delà du quota par membre mais qu’il est tout seul à faire des transactions à ce moment-là, cela peut passer sans saturer la blockchain, donc pas de frais.
* Néanmoins, il n'y a pas de remboursement possible en cas de fermeture de compte !
* Il n'y aura pas de remboursement pour les comptes anonymes.

*Il est peu probable qu'une telle attaque soit déclenchée car elle entraînerait la ruine de l'attaquant, pour un blocage temporaire. **Ces frais sont donc une dissuasion**.*

Plus d'informations sur [le forum technique](https://forum.duniter.org/t/les-frais-ca-devient-concret/11877/1)

- - -

<h3 style="color:#f7a212" id="forgerons">8/ Sous-toile Forgerons</h3> 
    
Aujourd'hui n'importe quel membre peut installer un nœud et forger des blocs, ce qui entraîne quelques défauts de mise à jour et de synchronisation ainsi que des problèmes de sécurité, car certains utilisateurs n'ont pas conscience des failles de sécurité de leur installation et "oublient" de mettre à jour.     

Avec la V2 de Duniter, seuls les membres de la sous-toile forgerons pourront forger des blocs.\
Tout le monde peut toujours faire tourner un nœud miroir qui n'écrit pas les blocs mais répond aux demandes des clients.\
Les nœuds forgerons se consacrant au calcul et à l'écriture des blocs, ils ne répondront plus aux demandes des clients.\
Tous les nœuds communiquent entre eux de façon quasi instantanée.

<img src="/uploads/blacksmith2medaille.png" width="15%" align ="right" >

Les **certifications forgerons** devront respecter une **licence** forgeron qui assure **un bon niveau de sécurité**, entre autres avoir déjà fait tourner correctement un nœud miroir depuis un certain temps, pouvoir **garder son serveur ouvert** 24H/24 et 7j/7 et avoir une bonne connexion internet.
Les membres de cette sous-toile n'ont pas nécessairement besoin de se connaître ou de se voir physiquement, car ils doivent d'abord être membres de la Toile de Confiance des Cocréateurs. 

Pour faire partie de cette sous-toile forgerons dès le démarrage, il faut faire tourner un nœud avant la bascule sur la gdev ou gtest (les monnaies qui servent à tester la version 2 avant le démarrage).

Une **documentation** expliquant comment **installer un nœud** et devenir Forgeron en V2 est en train d’être rédigée et sera communiquée dès qu’elle sera terminée.

Les **forgerons actuels** sont invités, si cela les intéresse, à essayer d'installer Duniter V2, pour voir comment ça marche, détecter tous les problèmes d'installation et aider à rédiger la documentation "devenir forgeron". 
Il est/sera possible d'utiliser [Yunohost](https://yunohost.org/fr) ou des [images Docker](https://www.ionos.fr/digitalguide/serveur/know-how/les-images-docker/).

Plus d'informations sur [le forum technique](https://forum.duniter.org/t/video-installer-un-noeud-duniter-v2-en-moins-de-dix-minutes/11243/1)

- - -

<h3 style="color:#f7a212" id="fontionalites">9/ Fonctionnalités à venir</h3> 

De nouvelles fonctionnalités seront possiblement implémentées après le démarrage de Duniter V2, si des développeurs sont disponibles pour s'y mettre.

* Faire des virements automatiques
* Déléguer des pouvoirs sur un compte
* Compte à signatures multiples
* Comptes portefeuilles liés à une identité
* Droit à l'oubli *(suppression de commentaire ?)*
* Possibilité de mettre en place des votes !
* Et plus selon l'imagination de chacun ...

<details> 
  <summary> <b>Auteur </b></summary>

[Maaltir](https://forum.monnaie-libre.fr/u/maaltir/summary) du Collectif MàJ-V2,  validé par [hugotrentesaux](https://duniter.org/team/hugotrentesaux/), [bgallois](https://duniter.org/team/bgallois/), [Moul](https://duniter.org/team/moul/), [Tuxmain](https://duniter.org/team/tuxmain/)

</details>

<h2 id="flyer2"><a href="https://forum.monnaie-libre.fr/uploads/short-url/JnmkyifzobkMKMN7PFWTE6WbYa.pdf" target="_blank" style="color:#34A1FF; font-weight: bold;">Flyer recto-verso à partager
<img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>

<p><strong>Cliquer sur l'image pour agrandir</strong></p>

<div style="float:left; margin: 0 1em 0;"><a href="/uploads/ce-qui-va-changer-p1.jpg" target="_blank">
<img src="/uploads/ce-qui-va-changer-p1.jpg" width="200"/></a><br/> réalisé par @Spiranne</div>

<div style="float:left; margin: 0 1em 0;"><a href="/uploads/ce-qui-va-changer-p2.jpg" target="_blank">
<img src="/uploads/ce-qui-va-changer-p2.jpg" width="200"/></a><br/> réalisé par @Spiranne</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >

<h2 style="color:#34a1ff" id="identique">Ce qui ne changera pas</h2>

* Vous retrouverez toutes vos Ǧ1 sur la nouvelle version.
* Les certifications validées gardent la même date de fin de validité en V2 
* Les règles de création monétaire restent les mêmes (1 DUğ1 par jour revalorisé tous les 6 mois). 
<img src="/uploads/rienechange.png" width="18%" align ="right" >
* Les règles pour être membre de la Toile De Confiance restent les mêmes : 5 certifications minimum respectant la règle de distance.
* La Ğ1 reste toujours la première Monnaie Libre et respecte toujours la T.R.M.
* DuniterV2s, Cesium2, Gecko, Tika, G1nkgo, Ğ1superbot sont toujours des logiciels libres.

**Les nombreuses discussions et questions** autour de cette mise à jour V2, font apparaître plus nettement **des choses qui existaient déjà dans la V1** ; 

* **La prédominance des développeurs** : depuis le début de la Ğ1, ce sont eux qui choisissent les mises à jour à faire. Après le démarrage de la V2, on pourra discuter longuement sur un système pour les prises de décision. 
* **Le pouvoir des forgerons**, ce sont eux qui acceptent ou pas les mises à jour. En V2 les forgerons n’auront plus besoin d’accepter ou pas une mise à jour, elles se feront automatiquement sauf s’ils refusent (principe de liberté).
* **La possibilité d'échange vers d'autres monnaies** : Chacun a toujours été libre d'échanger des ğ1 contre d'autres monnaies. Les autres monnaies sont des objets auxquels chacun est libre de donner une valeur. [Voir ici un extrait de la TRM](https://forum.monnaie-libre.fr/t/g1-et-plateformes-de-change/29900/61) 
* **La mise en place de la Ğ1 sur les plateformes d'exchange** n'est pas plus prévue en V2 qu'en V1. Mais il est de toute façon impossible d’empêcher qui que soit de créer une telle possibilité (c'est le monde du  LIBRE). Si cela n'a pas déjà été fait, c'est juste parce que personne n'y a trouvé suffisamment d'intérêt.  
  Quelques explications complémentaires sur le forum : https://forum.monnaie-libre.fr/t/g1-et-plateformes-de-change/29900/78

<h2 style="color:#34a1ff" id="bascule">Comment va se passer la bascule pour les utilisateurs ?</h2> 

<h3 style="color:#f7a212" id="date">A/ Date de bascule</h3>

La date de bascule n'est pas encore définie, cela dépend de l'avancement des développements et donc aussi de leur financement. [Soutenez les devs](https://www.helloasso.com/associations/axiom-team/collectes/finalisation-de-cesium-v2-et-duniter-v2). 

<img src="/uploads/code-geek.png" width="18%" align ="right" >

Pour l'instant, les développeurs visent le 8 mars, date anniversaire de la Ğ1. 

Ce changement de Blockchain est un projet de grande envergure qui va faciliter les choses pour les utilisateurs de la June.

Le collectif MàJ-V2 communique au maximum sur cette bascule, pour que tous les junistes soient à l'aise avec cette mise à jour de leurs logiciels, et puissent en parler. 

* sur le forum : '[Comment cela va se passer : encore des questions ?](https://forum.monnaie-libre.fr/t/comment-cela-va-se-passer-encore-des-questions/31042) 
* sur Telegram : Telegram: Contact [@monnaielibrejune](http://t.me/monnaielibrejune/57035)

<h3 style="color:#f7a212" id="scenario">B/ Scénario de bascule V1 vers V2</h3>

#### La fin de la V1

**Le scénario de bascule n'est pas encore formellement établi.**

 L'instant **0** (date et heure) de démarrage de Duniter 2.0 n'est pas encore définitivement choisi.

* 5 ou 6 semaines avant l’instant zéro, un message apparaîtra dans Cesium pour indiquer la date de la bascule en informant qu’il y aura une mise à jour (automatique ou manuelle) du logiciel client. 
* Une dernière mise à jour de césium sera disponible, par sécurité, celle-ci évitera toute action sur la V1, qui pourrait être perdue. 
* 30 Jours avant la bascule réelle, il y aura une bascule de test. Ceux qui le désirent pourront tester les logiciels disponibles avec la Ğtest. 
* Environs 1 h avant l'instant **0** il y aura une prise d'image de la Ğ1  

<img src="/uploads/photo.jpg" width="18%" align ="right" >

* Il sera préférable d'éviter toute action sur la Ğ1 quelques minutes avant la prise d'image ("Ne bougeons plus !", CLIC photo). Un dernier coup de communication sera lancé sur un maximum de plateformes pour encourager à arrêter d’utiliser la Ğ1 en attendant qu’elle soit déplacée sur la v2.
* Toutes les **actions sur la V1 après cette photo** seront enregistrées dans la blockchain V1 si (malgré tout) des nœuds continuent de tourner en V1, mais **seront perdues pour la V2**.
* La Blockchain V2 sera lancée à partir de cette image. 
* **La Ǧ1 sera indisponible pendant quelques heures, pendant cette phase de redémarrage.**

#### Après le démarrage de la V2.

* Les utilisateurs recevront ou seront invités à **mettre à jour chacune de leurs installations** (ordi, tablette, téléphone, Firefox, Brave...) de l'application cliente (Césium 2.0) Si tout va bien, la mise à jour sera automatique. Normalement, les versions précédentes de césium ne fonctionneront plus.
* **Si la mise à jour césium n'est pas immédiate**, c'est sans importance. Il suffira de la faire avant la prochaine connexion, ou bien d'installer un autre logiciel client (Gecko, Tikka, G1nkgo, ...) 
* Il faudra de préférence, vérifier cette **mise à jour avant d'aller à un ğmarché**, pour profiter d'une bonne connexion internet à la maison. 
* Après la mise à jour ou l'installation d'un logiciel client V2, **les utilisateurs retrouveront leurs comptes**, leurs opérations et leurs certifications, sauf ce qui était en attente d'être enregistré dans la blockchain (piscine).

<img src="/uploads/lav2.png" width="18%" align ="right" >

* **Le DU continue** de se créer tout seul sur les comptes membres.
* Les demandes d'adhésion non validées et les certifications en attente (en piscine) auront disparu. **Il suffira de les refaire**.
* Les transactions faites à l'instant de la prise d'image (pour ceux qui n'auraient pas suivi les consignes), **pourraient** ne pas avoir eu le temps d'être validées (on vous avait dit de **pas bouger**, vous êtes flou), elles seraient aussi à refaire. 
* Les utilisateurs pourront reprendre leurs activités comme avant avec beaucoup plus de fluidité grâce aux nouvelles applications clientes.

Détails sur le [forum technique](https://forum.duniter.org/t/chronologie-de-la-migration/12605)

<details> 
  <summary> <b>Auteur </b></summary>

[Maaltir](https://forum.monnaie-libre.fr/u/maaltir/summary) du Collectif MàJ-V2,  validé par les devs [hugotrentesaux](https://duniter.org/team/hugotrentesaux/), [bgallois](https://duniter.org/team/bgallois/), [Moul](https://duniter.org/team/moul/), [Tuxmain](https://duniter.org/team/tuxmain/), [Vit](https://duniter.org/team/vit/)

</details>

<h2 id="flyer3"><a href="https://forum.monnaie-libre.fr/uploads/short-url/c8RsHViPjwggiEzcWoQqT1KWYhk.pdf" target="_blank" style="color:#34A1FF; font-weight: bold;">Flyer recto-verso à partager <img src="/uploads/clic40.png" style="padding-left:10px;display:inline;"/></a></h2>

<p><strong>Cliquer sur l'image pour agrandir</strong></p>

<div style="float:left; margin: 0 1em 0;"><a href="/uploads/ce-qui-ne-change-pas-et-comment-5-spiranne_page_1.jpg" target="_blank">
<img src="/uploads/ce-qui-ne-change-pas-et-comment-5-spiranne_page_1.jpg" width="200"/></a><br/> réalisé par @Spiranne</div>

<div style="float:left; margin: 0 1em 0;"><a href="/uploads/ce-qui-ne-change-pas-et-comment-5-spiranne_page_2.jpg" target="_blank">
<img src="/uploads/ce-qui-ne-change-pas-et-comment-5-spiranne_page_2.jpg" width="200"/></a><br/> réalisé par @Spiranne</div>

<hr style=" border: 2px solid #34A1FF;margin:10px auto 50px; width: 100%;"/>
<img src="https://forum.monnaie-libre.fr/uploads/default/optimized/2X/b/b0cf4175b437b18b8f9f8f66ec5ddee2eb9d0daa_2_499x499.png" width="200" align ="center" >

<h2 style="color:#34a1ff" id="?">Les logiciels satellites - découverte et tests</h2>