---
title: Test typographie
description: Test...
---
# typographie

**Advertisement :)**
* **[pica](https://nodeca.github.io/pica/demo/)** - high quality and fast image
  resize in browser.
* **[babelfish](https://github.com/nodeca/babelfish/)** - developer friendly
  i18n with plurals support and easy syntax.


##  Titre 8-)

Pour faire un titre,  commencez la ligne par des # plus il y en a,  plus vous descendez dans la hiérarchie des titres.
```
# Titre 1

## Titre 2

### Titre 3

#### Titre 4

#####Titre 5

###### Titre 6
```
# Titre 1

## Titre 2



### Titre 3

#### Titre 4

#####Titre 5

###### Titre 6

Comme vous pouvez le voir si vous ne metter pas d'espace entre les # et le texte ça ne fonctionne pas.
Isolez les lignes titre avec une ligne vide avant et après pour que cela fonctionne correctement  

## Titres en couleurs

Il faut absoulument rajouter une id pour chaque titre (l'id automatique ne marche plus)
```
## <h2 style="color:#34A1FF;" id="pourquoi-changer-">Pourquoi changer ?</h2>

Les  dièses ne servent plus à rien

 <h3 style ="color: #F7A212" id=inconvenients>1/ Inconvénients de la V1</h3>
```

## <h2 style="color:#34A1FF;" id="pourquoi-changer-">Pourquoi changer ?</h2>

Les  dièses ne servent plus à rien

 <h3 style ="color: #F7A212" id=inconvenients>1/ Inconvénients de la V1</h3>

Et voilà c'est tout simple ! 

## Lignes horizontales
Il sufit de 3 tirets,
```
- - -
---
________
```

- - -
---
________


## Blocs d'alertes et déroulants (masqués)
```
<alert type="info">
Alert de type "info"
</alert>    
```  
<alert type="info">
Alert de type "info"
</alert>    

``` 
<alert type="warning">
Alert de type "warning"
</alert>
```    
<alert type="warning">
Alert de type "warning"
</alert>

<alert type="succes">
Alert de type "success"
</alert>

<alert type="danger">
Alert de type "danger"
</alert>

```
<details> 
  <summary> Afficher le contenu masqué</summary>
  Ici le contenu masqué 
</details>
```
<details> 
  <summary> Afficher le contenu masqué</summary>
  Ici le contenu masqué 
</details>

Vous pouvez combiner les deux : 
```
<alert type="info">
<details> <summary>**Afficher le contenu masqué**</summary>

  Ici le contenu masqué   
  [Maaltir](https://forum.monnaie-libre.fr/u/maaltir/summary)</details>
</alert>
```
<alert type="info">
<details> <summary>**Afficher le contenu masqué**</summary>

  Ici le contenu masqué   
   [Maaltir](https://forum.monnaie-libre.fr/u/maaltir/summary) 
</details>
</alert>

Il faut mettre une ligne vide pour que les liens fonctionnent



## Lexique
Utulisation du lexique  du site monnaie-libre
`<lexique>blockchain</lexique>` Donne : <lexique>blockchain</lexique>

`<lexique title="forgeron">forgeurs</lexique>` Donne : <lexique title="forgeron">forgeurs</lexique>

Il faut bien sur avoir créer l'élément lexique correspondant pour que ça marche


## Typographic replacements

Enable typographer option to see result.

(c) (C) (r) (R) (tm) (TM) (p) (P) +-

test.. test... test..... test?..... test!....

!!!!!! ???? ,, -- ---

"Smartypants, double quotes" and 'single quotes'

## Emphasis

**This is bold text**

**This is bold text**

*This is italic text*

*This is italic text*

~~Strikethrough~~

## Blockquotes

> Blockquotes can also be nested...
>
> > ...by using additional greater-than signs right next to each other...
> >
> > > ...or with spaces between arrows.

## Lists

Unordered

* Create a list by starting a line with `+`, `-`, or `*`
* Sub-lists are made by indenting 2 spaces:

  * Marker character change forces new list start:

    * Ac tristique libero volutpat at
    * Facilisis in pretium nisl aliquet
    * Nulla volutpat aliquam velit
* Very easy!

Ordered

1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa
4. You can use sequential numbers...
5. ...or keep all the numbers as `1.`

Start numbering with offset:

57. foo
58. bar

## Code

Pour afficher le code sans qu'il soit interprété :
Sur une ligne, il suffit de le mettre entre deux "`" : `code` 

Ce "`" s'obtient en appuyant les touche "alt gr" + "7" ou "è" sur les claviers français.

Pour un bloc de code, il faut commencer et finir par une ligne de 3 "`". 

```
// Some comments
line 1 of code
line 2 of code
line 3 of code
```

Syntax highlighting (faire apparaitre les mots du langage en couleur)
Il suffit de faire suivre les 3 premiers "`" par l'extension correspondant au langage ```js


```js
var foo = function (bar) {
  return bar++
}

console.log(foo(5))
```

## Tables

| Option | Description                                                               |
| ------ | ------------------------------------------------------------------------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default.    |
| ext    | extension to be used for dest files.                                      |

Right aligned columns

| Option | Description                                                               |
| ------ | ------------------------------------------------------------------------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default.    |
| ext    | extension to be used for dest files.                                      |

## Links

[link text](http://dev.nodeca.com)

[link with title](http://nodeca.github.io/pica/demo/ "title text!")

Autoconverted link https://github.com/nodeca/pica (enable linkify to see)

## Images

`![Minion](https://octodex.github.com/images/minion.png)`    
(ne pas oublier le point d'exclamation au debut) donne :
![Minion](https://octodex.github.com/images/minion.png) 

```html
<img src="https://octodex.github.com/images/minion.png" width="300" align ="right" > 


Avec du texte à coté !!
```  
donne :  
<img src="https://octodex.github.com/images/minion.png" width="300" align ="right" >


Avec du texte à coté !!

---
<hr/>


```html
<a href="/uploads/pourquoi-changer-de-version-duniter_page_1.jpg"><img src="/uploads/pourquoi-changer-de-version-duniter_page_1.jpg" width="200"/></a> 
```
donne :
<a href="/uploads/pourquoi-changer-de-version-duniter_page_1.jpg"><img src="/uploads/pourquoi-changer-de-version-duniter_page_1.jpg" width="200"/></a> 

```html
<img src="../uploads/pourquoi-changer-de-version-duniter_page_1.jpg" width="400"/> 
```

<img src="../uploads/pourquoi-changer-de-version-duniter_page_1.jpg" width="400"/> 


---
```
![image 1](/uploads/pourquoi-changer-de-version-duniter_page_1.jpg)
```
donne
![image 1](/uploads/pourquoi-changer-de-version-duniter_page_1.jpg)


---

![Stormtroopocat](https://octodex.github.com/images/stormtroopocat.jpg "The Stormtroopocat")

Like links, Images also have a footnote style syntax

![Alt text](https://octodex.github.com/images/dojocat.jpg "The Dojocat")

With a reference later in the document defining the URL location:

## Plugins



The killer feature of `markdown-it` is very effective support of
[syntax plugins](https://www.npmjs.org/browse/keyword/markdown-it-plugin).


### [Footnotes](https://github.com/markdown-it/markdown-it-footnote)
```
Footnote 1 link[^first].

Footnote 2 link[^second] en milieu de phrase.

Inline footnote^\[Text of inline footnote] definition.
Comme vous le voyez cela ne fonctionne pas

Duplicated footnote reference[^second].

[^first]: Footnote **can have markup**


Vous pouvez mettre la note[^3] n'importe où, elle apparaitra toujours en bas de pages
Voyez vous meme

[^3]: Attention quand meme aux notes de bas de pages
quand vous voulez les mettre sur plusieurs lignes. 

[^second]: Footnote text.

Meme si vous mettez du texte après
La note apparait toujours en bas de page
```

Footnote 1 link[^first].

Footnote 2 link[^second] en milieu de phrase.

Inline footnote^\[Text of inline footnote] definition.
Comme vous le voyez cela ne fonctionne pas

Duplicated footnote reference[^second].

[^first]: Footnote **can have markup**

Vous pouvez mettre la note[^3] n'importe où, elle apparaitra toujours en bas de pages
Voyez vous meme

[^3]: Attention quand meme aux notes de bas de pages
quand vous voulez les mettre sur plusieurs lignes.


[^second]: Footnote text.

Meme si vous mettez du texte après
La note apparait toujours en bas de page


<img src="https://octodex.github.com/images/minion.png" width="300" align ="right" > 

### [Emojies](https://github.com/markdown-it/markdown-it-emoji)

> Classic markup: :wink: :crush: :cry: :tear: :laughing: :yum:
>
> Shortcuts (emoticons): :-) :-( 8-) ;)

see [how to change output](https://github.com/markdown-it/markdown-it-emoji#change-output) with twemoji.

### [Subscript](https://github.com/markdown-it/markdown-it-sub) / [Superscript](https://github.com/markdown-it/markdown-it-sup)

``` 
* 19^th^
* H\~2\~O
```
* 19^th^
* H\~2\~O   
Ne fonctionne pas
```
* 19<sup>th</sup>
* H<sub>2</sub>O
```
* 19<sup>th</sup>
* H<sub>2</sub>O   
Fonctionne beaucoup mieux

### [<ins>](https://github.com/markdown-it/markdown-it-ins)

++Inserted text++

### [<mark>](https://github.com/markdown-it/markdown-it-mark)

\==Marked text==



### [Definition lists](https://github.com/markdown-it/markdown-it-deflist)

Term 1

: Definition 1
with lazy continuation.

Term 2 with *inline markup*

: Definition 2

```
    { some code, part of Definition 2 }

Third paragraph of definition 2.
```

*Compact style:*

Term 1
~ Definition 1

Term 2
\~ Definition 2a
\~ Definition 2b

### [Abbreviations](https://github.com/markdown-it/markdown-it-abbr)

This is HTML abbreviation example.

It converts "HTML", but keep intact partial entries like "xxxHTMLyyy" and so on.

\*\[HTML]: Hyper Text Markup Language

### [Custom containers](https://github.com/markdown-it/markdown-it-container)

::: warning
*here be dragons*
:::