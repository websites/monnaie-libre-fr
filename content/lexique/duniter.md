---
title: Duniter
description: Le logiciel qui permet d'écrire et de consulter la Blockchain
---
Duniter est le logiciel disponible pour toute personne désirant permettre l'utilisation de la monnaie.   
En l'installant sur un ordinateur, celui-ci devient un nœud.

Toutes les infos sur [duniter.org](https://duniter.org/)