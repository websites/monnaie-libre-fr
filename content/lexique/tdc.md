---
title: TdC
description: Toile de Confiance
---
Comme tout système fiduciaire, la monnaie libre repose sur la confiance. Dans le référentiel de la monnaie-dette, le banquier joue, en plus du role de créateur monétaire celui de "tiers de confiance". C'est sur lui que repose l'intégrité du système monétaire.

## Système d'identification décentralisé.

Dans le référentiel de la monnaie libre, ce role est également décentralisé sur les etres humains, sur tous les cocréateurs de monnaie. C'est ce qu'on appelle la Toile de Confiance, la TdC.

La *toile de Confiance* a pour but d'identifier tous les humains cocréateurs de monnaie.
Seul un humain vivant peut être cocréateur de monnaie, pour cela il faut identifier chaque humain et sa laison avec une clé publique unique.
Un humain ne peut devenir cocréateur de monnaie que s'il est identifié par au moins cinq autres cocréateurs de monnaie, tout en respectant la <lexique title="règle de distance">règle des cinq pas</lexique>.
Il doit au préalable adhérer, en transformant un de ses portefeuilles en *"compte membre"*.

## Certification

L'identification se fait par la certification.
Quand un cocréateur de monnaie est capable d'identifier un humain comme étant unique et vivant, il peut le certifier.
Cela implique de bien connaître le <lexique>jüniste</lexique>, être en mesure de le reconnaître et de le contacter en dehors du réseau de la Ğ1.
Cette certification est valable deux ans seulement (730 jours et 12 heures), une certification peut être renouvelée autant que vous voulez, ou pas.

## Stock de certifications.

Chaque cocréateur peut avoir maximum 100 certifications émises en cours de validité. Chaque certifications a une durée de validité de 2 ans.\
Toute certifications émise depuis plus de 2 ans n'est plus comptée dans les 100.   
Donc chaque cocréateur peut émettre 100 certifications sur 2 années glissantes. 

## Adhésion

L'adhésion se fait au moment de la transformation d'un portefeuille en compte membre.
Pour pouvoir continuer à être cocréateur de monnaie un cocréateur doit renouveler son adhésion dans un délai de 365 jours et 6 heures, à compter de la date de son dernier renouvellement.

## Plus de détails

Voir la [toille de confiance en détail](https://duniter.org/fr/toile-de-confiance/la-toile-de-confiance-en-detail/)