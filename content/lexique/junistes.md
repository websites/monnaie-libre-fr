---
title: Junistes
description: Les utilisateurs de la june
synonyms:
  - juniste
  - jüniste
  - jünistes
---

## Un(e) Juniste au singulier.

Désigne un humain qui utilise la Ğ1.
Qu'il soit membre de la <lexique title="TdC">toile de confiance</lexique> (compte créateur) ou simple utilisateur (portefeuille).

## Un monnaie-libriste

Désigne tout utilisateur d'une monnaie libre au sens de la <lexique>TRM</lexique>. A ce jour il n'existe qu'une seule monnaie libre la Ğ1.
La distinction avec le Juniste apparaîtra quand de nouvelles monnaies libres feront leur apparition, pour l'instant il n'y en a pas.
