---
title: Client
description: Logiciel pour les utilisateurs de la Ğ1
---
## Pour les utilisateurs de la Ğ1
Pour pouvoir utiliser la Ğ1 il faut un logiciel que l'on télécharge sur son ordinateur ou son ordiphone.

Les logiciels clients de la  Ğ1 :
* [Césium](https://cesium.app/fr/) Le plus utilisé, existe pour presque toutes les versions de système.
* Sakia, un peu plus complexe, qui sera bientôt remplacer par Tikka.
* Silkaj en ligne de commande
* Gecko pas encore disponible plutôt axé sur les paiements.
* Tikka futur remplaçant de sakia

## Les clients se connectent aux noeuds.
Les client se sélectionnent plus ou moins automatiquement le <lexique>nœud</lexique> auquel ils se connectent. Pour l'instant césium propose à l'utilisateur de selectionner un nœud par paramétrage.
