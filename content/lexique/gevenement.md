---
title: ğévènement
description: Évènement autour de la monnaie libre Ğ1
---
Évènement autour de la monnaie libre Ğ1. 

Les évènement que proposent les utilisateurs sont souvent des **ğ**quelque-chose.   
Rares sont ceux qui peuvent prétendre avoir vraiment compris la signification de ce "ğ" voir [www.glibre.org](https://www.glibre.org/fondement-semantique-de-%e1%b8%a1/).

Les organisateur d'évènements utilisent ce ğ régulièrement comme simple référence à la Ğ1.   
Nous avons donc des 
* ğapéros
* ğrencontres
* ğpique-niques
* ğmarchés
* ğ...     

A vous d'inventer le ğ... qui vous convient. 
